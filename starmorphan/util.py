from functools import lru_cache
import re
import typing
import unicodedata

POLISH_LATIN_TO_CYRILLIC_FROM = \
    "ABCEHKMOPTXacekopxyёАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
POLISH_LATIN_TO_CYRILLIC_TO = \
    "авсенкмортхасекорхуеабвгдежзийклмнопрстуфхцчшщъыьэюя"
RUSSIAN_VOWELS = "аеиоуыэюя"
VOWELS = ("AaEeIiOoUuYyАаЕеИиОоУуЫыЭэЮюЯяɛʡɨɔǖⒶⓐ⒜ẠạÅåÄäẢảḀḁẤấẦầẨẩȂȃẪẫẬậẮắẰằẲẳ"
          "ẴẵẶặĀāĄąȀȁǺǻȦȧÁáǞǟǍǎÀàÃãǠǡÂâȺⱥÆæǢǣǼǽⱯꜲꜳꜸꜹꜺꜻⱭ℀⅍℁ªⒺⓔ⒠ḔḕḖḗḘḙḚḛḜḝẸẹẺẻẾế"
          "ẼẽỀềỂểỄễỆệĒēĔĕĖėĘęĚěÈèÉéÊêËëȄȅȨȩȆȇƎⱻɆɇƏǝⱸℯ℮ℇƐⒾⓘ⒤ḬḭḮḯĲĳÍíÌìÎîÏïĨĩĪī"
          "ĬĭĮįǏǐıỺⅈⅉℹℑℐỊİịȈȉȊȋƜⓄⓞ⒪ÖöṎṏṌṍṐṑṒṓȪȫȬȭȮȯȰȱǪǫǬǭỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞở"
          "ỠỡỢợƠơŌōŎŏŐőÒòÓóÔôÕõǑǒȌȍȎȏŒœØøǾǿꝊꝎꝏ⍥⍤ℴⓊⓤ⒰ṲṳṴṵṶṷṸṹṺṻỦủỤụỨứỪừỬửỮữỰựŨũ"
          "ŪūŬŭŮůŰűǙǚǗǘǛǜŲųǓǔȔȕÛûȖȗÙùÚúÜüƯưɄƲƱⓎⓨ⒴ẎẏỾỿỲỳỴỵỶỷỸỹŶŷƳƴŸÿÝýɎɏȲȳƔẙ⅄ℽ")

REMOVE_ACCENTS = str.maketrans("ё", "е", "\u0301")
REMOVE_PARENS = str.maketrans("", "", "()")
POLISH_LATIN_TO_CYRILLIC = str.maketrans(POLISH_LATIN_TO_CYRILLIC_FROM,
                                         POLISH_LATIN_TO_CYRILLIC_TO)


class SuccessException(Exception):
    pass


def deacc(s: str, pad=False) -> str:
    if pad:
        return ' '.join(w.translate(REMOVE_ACCENTS).ljust(len(w))
                        for w in s.split(' '))
    return s.translate(REMOVE_ACCENTS)


@lru_cache(maxsize=300)
def _parse_accent_place(accent_place: str) -> typing.Tuple[int, int, int]:
    pr_pl = take_int(accent_place)
    if pr_pl == 0:
        return (0, 0, 0)
    sec_pl, yo_pl = 0, 0
    q = accent_place.find(", ")
    if q >= 0:
        yo_pl = take_int(accent_place[q + 1:])
    q = accent_place.find(".")
    if q >= 0:
        sec_pl = take_int(accent_place[q + 1:])
    return (pr_pl, sec_pl, yo_pl)


def demacc(string: str, accent_place: str) -> str:
    """Simple accentuation of a word.
    Note that the place of accent (AKZPL) is passed as a string:
    it may contain ", " and "." to specify secondary accents and
    "yo"."""

    if not accent_place:
        return string

    pr_pl, sec_pl, yo_pl = _parse_accent_place(accent_place)
    if pr_pl == 0:
        return string

    result = string
    if yo_pl > 0:
        result = stuff(result, yo_pl - 1, 1, "ё")
    if voc(string, pr_pl):
        result = stuff(result, pr_pl - 1, 1, pr_acc(result[pr_pl - 1]))
    if sec_pl > 0:
        result = stuff(result, sec_pl - 1, 1, sec_acc(result[sec_pl - 1]))
    return result


def derform(string: str,
            refl: bool = False) -> str:
    """Strip off the reflexive suffix "-ся"."""

    return string[:-2] if refl else string


def engf(s: str) -> str:
    return ''.join((c if c in "'-\\" or 'a' <= c
                    else c.lower() if 'A' <= c <= 'Z'
                    else '') for c in s)


def first(s: str,
          delimiter: str = ' ') -> str:
    """Return the slice of the string before delimiter or end of string"""
    pos = s.find(delimiter)
    return s[:pos] if pos >= 0 else s


@lru_cache(maxsize=10000)
def is_cyrillic(c: str) -> bool:
    return unicodedata.name(c).startswith("CYRILLIC")


def isstandard(s: str) -> bool:
    return ord(s[:1]) < 127 or 'А' <= s[0].upper() <= 'Я'


def isvoc(s: str) -> bool:
    """Test if there is a vowel in the source string."""

    return any(voc(s, p) for p in range(1, len(s) + 1))


def letterize(s: str) -> str:
    """Normalize the transliteration of every received word,
    reducing it to canonical dictionary shape."""

    return ''.join((c if c in "'-" or 'a' <= c <= 'z' or c >= chr(128)
                    else c.lower() if 'A' <= c <= 'Z'
                    else '') for c in s.lstrip("'"))


def plene(s: str) -> str:
    """Inserts the "fugitive" е vowel into a word."""

    if s == "зайц":
        return "заяц"
    elif s == "кочн":
        return "кочан"
    elif (len(s) < 2 or
          (s.endswith("ьц") and not s.endswith("льц")) or
          (s[-2] not in "ьвптдбчжшщй" and s[-1] not in "цнл") or
          s[-2] in RUSSIAN_VOWELS):
        return ""
    pos, count = ((-2, 1)
                  if (len(s) > 2 or s in ("пс", "дн", "пн")) and yer(s)
                  else (-1, 0))
    return stuff(s, pos, count, "е")


def pleno(s: str) -> str:
    """Inserts the "fugitive" о vowel into a word."""

    if len(s) >= 2 and \
            s[-2] not in RUSSIAN_VOWELS and \
            (len(s) > 2 or
             s in ("лб", "мх", "рв", "рт", "сн", "шв", "вш", "лж", "рж",
                   "вс", "зл")):
        return stuff(s, -1, 0, "о")
    return s


def polyvocal(string: str) -> int:
    """Returns 0 if string has now vowels, 1 if  string has just 1 vowel, 2 otherwise"""
    vocals = (1 for c in string if c in VOWELS)
    return next(vocals, 0) + next(vocals, 0)


def pr_acc(symbol: str) -> str:
    """Adds a primary accent to a symbol"""

    return symbol + "\u0301"


def rest(s: str, delimiter: str = ' ') -> str:
    """Return the string slice after delimiter"""

    return s[s.index(delimiter) + len(delimiter):]


def rform(string: str, refl: bool = False) -> str:
    """Add the reflexive suffix if needed."""

    if string and not string.startswith("-") and refl:
        return "сь" if voc(string, -1) or string.endswith("\u0301") else "ся"
    return ""


def rusf(s: str) -> str:
    """Substitute accidental Latin characters by Cyrillic"""

    return s.translate(POLISH_LATIN_TO_CYRILLIC)


def sec_acc(symbol):
    """Adds a secondary accent to a symbol"""

    return symbol + "\u0300"


def stuff(s: str, start: int, count: int, replacement: str) -> str:
    return s[:start] + replacement + s[start + count:]


def take_int(s: str) -> int:
    m = re.match(r"^ *(\d+)", s)
    if m:
        return int(m.group(1))
    return 0


def voc(s: str, index: int) -> bool:
    return s[index if index < 0 else (index - 1)] in VOWELS


def vocal(string: str) -> str:
    """Inserts morphonological vowels after verbal prefixes."""

    if string:
        if string.endswith(('рас', 'нис', 'вос', 'вс', 'ис')):
            return string[:-1] + 'зо'
        elif string.endswith(('под', 'раз', 'над', 'ред', 'воз', 'низ')):
            return string + 'о'
        elif string.endswith(('в', 'с', 'от', 'об', 'из', 'вз')):
            return string + 'о'
    return string


def yer(s: str) -> bool:
    """Tests if the 2nd to last letter of a word-stem is "ь, й" or not."""

    return len(s) >= 2 and s[-2] in "йь"
