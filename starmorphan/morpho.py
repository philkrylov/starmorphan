"""
MORPHO.PRG - a complete module for Russian (and English)
morphological analysis.
*Author: S.A.Starostin

LIST OF FUNCTIONS

SPELLCHECK
SCREENOUT [S]
RMORPH
LISTING   [S]
PR_ACC
SEC_ACC
DEMACC
CONTRET
SGET
DERFORM
RFORM
NOMINDEX
BUILDIND
ADDCASE
DETTYPE
POLYVOC
FIRSTVOC
LASTACC
FIRSTACC
SHOWPAR
BUILDVIND
SHOWVPAR
DICTWRITE [S]
AADCH  [S]
ENGSEEK
ELASTACC [S]
PROPOSE
RUSDECLAR [S]
NOUN
NOUNSEEK
DEPAL  [S]
SEND   [S]
VERB
VERBSEEK
PASS_TEST [S]
PASTTEST [S]
PRESTEST [S]
VOCAL [S]
DEVOC [S]
PARTTEST [S]
VSEND [S]
SEARCHCASE
"""

from copy import copy
from itertools import chain, zip_longest
import re
from typing import Iterable, List, Optional, Tuple

import attr

from .paradigm import paradigm_scope
from .tables import (
    z_irrstr1, z_irrinf1,
    z_irrstr2, z_irrinf2,
    z_irrstr3, z_irrinf3,
    z_irrstr4, z_irrinf4,
    z_irrstr5, z_irrinf5,
    z_irrstr6, z_irrinf6,
    z_irrstr7, z_irrinf7,
    z_irrstr8, z_irrinf8,
    z_irrstr9, z_irrinf9,
    z_cases,
    z_m_str,
    z_f_str,
    z_f_str2,
    z_n_str,
    z_men_str,
    z_a_cases,
    z_a_str,
    z_eatstring,
    z_eatstr2,
    z_givestring,
    z_shavestring,
    z_singstring,
    z_blowstring,
    z_smellstring,
    p2string,
    prstring,
    z_irrst10, z_irrin10,
    z_irrinf,
    z_exstring,
)
from .util import (
    deacc, demacc, derform, engf, first, is_cyrillic, isstandard, isvoc, letterize,
    plene, pleno, polyvocal, rest, rusf, take_int, voc, vocal, yer, REMOVE_PARENS, SuccessException
)
from .zalizniak import dict_get_entry


nompar, verbpar = paradigm_scope()


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * n
    return zip_longest(fillvalue=fillvalue, *args)


@attr.s(auto_attribs=True, slots=True)
class Hypothesis:
    source: str = ""  # potential source forms
    mess: str = ""    # nominal dictionary entries


@attr.s(auto_attribs=True, slots=True)
class NounHypothesis(Hypothesis):
    n_acc_pl: str = ""
    ending: str = ""      # potential noun endings
    ablaut: bool = False  # Boolean for nouns
    res_str: str = ""     # noun labels
    case_str: str = ""    # case-name strings
    end_str: str = ""     # case-ending strings
    accent: str = ""      # noun accents
    succnom: bool = False  # Id. for nouns

    def to_delimited(self, delimiter: str, build_par=False, transinf=False,
                     full=False) -> str:
        if self.succnom:
            if build_par:
                if transinf:
                    return self.mess + " " + delimiter
                if "Indecl" in self.res_str:
                    return demacc(self.source, self.n_acc_pl) + " " + delimiter
                return nompar(self.source, self.mess, self.case_str,
                              self.end_str, self.accent, self.n_acc_pl,
                              build_par=build_par)[0] + delimiter
            addresult = ((" [" + first(self.mess, "\x04") + "] ") if full
                         else " ")
            return self.source + addresult + self.res_str[1:] + delimiter
        return ''


@attr.s(auto_attribs=True, slots=True)
class VerbHypothesis(Hypothesis):
    inf: str = ""           # Id. for verbs
    succverb: bool = False  # Boolean success marks
    v_acc_pl: str = ""
    voice: str = ""         # voices
    mood: str = ""          # Mood
    tense: str = ""         # Tense
    person: str = ""        # Person
    gender: str = ""        # Gender
    cnumber: str = ""       # Number
    v_type: str = ""        # Conjugation type
    endstem: str = ""       # stem-final consonants (letters)
    info: str = ""          # dictionary entries (same as INFORM)
    reflexive: bool = False
    is_participle: bool = False

    def rform(self, s: str) -> str:
        """Add the reflexive suffix if needed."""

        if s and not s.startswith("-") and self.reflexive:
            return ("сь" if (voc(s, -1) or
                             s.endswith(("а́", "и́", "о́", "у́", "ю́", "я́")))
                    else "ся")
        return ""

    def to_delimited(self, form: str,
                     delimiter: str,
                     build_par: bool = False,
                     transinf: bool = False,
                     full: bool = False,
                     secend: Optional[str] = '') -> str:
        if self.succverb:
            if build_par:
                if transinf:
                    return self.info + "<p>"
                if self.voice.startswith('act.'):
                    self.inf += self.rform(self.inf)
                return (verbpar(self.inf, self.info, self.v_acc_pl, build_par)
                        + delimiter)
            addresult = (" [" + first(self.info, "\x04")) if full else " "
            partcond = ((self.is_participle or self.gender.startswith('p.'))
                        and not (self.voice.startswith('act.')
                                 and form.endswith("мый")))
            if partcond or self.gender.startswith('p.'):
                self.mood = "part."
                return ''.join((
                    self.inf,
                    (self.rform(self.inf) if self.voice.startswith('act.') else ''),
                    addresult,
                    self.voice,
                    " ",
                    self.mood,
                    " ",
                    self.tense,
                    " ",
                    searchcase(secend, z_a_cases, z_a_str, form=form),
                    delimiter
                ))
            if self.mood.startswith("ger."):
                self.cnumber = ""
            elif self.mood.startswith("imp."):
                self.tense = ""
            return ''.join((
                self.inf,
                self.rform(self.inf) if self.voice.startswith('act.') else '',
                addresult,
                self.voice,
                " ",
                self.mood,
                " ",
                self.tense,
                " ",
                " ",
                self.gender,
                self.person,
                " ",
                self.cnumber,
                delimiter
            ))
        return ''


@attr.s(auto_attribs=True, slots=True)
class QResult:
    infin: str
    inform: str
    v_pl_ac: str


@attr.s(auto_attribs=True, slots=True)
class DResult:
    cargo: Optional[List] = None
    source: Optional[str] = None
    sform: Optional[str] = None
    info: Optional[str] = None
    aspect: Optional[str] = None
    trans: Optional[str] = None
    voice: Optional[str] = None
    mood: Optional[str] = None
    pspeech: Optional[str] = None
    person: Optional[str] = None
    gender: Optional[str] = None
    number: Optional[str] = None
    tense: Optional[str] = None
    anim: Optional[str] = None


BRACKET_TO_PARENS = str.maketrans("[]", "()")
REMOVE_BRACKETS = str.maketrans("", "", "[]")
REMOVE_CURLY = str.maketrans("", "", "{}")
REMOVE_ACUTE_DIERESIS = str.maketrans("", "", "\u0301\u0308")
REMOVE_HYPHEN_APOSTROPHE = str.maketrans("", "", "-'")


def irregular(inf_string: str, str_string: str, stem: str,
              contmrph: bool = False) -> str:
    spaced_stem = ''.join((' ', stem, ' '))
    if spaced_stem in str_string:
        if contmrph:
            raise SuccessException()
        return first(inf_string[str_string.index(spaced_stem) + 1:])
    return ''


def prstring_get(form: str, only_success=False):
    spaced_form = ''.join((' ', form, ' '))
    prpl = prstring.find(spaced_form)
    if prpl >= 0:
        if only_success:
            return True
        midstr = p2string[p2string.rfind('\n', 0, prpl) + 1:]
        par_str = first(midstr, "\n")
        sour_str = par_str[par_str.index(' ', par_str.index(' ') + 1) + 1:]
        cas_str = p2string[prpl - 20:prpl].strip()
        cas_str = cas_str[(cas_str.rfind('\n') if '\n' in cas_str
                           else cas_str.rfind(' ')) + 1:]
        return NounHypothesis(
            source=first(sour_str),
            mess=par_str,
            res_str=' %s' % (cas_str[cas_str.rfind(' ') + 1:]),
            succnom=True,
        )


@attr.s(auto_attribs=True, slots=True)
class Parser:
    noun_hypotheses: List[NounHypothesis] = attr.Factory(list)
    verb_hypotheses: List[VerbHypothesis] = attr.Factory(list)
    build_stage: int = -1  # For interactive userdict learning
    build_accoption: int = 0  # build_stage parameter
    build_locoption: int = 0  # build_stage parameter
    build_soption: int = 0  # build_stage parameter
    compfound: bool = False
    contmrph: bool = False
    delim: Optional[str] = None
    dict: bool = False
    #form: str = None  # FIXME: not used?
    refl: bool = False
    reflex: bool = False

    # VERB state
    pasprt: bool = False
    paspst: str = ""

    r_str = None  # VERBPAR and NOMPAR
    ret_mess: str = "Analysis failed. Press any key..."
    trus: bool = False  # especially for 'трусь'!
    virtbuff: Optional[str] = None

    # ENGSEEK additional return when build_par is True
    engstring: str = ""

    accentless: bool = False
    secform: str = ""
    secend: str = ""

    # SEARCHCASE, NOUNSEEK modify success
    success: bool = False

    dialect: str = ''

    def contret(self):
        """Break the spellchecking process once success is achieved."""
        if self.contmrph:
            self.success = True
            raise SuccessException

    def engseek(self, form: str, bu_ff: Optional[str], build_par: bool) -> bool:
        """Analyze an English word."""

        # local pl, i := 1, k, extlen, _begin, p, finish, next_pl, ;
        #  atdescr, message, subdescr, t_form, okey := False, perhaps, tempmess, q, statmess, ;
        #  newmess, curson, cursoff, oldarea, varmess, messtr, hword, langvar, ;
        #  nmess, recarr, r, iword, rrecords, atpl, ilang
        # LOCAL varia_area, varindex_area
        # LOCAL n_entry

        self.noun_hypotheses = []
        while form[-1] < chr(97):
            form = form[:-1]

        self.noun_hypotheses.append(NounHypothesis(source=form))

        if form.endswith("ly"):
            if not form.endswith("yly"):
                t_source = form[:-2]
                self.noun_hypotheses.append(NounHypothesis(source=t_source))
                if t_source.endswith("l"):
                    self.noun_hypotheses.append(NounHypothesis(source=form[:-2] + "l"))
                elif t_source.endswith("i"):
                    self.noun_hypotheses[-1].source = t_source[:-1] + "y"
                elif t_source.endswith("b"):
                    self.noun_hypotheses.append(NounHypothesis(source=form[:-2] + "le"))

        elif form.endswith("s"):
            if form.endswith("'s"):
                self.noun_hypotheses.append(NounHypothesis(source=form[:-2]))
            else:
                if form.endswith("ys") and voc(form, -3) or form.endswith("oos"):
                    self.noun_hypotheses.append(NounHypothesis(source=form[:-1]))
                elif not form.endswith(("os", "ss", "xs", "shs", "chs", "zs")):
                    self.noun_hypotheses.append(NounHypothesis(source=form[:-1]))
                if form.endswith("es"):
                    if form.endswith(("ses", "xes", "shes", "ches", "zes", "oes")):
                        self.noun_hypotheses.append(NounHypothesis(source=form[:-2]))
                    elif form.endswith("ies"):
                        self.noun_hypotheses.append(NounHypothesis(source=form[:-3] + "y"))

        elif form.endswith(("ed", "ing", "er", "est")):
            if not form.endswith((form, "ieed", "ieing", "ieer", "ieest")):
                extlen = 2 if form.endswith(("ed", "er")) else 3

                self.noun_hypotheses.append(NounHypothesis(source=form[:-extlen] + "e"))

                if form.endswith(("ied", "ier", "iest", "ying")):
                    self.noun_hypotheses.append(NounHypothesis(source=form[:-extlen - 1] + "y"))
                    if form.endswith("ying"):
                        self.noun_hypotheses.append(NounHypothesis(source=(form[:-extlen - 1]
                                                                           + "ie")))
                else:
                    if not (form.endswith(("yed", "yer", "yest")) and
                            not voc(form, -2 - extlen)) and not form.endswith("eed"):
                        t_source = form[:-extlen]
                        self.noun_hypotheses.append(NounHypothesis(source=t_source))

                        if t_source[-1] == t_source[-2]:
                            if not voc(t_source, -1):
                                if voc(t_source, -3):
                                    if (not voc(t_source, -4)) or t_source[-3] == "u":
                                        self.noun_hypotheses.append(
                                            NounHypothesis(source=form[:-extlen - 1])
                                        )

        for hypo in self.noun_hypotheses:
            n_entry = 1

            perhaps = True
            _begin = 1
            statmess = ""

            while perhaps:
                pl = 0
                bu_ff = dict_get_entry(True, hypo.source, n_entry)
                n_entry += 1
                if bu_ff:
                    pl = 1
                    if self.contmrph:
                        if form == hypo.source \
                                or not form.endswith(("ed", "er", "est")):
                            return True
                    if not hypo.mess:
                        p_hypo = hypo
                    else:
                        statmess = hypo.mess
                        p_hypo = NounHypothesis(source=hypo.source)
                        self.noun_hypotheses.append(p_hypo)
                    p_hypo.mess = first(bu_ff[_begin + pl:])

                    if "]" not in p_hypo.mess:
                        q = statmess.find("]")
                        if q >= 0 and p_hypo.source == hypo.source:
                            p_hypo.mess = statmess[statmess.index("["):q + 1] \
                                + ' ' + p_hypo.mess
                    q = p_hypo.mess.find("]")
                    atdescr = (q + 1 + 7) if q >= 0 else (1 + 7)
                    subdescr = p_hypo.mess[:atdescr]

                    if form.endswith("s") and form != p_hypo.source:
                        if " a " in subdescr:
                            statmess = p_hypo.mess
                            p_hypo.mess = ""
                            continue
                    elif form.endswith(("ed", "er", "est", "ing")):
                        extlen = 3 if form.endswith(("est", "ing")) else 2
                        if (" v (" in p_hypo.mess or " a (" in p_hypo.mess) \
                                and form.endswith(("ed", "er", "est")):
                            if form not in p_hypo.mess:
                                # means that the form exists as an alternative:
                                # bereaved / bereft
                                statmess = p_hypo.mess
                                p_hypo.mess = ""
                                continue
                        elif form.endswith(("er", "est")) and \
                                not (" a " in subdescr or " adv" in subdescr):
                            if form != p_hypo.source:
                                statmess = p_hypo.mess
                                p_hypo.mess = ""
                                continue
                        elif (not voc(form, -extlen - 1)
                              and voc(form, (extlen + 2) * -1)
                              and (not voc(form, (extlen + 3) * -1)
                                   or form[-extlen - 2] == "u")):
                            # cases like: dated, mated; eliminated should be
                            # false forms like *wraped or *squated (the latter
                            # one justifies the last condition)
                            if p_hypo.source == form[:-extlen]:
                                if form.endswith(("led", "ling")) \
                                        or elastacc(subdescr):
                                    if not form.endswith(
                                        ("wed", "wing", "wer", "west",
                                         "xed", "xing", "xer", "xest")
                                    ):
                                        statmess = p_hypo.mess
                                        p_hypo.mess = ""
                                        continue
                        elif (not voc(form, (extlen + 1) * -1)
                              and not voc(form, (extlen + 2) * -1)
                              and voc(form, (extlen + 3) * -1)):
                            if p_hypo.source == form[:-extlen-1]:
                                if "'" in subdescr and not elastacc(subdescr):
                                    if not form.endswith(("lled", "lling")):
                                        statmess = p_hypo.mess
                                        p_hypo.mess = ""
                                        continue

                    if self.contmrph:
                        return True

                else:
                    perhaps = False

        if not self.contmrph:
            okey = False
            message = ''
            for hypo in self.noun_hypotheses:
                if hypo.mess:
                    okey = True
                    atpl = hypo.mess.find("#")
                    if atpl >= 0:
                        hypo.mess = hypo.mess[:atpl]

                    if hypo.mess.startswith("+"):
                        hypo.source = hypo.source[0].upper() + hypo.source[1:]
                        hypo.mess = hypo.mess[1:]

                    message += hypo.source + ' ' + hypo.mess + "\r\n"

            varmess = add_varia(form,
                                (h.source
                                 for h in self.noun_hypotheses if h.mess))
            if varmess:
                # let us mark the beginning of this stuff with chr 5
                message += "\x05-----------------\r\n" + varmess
                okey = True

            if okey:
                if build_par:
                    self.engstring = message

        return okey

    def rform(self, string: str) -> str:
        """Return the reflexive suffix if needed."""

        if string and not string.startswith("-") and self.refl:
            return ("сь" if (voc(string, -1)
                             or string.endswith(("а́", "и́", "о́",
                                                 "у́", "ю́", "я́")))
                    else "ся")
        return ""

    def listing(self, form: str) -> Optional[List[str]]:
        """Displaying all solutions generated for an unknown form"""

        '''local soption, accoption, \
           tempsour, tempinf, tempend, tempcas, akz_pl, accarr, q, \
           accarray, demsour
        MEMVAR build_accoption, build_soption, build_stage
        '''

        last_n_hypo: int = 1
        indarr: list = []
        # First hypothesis should be always Indecl.
        indarr.append(form + " н<0>")

        for nhypo in self.noun_hypotheses:
            if nhypo.source and nhypo.ending:
                nindex = nomindex(nhypo.source, nhypo.ending, nhypo.ablaut)
                if nindex:
                    last_n_hypo = buildind(nhypo.source, nindex, indarr)
        for vhypo in self.verb_hypotheses:
            # do not generate irregular verbs
            if vhypo.inf and vhypo.v_type != "@":
                buildvind(vhypo.inf, vhypo.v_type, vhypo.endstem, indarr)

        soption = 1

        while soption > 0:
            if len(indarr) > 0:
                if self.build_stage == 0:
                    return [first(s, "<") if isinstance(hypo, NounHypothesis)
                            else s
                            for s, hypo in zip(indarr, chain(self.noun_hypotheses,
                                                             self.verb_hypotheses))]

                else:
                    soption = self.build_soption
                if soption == 0:
                    break

                tempsour, tempinf = indarr[soption].split(maxsplit=1)

                accoption = 1

                vocals = (voc(tempsour, i + 1) for i in range(len(tempsour)))
                accarray: List[str] = [demacc(tempsour, str(p))
                                       for p, v in enumerate(vocals) if v]
                if self.build_stage == 1:
                    return accarray
                else:
                    accoption = self.build_accoption

                if accoption == 0:
                    continue
                else:
                    r = 0
                    for q in range(len(tempsour)):
                        if voc(tempsour, q):
                            r += 1
                        if r == accoption:
                            akz_pl = q
                            break

                if soption > last_n_hypo:  # A verbal hypothesis:
                    if self.build_stage >= 2:
                        return self.showvpar(tempsour, tempinf, akz_pl, form)
                    self.showvpar(tempsour, tempinf, akz_pl, form)
                else:
                    tempinf = first(tempinf, "<")
                    tempend = rest(indarr[soption], "<")
                    tempend = first(tempend, ">")
                    tempcas = re.match(r"^.*\{([^}]*)\}",
                                       indarr[soption]).group(1)  # type: ignore
                    accarr = [""]
                    if ' п' in indarr[soption]:
                        if akz_pl == len(tempsour) - 1:
                            accarr = ["в"]
                            if not self.polyvoc(tempsour):
                                accarr = ["а"]
                        else:
                            accarr = ["а"]
                    elif ' 8' in indarr[soption]:
                        accarr = ["а", "в'", "е", "F''"]
                    elif ' с' in indarr[soption]:
                        if lastacc(tempsour, akz_pl):
                            if self.polyvoc(tempsour):
                                accarr = ["в", "D", "F"]
                            else:
                                accarr = ["а", "с", "е", "в", "D", "F"]
                        else:
                            accarr = ["а", "с", "е"]
                    elif ' ж' in indarr[soption]:
                        if lastacc(tempsour, akz_pl):
                            if self.polyvoc(tempsour):
                                accarr = ["в", "D", "D'", "F", "F'"]
                            else:
                                accarr = ["а", "е", "в", "D", "D'", "F", "F'"]
                        else:
                            accarr = ["а", "е"]
                    elif ' м' in indarr[soption]:
                        if lastacc(tempsour, akz_pl):
                            if self.polyvoc(tempsour):
                                accarr = ["а", "в", "с", "D"]
                            else:
                                accarr = ["а", "в", "с", "D", "е", "F"]
                        else:
                            accarr = ["а", "е", "F"]
                    if self.build_stage >= 2:
                        if accarr == [""]:  # Indecl.:
                            return tempsour + " " + akz_pl + " н"
                        else:
                            return self.showpar(tempsour, tempinf, tempcas,
                                                tempend, accarr, akz_pl)
                    self.showpar(tempsour, tempinf, tempcas,
                                 tempend, accarr, akz_pl)

            else:
                break

        return None

    def noun(self, form: str, bu_ff: Optional[str]) -> None:
        """Noun analysis program."""

        #FINNUM++  FIXME: ?

        self.reflex = False
        if len(form) > 4:
            if form.endswith(('ишь', 'ешь')) \
                    and form not in ('плешь', 'флешь', 'брешь', 'тишь'):
                return None
            if form.endswith(('ься', 'тся', 'лся',
                              'тесь', 'лась', 'лось', 'лись')):
                return None

        lastlet = form[-1]
        stem = form[:-1]
        endroot = stem[-1:] or '^'
        second = stem[-2:-1] or '^'

        self.send(form, '0', False)

        if len(form) <= 2 and form != 'щи':
            if form in ('б', 'в', 'ж', 'к', 'с'):  # предлоги, частицы (csend их не обрабатывает)
                self.noun_hypotheses.append(NounHypothesis(source=form, ending='0', ablaut=False))
            else:
                self.send(form + 'а', '0', False)

        elif form.endswith(('ами', 'ам', 'ах')) and len(form) > (3 if form.endswith('ами') else 2):
            e_nd = 'ами' if form.endswith('ами') else form[-2:]
            stem = form[:-len(e_nd)]
            if not (voc(stem, -1) or stem.endswith(('ь', 'й'))):
                s_stem = irregular(z_irrinf2, z_irrstr2, stem, self.contmrph)
                if s_stem:
                    del self.noun_hypotheses[-1]
                    t_abl = (s_stem in 'цветок церковь ребеночек бесенок чертенок щенок щеночек')
                    if stem == 'суд':
                        self.send('суд', e_nd, False)
                    self.send(s_stem, e_nd, t_abl)
                    self.nounseek(False, bu_ff, form)
                    return None
                self.send(stem, e_nd, False)        # суп -ами
                if len(stem) >= 2 and (yer(stem) or not voc(stem, -2)):
                    # FIXME: this fixes parsing ледами as лёд Dp; recheck
                    self.send(plene(stem), e_nd, True)    # конец - концами
                    if not yer(stem):
                        self.send(pleno(stem), e_nd, True)    # щенок - щенками
                self.send(stem + 'а', e_nd, False)        # рука - руками
                self.send(stem + 'о', e_nd, False)        # село - селами
                if stem.endswith(('к', 'г', 'х')):
                    self.send(stem + 'и', e_nd, False)        # ботинки - ботинками
                elif not stem.endswith(('ч', 'ж', 'ш', 'щ')):
                    self.send(stem + 'ы', e_nd, False)        # кеды - кедами
                if stem.endswith(('ч', 'ж', 'ш', 'щ')):
                    self.send(stem + 'ь', e_nd, False)    # ночь - ночами
                    self.send(stem + 'и', e_nd, False)    # онучи - онучами
                    if len(stem) >= 2 and not voc(stem, -2) and not yer(stem):
                        self.send(pleno(stem) + 'ь', e_nd, True)    # только ложь, рожь, вошь
                    self.send(stem + 'е', e_nd, False)    # ложе - ложами
                elif stem.endswith('ц'):
                    self.send(stem + 'е', e_nd, False)    # солнце - солнцами
                if stem.endswith(('ят', 'ат')):
                    t_stem = stem[:-2]
                    self.send(t_stem + ('онок' if stem.endswith('ат') else 'енок'), e_nd, True)
                elif stem.endswith(('ятк', 'атк')):
                    t_stem = stem[:-3]
                    self.send(t_stem + ('оночек' if stem.endswith('атк') else 'еночек'), e_nd, True)
                elif stem.endswith('мен'):
                    self.send(stem[:-3] + 'мя', e_nd, False)
                elif stem.endswith(('ан', 'ян', 'ар', 'яр', 'господ')):
                    self.send(stem + 'ин', e_nd + '*', False)
                    # дополнительная помета при окончаниях у слов типа
                    # "боярами"; сличается с пометой "**" в словаре - чтобы
                    # не было случаев "сахара > сахарин".

        elif form.endswith(('ями', 'ям', 'ях')):
            e_nd = 'ами' if form.endswith('ями') else ('а' + form[-1])
            stem = form[:-len(e_nd)]
            if not stem.endswith(('ч', 'ш', 'ж', 'щ', 'к', 'г', 'х')):
                s_stem = irregular(z_irrinf3, z_irrstr3, stem)
                if s_stem:
                    if stem not in ('детк', 'щенятк'):
                        if not (stem in ('дет', 'люд') and e_nd == 'ами'):
                            self.contret()
                            # FINNUM--
                            t_abl = s_stem.endswith(('ок', 'нь', 'ье'))
                            self.send(s_stem, e_nd, t_abl)
                            bu_ff = chr(13) + chr(10) + s_stem    # FIXME: ???
                            if stem != 'зор':
                                self.nounseek(False, bu_ff, form)
                                return None
                        else:
                            return None

                if not stem.endswith(('к', 'г', 'х', 'ц', 'ч', 'ш', 'ж', 'щ')):
                    if stem.endswith('ь'):
                        base2 = stem[:-1]
                        s_stem = irregular(z_irrinf1, z_irrstr1, base2, self.contmrph)
                        if s_stem:
                            self.contret()
                            # FINNUM--
                            t_abl = False
                            if s_stem.startswith('шури'):  # FIXME: What's this?
                                s_stem = 'шурин'
                            elif s_stem.startswith('дно'):
                                t_abl = True
                            self.send(s_stem, e_nd, t_abl)
                            if s_stem.startswith('дно'):
                                self.send("донья", e_nd, True)
                            self.nounseek(False, bu_ff, form)
                            return None

                        self.send(stem[:-1] + 'ей', e_nd, True)    # ручей - ручьями
                        self.send(stem + 'е', e_nd, False)        # копье - копьями
                        self.send(stem + 'я', e_nd, False)        # гостья - гостьями

                    else:
                        if not voc(stem, -1):
                            self.send(stem + 'ь', e_nd, False)        # конь - конями
                            if len(stem) >= 2 and not voc(stem, -2):
                                self.send(plene(stem) + 'ь', e_nd, True)    # корень - корнями
                                if not yer(stem):
                                    self.send(pleno(stem) + 'ь', e_nd, True)    # огонь - огнями
                        else:
                            self.send(stem + 'й', e_nd, False)        # герой - героями
                        self.send(stem + 'я', e_nd, False)        # доля - долями
                        self.send(stem + 'е', e_nd, False)        # поле - полями
                        self.send(stem + 'и', e_nd, False)        # сласти - сластями

        elif form.endswith(('ыми', 'ими', 'ым', 'им', 'ых', 'их')):
            e_nd = 'ыми' if form.endswith(('ыми', 'ими')) else ('ы' + form[-1])
            stem = form[:-len(e_nd)]
            if form.endswith(('ыми', 'ых', 'ым')):
                if not stem.endswith(('к', 'г', 'х', 'ч', 'ш', 'ж', 'щ')):
                    self.send(stem + 'ый', e_nd, False, True)  # старый - старыми
                    self.send(stem + 'ой', e_nd, False)        # молодой - молодыми
                    self.send(stem + 'ая', e_nd, False)        # столовая - столовыми
                    self.send(stem + 'ое', e_nd, False)        # сказуемое - сказуемыми
                    self.send(stem + 'ые', e_nd, False)        # озимые - озимыми
                if stem.endswith(('ин', 'ын', 'ов', 'ев')):
                    self.send(stem, e_nd, False)
            else:
                base = stem[:-1] if stem.endswith('ь') else stem
                self.send(base + 'ий', e_nd, False)  # лисий - лисьими; синий - синими

                if form.endswith(("ращим", "тащим", "ощим", "рщим", "лущим")) or form == "плющим":
                    self.secform = form + "ый"
                    self.secend = '0'
                elif form.endswith("им") and not form.endswith(("щим", "шим")):
                    # FIXME: *-вшим were not parsed
                    self.secform = form + "ый"
                    self.secend = '0'
                else:
                    self.secform = base + "ий"
                    self.secend = e_nd

                self.send(base + ('ая' if base.endswith(('к', 'г', 'х', 'ч', 'ш', 'ж', 'щ'))
                                  else 'яя'), e_nd, False)  # рабочая, передняя
                self.send(base + ('ое' if base.endswith(('к', 'г', 'х')) else 'ее'),
                          e_nd, False)  # легкое, подлежащее (есть, правда (1 случай): чужое)
                self.send(base + 'ие', e_nd, False)  # окружающие - окружающими
                if base.endswith(('к', 'г', 'х', 'ч', 'ш', 'ж')):
                    self.send(base + 'ой', e_nd, False)  # сухой, большой
                if stem.endswith('ь'):
                    self.send(stem + 'я', e_nd, False)  # ничья - ничьими
                    self.send(stem + 'е', e_nd, False)  # третье - третьими

        elif lastlet == 'а':
            if voc(stem, -1) or stem.endswith('ь'):
                return None
            self.send(form, 'а', False)
            s_stem = irregular(z_irrinf4, z_irrstr4, stem, self.contmrph)
            if s_stem:
                # FINNUM--
                t_abl = (s_stem in ('бесенок', 'чертенок', 'щенок'))
                if s_stem == 'господь':
                    self.send('господин', 'ы', t_abl)
                    self.send('господь', 'а', t_abl)
                else:
                    self.send(s_stem, 'а', t_abl)
            else:
                if endroot not in 'ьй':
                    if not voc(endroot, 1):
                        if stem == 'суд':
                            self.send('судно', 'а', False)
                        self.send(stem, 'а', False)             # сук - сука
                        self.send(stem + 'о', 'а', False)     # окно - окна
                        if endroot in 'чжшщц':
                            self.send(stem + 'е', 'а', False)     # ложе - ложа
                        if second in 'йь':
                            self.send(plene(stem), 'а', True)    # хорек - хорька
                        elif not voc(second, 1):  # FIXME: this fixes parsing леда as лёд Gs, recheck
                            self.send(plene(stem), 'а', True)    # конец - конца
                            self.send(pleno(stem), 'а', True)    # щенок - щенка

                        if form in ('мала', 'солона'):
                            s_stem = irregular(z_irrinf9, z_irrstr9, form[:-1], self.contmrph)
                            self.send(s_stem, 'а', False)

                        if form.endswith(('ята', 'ата')):
                            self.send(form[:-3] + ('енок' if form.endswith('ята')
                                                   else 'онок'), 'ы', True)
                        if form.endswith('мена'):
                            self.send(form[:-3] + 'я', 'на', False)

                        self.send(stem + 'ой', 'а', False)        # молодой - молода
                        t_stem = stem + ('ий' if endroot in 'кгхчжшщ' else 'ый')
                        self.send(t_stem,
                                  'а', False, (endroot in 'тм'))  # мягкий - мягка, старый - стара
                        if endroot == 'н':
                            self.send(stem + 'ный', 'а', False, True)
                        if stem.endswith(('ин', 'ын', 'ов', 'ев')):
                            self.send(stem, 'ого', False)
                            self.send(stem, 'ая', False)

        elif lastlet == 'в':
            base = form[:-2]
            if form in ('цветов', 'хозяев', 'судов'):
                self.contret()
                # FINNUM--
                s_stem = irregular(z_irrinf5, z_irrstr5, base, self.contmrph)
                if s_stem == 'цветок':
                    t_abl = True
                else:
                    t_abl = False
                if s_stem == 'судно':
                    t_end = '0'
                else:
                    t_end = 'ов'
                self.send(s_stem, t_end, t_abl)
                if s_stem == 'судно':
                    s_stem = 'суд'    # FIXME: Looks like dead code... -- Ph. maybe lost send?

            if form.endswith('ев') and not base.endswith(('ч', 'ж', 'ш', 'щ')):
                if voc(second, 1):
                    self.send(base + 'й', 'ов', False)  # герой - героев
                    self.send(base + 'и', 'ов', False)  # пропилеи - пропилеев
                    if base.endswith('и'):
                        self.send(stem, 'ов', False)  # острие - остриев

                elif second == 'ь':

                    base2 = base[:-1]

                    s_stem = irregular(z_irrinf1, z_irrstr1, base2)
                    if s_stem:
                        if base2 not in ('муж', 'девер', 'друз', 'сынов'):
                            self.contret()
                            # FINNUM--
                            t_abl = False
                            t_end = 'ов'

                            if s_stem.startswith('шури'):  # FIXME: what's this?
                                s_stem = 'шурин'

                            if z_irrinf1.find(' ' + s_stem + ' ') >= 167:
                                t_end = '0'
                                t_abl = False

                            self.send(s_stem, t_end, t_abl)

                            self.nounseek(False, bu_ff, form)
                            return None

                    self.send(base[:-1] + 'ей', 'ов', True)  # улей - ульев
                    self.send(stem, 'ов', False)             # платье - платьев
                    self.send(base + 'я', 'ов', False)       # хлопья - хлопьев

                elif second == 'ц':
                    self.send(base, 'ов', False)        # месяц - месяцев
                    self.send(plene(base), 'ов', True)  # палец - пальцев
                    self.send(stem, 'ов', False)        # болотце - болотцев
                    self.send(base + 'ы', 'ов', False)  # шлепанцы - шлепанцев
                    self.send(base + 'а', 'ов', False)  # только (?) щупальца - щупальцев

                self.send(form, 'ый', False)

            if form.endswith('ов') and not base.endswith(('ч', 'ж', 'ш', 'щ', 'ь')):
                self.send(base, 'ов', False)            # завод - заводов

                self.send(stem, 'ов', False)            # облако - облаков
                self.send(base + 'а', 'ов', False)      # потроха - потрохов
                if base.endswith(('к', 'г', 'х')):
                    self.send(base + 'и', 'ов', False)  # ноготки - ноготков
                else:
                    self.send(base + 'ы', 'ов', False)  # пары - паров
                if len(base) >= 2 and (yer(base) or not voc(base, -2)):  # FIXME: this fixes parsing ледов as лёд Gp; recheck
                    if not yer(base):
                        self.send(pleno(base), 'ов', True)  # щенок - щенков
                    self.send(plene(base), 'ов', True)      # хорек - хорьков
                # self.send(form, 'ый', False)  # FIXME: What's this?

                if base.endswith('ц'):
                    self.send(base + 'е', 'ов', False)  # деревце - деревцов

        elif lastlet == 'е':

            dofurther = True

            s_stem = irregular(z_irrin10, z_irrst10, form) \
                or (form.startswith("по") and irregular(z_irrin10, z_irrst10, form[2:])) \
                or ''
            if s_stem:
                # dofurther deals with cases like пожиже, поуже etc.
                if self.compfound:
                    dofurther = False
                else:
                    self.compfound = True
                self.contret()
                if ' ' + form + ' ' in z_irrst10:
                    # 20081207: commented out by Phil; not sure why the previous
                    # hypothesis should be dropped, but it breaks analysis of уже as
                    # indecl.
                    # FINNUM--
                    self.send(s_stem, 'ее', False)

            if dofurther:

                if endroot in 'иыоежлрцчшщ':
                    t_end = 'о '
                    if endroot == 'о':
                        t_end = 'ое'
                    elif endroot == 'ы':
                        t_end = 'ые'
                    self.send(form, t_end, False)

                base = stem[:-1]
                if voc(endroot, 1):
                    if endroot in 'аеоу':
                        self.send(stem + 'я', 'е', False)     # фея - фее
                        self.send(stem + 'й', 'е', False)     # чародей - чародее
                    if endroot == 'ы':
                        if form == 'вые':
                            self.send('выя', 'е', False)
                        elif second not in 'кгхцчжшщ':
                            self.send(base + 'ый', 'ые', False, True)    # старый - старые
                            # *self.secform = base + 'ый'
                            self.send(base + 'ой', 'ые', False)    # молодой - молодые
                            self.send(base + 'ая', 'ые', False)    # прямая - прямые
                            self.send(base + 'ое', 'ые', False)    # земноводное - земноводные

                    elif endroot == 'о':
                        if second not in 'чжшщ':
                            t_form = base + ('ий' if second in 'кгх' else 'ый')
                            self.send(t_form, 'ое', False, True)    # мягкий-мягкое, старый-старое
                            self.send(base + 'ой', 'е', False)  # герой - герое
                        self.send(base + 'ой', 'ое', False)    # молодой - молодое

                    elif endroot == 'е':

                        if second not in 'кгх':
                            if not self.compfound:
                                self.send(base + 'ий', 'ое', False, True)  # синий - си/нее
                                self.send(base + 'ее', 'ое', False)    # млекопитающее!
                            self.compfound = True
                            self.send(base + 'ий', 'ее', False)    # синий - сине/е
                            self.send(base + 'ый', 'ее', False)    # старый - старее
                            self.send(base + 'ой', 'ее', False)    # больной - больнее

                    elif endroot == 'и':
                        if base == 'к':
                            self.send(base + 'ий', 'е', False)     # только кий - кие
                        else:
                            self.send(base + 'ий', 'ые', False, True)    # синий - синие
                            self.send(base + ('ая' if second in 'кгхчжшщ' else 'яя'),
                                      'ые', False)  # русская - русские, передняя-передние
                            self.send(base + ('ое' if second in 'кгх' else 'ее'),
                                      'ые', False)  # легкое - легкие, горючее-горючие
                            if second in 'кгхчшжщ':
                                self.send(form, 'ые', False)  # легкие
                                self.send(base + 'ой',
                                          'ые', False)  # большой - большие, сухой - сухие

                else:
                    if endroot == 'ь':
                        self.send(base + 'ей', 'е', True)    # репей - репье
                        self.send(stem + 'я', 'е', False)    # статья - статье
                        self.send(base + 'ий', 'ое', False)  # лисий - лисье
                        self.send(stem + 'е', 'е', False)    # ущелье - ущелье
                        self.send(stem + 'е', 'о', False)
                        self.send(stem + 'е', 'ое', False)   # третье - третье
                    else:
                        self.send(stem, 'е', False)          # сук - суке
                        if endroot not in 'кгх':
                            self.send(stem + 'ь', 'е', False)   # конь - коне
                        if stem.endswith('христ'):
                            self.send(stem + 'ос', 'е', False)
                        if second + endroot in 'ан ян ар яр':   # боярин - бояре
                            if stem == 'цыган':
                                self.send(stem, 'ы', False)
                            else:
                                self.send(stem + 'ин', 'ы*', False)
                        if not voc(second, 1):
                            self.send(plene(stem), 'е', True)     # палец - пальце
                            if endroot not in 'кгх':
                                self.send(plene(stem) + 'ь', 'е', True)    # корень - корне
                            if not yer(stem):
                                self.send(pleno(stem), 'е', True)         # кусок - куске
                                if endroot not in 'кгх':
                                    self.send(pleno(stem) + 'ь', 'е', True)    # ноготь - ногте
                        self.send(stem + 'о', 'е', False)             # окно - окне
                        self.send(stem + 'а', 'е', False)             # рука - руке
                        if endroot in 'чжшщцрл':
                            if endroot == 'щ':
                                self.secform = stem + 'ий'
                                self.secend = 'о '
                            self.send(stem + 'е', 'е', False)         # море - море (loc.)
                        if endroot not in 'чжшщцкгх':
                            self.send(stem + 'я', 'е', False)            # петля - петле
                    if endroot in 'чжш':
                        self.compfound = True
                        self.send(base + depal(endroot) + 'ий', 'ее', False)     # мягкий - мягче
                        self.send(base + depal(endroot) + 'ой', 'ее', False)     # сухой - суше
                if stem.endswith(('н', 'ж', 'ч', 'ш', 'щ')):
                    self.send(stem + 'ий', 'о ', False)                # синий - сине

        elif lastlet == 'и':
            self.send(form, 'ы', False)

            s_stem = irregular(z_irrinf3, z_irrstr3, stem, self.contmrph)
            if s_stem:
                # FINNUM--
                if s_stem in 'полузабытье':
                    t_end = 'е'
                else:
                    t_end = 'ы'
                t_abl = (s_stem in 'щеночек ребеночек ребенок ')

                if s_stem in 'ухо око колено':
                    t_end = 'а'
                self.send(s_stem, t_end, t_abl)
                if stem != 'зор':
                    self.nounseek(False, bu_ff, form)
                    return None

            if form.endswith('ьми'):
                stem = form[:-3]

                s_stem = irregular(z_irrinf6, z_irrstr6, stem, self.contmrph)
                if s_stem:
                    self.contret()
                    # FIXME: Was it just an optimization?
                    # FINNUM--
                    self.send(s_stem, 'ами', (stem == 'дет'))
                self.nounseek(False, bu_ff, form)
                return None

            if form.endswith(('атки', 'ятки')):
                base = form[:-4]
                self.send(base + ('оночек' if form.endswith('атки') else 'еночек'), 'ы', True)
            elif form.endswith('мени'):
                self.send(form[:-3] + 'я', 'ы', False)

            base = form[:-2]
            if voc(endroot, 1):
                self.send(stem + 'й', 'ы', False)  # герой - герои
                self.send(stem + 'я', 'ы', False)  # фея - феи
                if endroot == 'и':
                    self.send(stem + 'й', 'е', False)  # сценарий - сценарии
                    self.send(stem + 'е', 'е', False)  # открытие - открытии
                    self.send(stem + 'я', 'е', False)  # лекция - лекции
            else:
                if endroot == 'ь':
                    self.send(base + 'ей', 'ы', True)    # улей - ульи
                    self.send(stem + 'я', 'ы', False)    # статья - статьи
                    self.send(base + 'ий', 'ые', False)  # лисий - лисьи
                    self.send(stem + 'е', 'ые', False)   # только (?) третье - третьи
                    self.send(stem + 'я', 'ые', False)   # ничья - ничьи
                else:
                    self.send(stem if endroot in 'кгх' else (stem + 'ь'),
                              'ы', False)  # сук - суки, ночь-ночи
                    if endroot in 'чжшщ':
                        self.send(stem, 'ы', False)
                    if len(stem) >= 2 and not voc(stem, -2):
                        self.send(plene(stem) if endroot in 'кгх' else (plene(stem) + 'ь'),
                                  'ы', True)  # конек - коньки, корень - корни
                        if not yer(stem):
                            self.send(pleno(stem) if endroot in 'кгх' else (pleno(stem) + 'ь'),
                                      'ы', True)  # щенок - щенки, ноготь - ногти
                    self.send(stem + ('а' if endroot in 'кгхчжшщ' else 'я'),
                              'ы', False)  # рука-руки, петля-петли
                    if endroot in 'кгхчжшщ':
                        self.send(stem + 'о', 'ы', False)  # яблоко - яблоки
                        self.send(stem + 'ий', 'ы', False, True)  # мягкий - мягки
                        self.send(stem + 'ой', 'ы', False)  # сухой

        elif lastlet == 'й':
            if voc(endroot, 1):
                if form.endswith(("ой", "ый", "ий")):
                    self.send(form, "ый", False, True)
                if self.dialect == 'aos' and form.endswith("ей"):
                    self.send(form, "ый", False, True)
                if endroot != 'ы' or stem == 'вы':
                    self.send(stem + 'я', '0', False)    # фея - фей
                if endroot == 'и':
                    self.send(stem + 'е', '0', False)    # здание - зданий

                if form.endswith('ей'):
                    stem = form[:-2]
                    s_stem = irregular(z_irrinf3, z_irrstr3, stem)
                    if s_stem:
                        if stem not in ('забыть', 'детк', 'щенятк', 'зор'):
                            self.contret()
                            # FINNUM--
                            t_end = 'ов' if ' ' + stem + ' ' in z_irrstr3[:36] else 'ей'
                            t_abl = (s_stem in ('полдень', 'ребенок'))
                            self.send(s_stem, t_end, t_abl)
                            self.nounseek(False, bu_ff, form)
                        return None
                    else:

                        if voc(form, -3):
                            self.send(stem + 'я', 'ой', False)   # фея - феей
                            self.compfound = True
                            self.send(stem + 'ий', 'ее', False)  # длинношеий - длинношеей
                            self.send(stem + 'ий', 'ой', False)  # id.
                        elif form[-3] != 'ь':
                            if stem == 'друз':
                                self.send('друг', 'ов', False)
                            elif stem == 'сынов':
                                self.send('сын', 'ов', False)
                            elif stem == 'колен':
                                self.send('колено', 'ей', False)
                            else:
                                self.send(stem + 'ь', 'ов', False)  # конь - коней
                                self.send(stem + 'и', 'ов', False)  # сани - саней
                                self.send(stem + 'и', 'ей', False)  # корчи - корчей
                            self.send(stem + 'ье', '0', True)       # питье - питей
                            self.send(stem + 'ья', '0', True)       # статья - статей
                            if len(stem) < 2 or not voc(stem, -2):
                                self.send(plene(stem) + 'ь', 'ов', True)  # корень - корней
                                if not yer(stem):
                                    self.send(pleno(stem) + 'ь', 'ов', True)  # ноготь - ногтей

                            self.send(stem + ('а' if stem.endswith(('ч', 'ж', 'ш', 'щ', 'ц'))
                                              else 'я'), 'ой', False)  # куча - кучей, буря - бурей
                            self.send(stem + ('а' if stem.endswith(('ч', 'ж', 'ш', 'щ', 'ц'))
                                              else 'я'), 'ей', False)  # gen. pl.
                            if stem.endswith(('ч', 'ж', 'ш', 'щ', 'ц')):
                                self.send(stem, 'ов', False)  # харч - харчей
                            self.send(stem + 'е', 'ей', False)  # море - морей
                            self.send(stem + 'ий', 'ее', False)  # синий - синей (comp.;gen. etc. )
                            self.compfound = True
                            self.send(stem + 'ий', 'ой', False, True)
                            if not stem.endswith(('ч', 'ж', 'ш', 'щ', 'ц', 'к', 'г', 'х')):
                                self.compfound = True
                                self.send(stem + 'ый', 'ее', False)  # мудрый - мудрей
                                self.send(stem + 'ой', 'ее', False)  # седой - седей
                            elif stem.endswith(('ч', 'ж', 'ш', 'щ')):
                                self.send(stem + 'ая', 'ой', False)  # рабочая - рабочей
                        if stem.endswith('ь'):
                            self.send(stem[:-1] + 'ий', 'ой', False)  # лисий - лисьей
                            self.send(stem[:-1] + 'ья', 'ой', False)  # ничья - ничьей
                elif form.endswith('ой'):
                    stem = form[:-2]
                    if not (voc(stem, -1) or stem.endswith('ь')):
                        self.send(stem + 'а', 'ой', False)  # вода-водой
                        t_stem = stem + ('ий' if stem.endswith(('ч', 'ж', 'ш', 'щ', 'к', 'г', 'х'))
                                         else 'ый')
                        self.send(t_stem, 'ой', False)     # мягкий - мягкой, новый - новой
                        if len(form) > 3 and (form.endswith(('емой', 'имой', 'нной', 'той'))):
                            if not form.endswith('стой'):
                                self.secform = t_stem
                                self.secend = 'ой'
                        self.send(stem + 'ая', 'ой', False)  # глухая - глухой
                        if stem.endswith(('ин', 'ын', 'ов', 'ев')):
                            self.send(stem, 'ой', False)
                elif form.endswith('ий'):
                    stem = form[:-2]
                    self.send(stem + 'ья', '0', True)  # гостья - гостий
                    self.send(stem + 'ье', '0', True)  # копье - копий

        elif lastlet == 'м':
            stem = form[:-2]

            s_stem = irregular(z_irrinf7, z_irrstr7, stem)
            if s_stem:
                if (form.endswith('ем') and stem in 'полудн полым') or \
                        (form.endswith('ом') and stem in 'господ христ'):
                    self.contret()
                    # FINNUM--
                    t_abl = (s_stem == 'полдень')
                    self.send(s_stem, 'ом', t_abl)
                    self.nounseek(False, bu_ff, form)
                    return None

            if form.endswith('ем'):
                if voc(stem, -1):
                    self.send(stem + 'й', 'ом', False)  # герой - героем
                    if stem.endswith('и'):
                        self.send(stem + 'е', 'ом', False)  # здание - зданием
                else:
                    if stem.endswith('ь'):
                        self.send(stem + 'е', 'ом', False)        # копье - копьем
                        self.send(stem[:-1] + 'ий', 'ом', False)  # лисий - лисьем
                        self.send(stem[:-1] + 'ье', 'ом', False)  # третье - третьем
                        self.send(stem[:-1] + 'ей', 'ом', True)   # улей - ульем
                    else:
                        self.send(stem if stem.endswith(('ч', 'ж', 'ш', 'щ', 'ц'))
                                  else (stem + 'ь'), 'ом', False)  # плач - плачем, конь-конем
                        if stem.endswith('ц'):
                            self.send(stem + 'ый', 'ом', False)  # куцый - куцем
                        else:
                            if stem.endswith(('ч', 'ж', 'ш', 'щ', 'н')) and len(stem) > 1:
                                self.send(stem+'ий', 'ом', False, True)  # горячий - горячем
                                self.send(stem+'ее', 'ом', False)        # горючее - горючем
                        self.send(stem + 'е', 'ом', False)               # море - морем
                        if len(stem) > 1 and not voc(stem, -2):
                            self.send(plene(stem) if stem.endswith('ц') else (plene(stem) + 'ь'),
                                      'ом', True)
                            # горец - горцем, корень - корнем
                            if not yer(stem) and not stem.endswith(('ч', 'ж', 'ш', 'щ', 'ц')):
                                self.send(pleno(stem) + 'ь', 'ом', True)  # ноготь - ногтем
                if form.endswith('менем'):
                    self.send(form[:-5] + 'мя', 'ом', False)

            elif form.endswith('ом'):
                if not (voc(stem, -1) or stem.endswith('ь')):
                    self.send(stem, 'ом', False)        # сук - суком
                    self.send(plene(stem), 'ом', True)  # конец - концом
                    if not yer(stem):
                        self.send(pleno(stem), 'ом', True)  # щенок - щенком
                    self.send(stem + 'о', 'ом', False)      # окно - окном
                    if not stem.endswith(('ч', 'ж', 'ш', 'щ', 'ц')):
                        t_stem = stem + ('ий' if stem.endswith(('к', 'г', 'х')) else 'ый')
                        self.send(t_stem, 'ом', False)  # мягкий-мягком, старый -старом
                        if t_stem.endswith(('нный', 'мый', 'тый')):
                            self.secform = t_stem
                            self.secend = 'ом'
                    self.send(stem + 'ой', 'ом', False)  # молодой - молодом
                    self.send(stem + 'ое', 'ом', False)  # легкое - легком

        elif lastlet == 'о':
            self.send(form, 'о ', False)
            t_stem = stem + ('ий' if endroot in 'кгхчщшж' else 'ый')
            self.send(t_stem, 'о ', False)  # мягкий-мягко, старый-старо

            if ' ' + form + ' ' in ' мало солоно ':
                s_stem = irregular(z_irrinf9, z_irrstr9, form[:-1])
                self.send(s_stem, 'о', False)

            if endroot in 'тм':
                self.secform = t_stem
                self.secend = 'о '
            self.send(stem + 'ой', 'о ', False)  # молодой - молодо
            if endroot == 'н':
                self.send(stem + 'ный', 'о ', False, True)
                if form.endswith(('ино', 'ыно')):
                    self.send(stem, 'ое', False)
            if form.endswith(('ово', 'ево')):
                self.send(stem, 'ое', False)
            if len(form) > 3 and form.endswith('ого'):
                stem = form[:-3]
                if voc(stem, -1) or stem.endswith('ь'):
                    return None
                endroot = stem[-1]
                self.send(stem + 'ой', 'ого', False)  # молодой - молодого
                self.send(stem + 'ое', 'ого', False)  # животное - животного
                t_stem = stem + ('ий' if endroot in 'кгх' else 'ый')
                self.send(t_stem, 'ого', False, True)  # мягкий - мягкого, старый - старого
                if stem.endswith(('ин', 'ын')):
                    self.send(stem, 'ого', False)
            elif len(form) > 3 and form.endswith('его'):
                stem = form[:-3]
                endroot = stem[-1]
                if endroot == 'ь':
                    self.send(stem[:-1] + 'ий', 'ого', False)  # лисий - лисьего
                    self.send(stem[:-1] + 'ье', 'ого', False)  # третье - третьего
                else:
                    t_stem = stem + ('ый' if endroot == 'ц' else 'ий')
                    self.send(t_stem, 'ого', False, True)  # куцый - куцего, хороший-хорошего
                    #*self.secform = t_stem
                    self.send(stem + 'ее', 'ого', False)  # среднее - среднего

        elif lastlet == 'т':
            if form.endswith(('ят', 'ат')):
                if form in 'бесенят чертенят щенят':
                    self.contret()
                    self.send(form[:-4] + 'енок', 'ов', True)
                else:
                    stem = form[:-2]
                    self.send(stem + ('онок' if form.endswith('ат') else 'енок'), 'ов', True)

        elif lastlet == 'у':
            if voc(stem, -1) or stem.endswith('ь'):
                return None

            s_stem = irregular(z_irrinf7, z_irrstr7[:16], stem, self.contmrph)
            if s_stem:
                # FINNUM--
                self.send(s_stem, 'у', False)
                self.nounseek(False, bu_ff, form)
                return None

            self.send(stem, 'у', False)  # сук - суку
            if yer(stem) or not voc(second, 1):  # FIXME: this fixes parsing леду as лёд G2,Ds,L2; recheck
                self.send(plene(stem), 'у', True)  # конец - концу
                if not yer(stem):
                    self.send(pleno(stem), 'у', True)  # кусок - куску
            self.send(stem + 'а', 'у', False)    # рука - руку
            self.send(stem + 'о', 'у', False)    # окно - окну
            if endroot in 'чшжщц':
                self.send(stem + 'е', 'у', False)    # солнце - солнцу, ложе - ложу
            if stem.endswith(('ин', 'ын', 'ов', 'ев')):
                self.send(stem, 'ому', False)
                self.send(stem, 'ую', False)

            if len(form) > 3 and form.endswith(('ому', 'ему')):
                stem = form[:-3]
                if form.endswith('ому'):
                    if voc(stem, -1) or stem.endswith('ь'):
                        pass
                    else:
                        self.send(stem + 'ой', 'ому', False)  # молодой-молодому
                        t_stem = stem + ('ий' if stem.endswith(('к', 'г', 'х')) else 'ый')
                        self.send(t_stem, 'ому', False, True)  # мягкий-мягкому, старый-старому
                        self.send(stem + 'ое', 'ому', False)    # животное - животному
                        if stem.endswith(('ин', 'ын')):
                            self.send(stem, 'ому', False)            # дядин - дядиному
                else:
                    if stem[-1] == 'ц':
                        self.send(stem + 'ый', 'ому', False)    # куцый - куцему
                    elif stem.endswith('ь'):
                        self.send(stem[:-1] + 'ий', 'ому', False)    # лисий - лисьему
                        self.send(stem[:-1] + 'ье', 'ому', False)    # третье - третьему
                    else:
                        self.send(stem + 'ий', 'ому', False, True)     # синий - синему
                        self.send(stem + 'ее', 'ому', False)     # среднее - среднему

        elif lastlet == 'ы':
            self.send(form, 'ы', False)
            if not (endroot in 'кгхчшжщь' or voc(endroot, 1)):
                s_stem = irregular(z_irrinf8, z_irrstr8, stem, self.contmrph)
                if s_stem:
                    # FINNUM--
                    t_abl = (s_stem == 'цветок')
                    self.send(s_stem, 'ы', t_abl)
                    self.nounseek(False, bu_ff, form)
                    return None

                self.send(stem, 'ы', False)        # завод - заводы
                if yer(stem) or not voc(second, 1):  # FIXME: this fixes parsing леды as лёд Np, Api; recheck
                    self.send(plene(stem), 'ы', True)  # конец - концы
                    if not yer(stem):
                        self.send(pleno(stem), 'ы', True)     # свекор - свекры
                self.send(stem + 'а', 'ы', False)         # вода - воды
                self.send(stem + 'ый', 'ы', False, True)  # старый - стары
                self.send(stem + 'ой', 'ы', False)        # молодой - молоды
                if endroot == 'н':
                    self.send(stem + 'ный', 'ы', False)
                    self.secform = stem + 'ный'
                if stem.endswith(('ин', 'ын', 'ев', 'ов')):
                    self.send(stem, 'ые', False)

        elif lastlet == 'ь':

            if endroot not in "чжшщкгх":
                self.send(stem + 'я', '0', False)            # буря - бурь
                if stem == 'кухон':
                    self.send('кухня', '0', True)
                elif stem == 'зор':
                    self.send('заря', '0', False)
                else:
                    if second == 'е':
                        self.send(stem[:-2] + endroot + 'я', '0', True)  # земля - земель
                        self.send(stem[:-2] + endroot + 'и', '0', True)  # капли - капель
                    if endroot in 'нc':
                        self.send(stem + 'ий', '0', False)         # синий - синь

        elif lastlet == 'ю':
            if endroot not in "чшжщкгх":
                if voc(endroot, 1):
                    self.send(stem + 'й', 'у', False)         # герой - герою
                    self.send(stem + 'я', 'у', False)         # фея - фею
                    self.send(stem + 'е', 'у', False)         # здание - зданию
                    if form.endswith(('ую', 'юю', 'ою', 'ею')):
                        stem = form[:-2]
                        if form.endswith('ую'):
                            t_stem = stem + ('ий' if stem.endswith(('ч', 'ж', 'ш', 'щ',
                                                                    'к', 'г', 'х')) else 'ый')
                            self.send(t_stem, 'ую', False, True)  # мягкий-мягкую, старый-старую
                            self.send(stem + 'ой', 'ую', False)   # молодой - молодую
                            self.send(stem + 'ая', 'ую', False)   # рабочая - рабочую
                        elif form.endswith('юю'):
                            self.send(stem + 'ий', 'ую', False)     # синий - синюю
                            self.send(stem + 'яя', 'ую', False)     # передняя - переднюю
                        elif form.endswith('ею'):
                            if stem.endswith('ц'):
                                self.send(stem + 'ый', 'ою', False)         # куцый - куцею
                                self.send(stem + 'а', 'ою', False)            # летчица - летчицею
                            else:
                                self.send(stem + 'ий', 'ою', False, True)         # синий - синею
                                self.send(stem + 'яя', 'ою', False)         # передняя - переднею
                                self.send(stem + ('а' if stem.endswith(('ч', 'ж', 'ш', 'щ'))
                                                  else 'я'),
                                          'ою', False)  # куча - кучею, буря - бурею
                        else:
                            t_stem = stem + ('ий' if stem.endswith(('к', 'г', 'х', 'ч', 'ж', 'ш',
                                                                    'щ')) else 'ый')
                            self.send(t_stem, 'ою', False)  # мягкий-мягкою, старый-старою
                            if len(form) > 3 and (form.endswith(('емою', 'имою', 'нною', 'тою'))):
                                if not form.endswith('стою'):
                                    self.secform = t_stem
                                    self.secend = 'ою'
                            self.send(stem + 'ой', 'ою', False)  # молодой - молодою
                            self.send(stem + 'ая', 'ою', False)  # больная - больною
                            self.send(stem + 'а', 'ою', False)   # вода - водою
                else:

                    if endroot == 'ь':

                        if form == 'тысячью':
                            self.contret()
                            # FINNUM--
                            self.send('тысяча', 'ою', False)
                            self.nounseek(False, bu_ff, form)
                            return None

                        self.send(stem[:-1] + 'ий', 'ую', False)    # лисий - лисью
                        self.send(stem + 'я', 'ую', False)        # ничья - ничью, семья - семью
                        self.send(stem + 'я', 'у', False)
                        # store 'у' to ending[4], ending[6], ending[7]  # FIXME: what's this?
                        self.send(stem, 'ью', False)         # ночь - ночью
                        self.send(stem + 'е', 'у', False)  # копье - копью
                        self.send(stem[:-1] + 'ей', 'у', True)     # ручей - ручью
                    else:
                        self.send(stem + 'ь', 'у', False)        # конь - коню
                        self.send(stem + 'я', 'у', False)        # буря - бурю
                        self.send(stem + 'е', 'у', False)        # горе - горю
                        if not voc(second, 1):
                            self.send(plene(stem) + 'ь', 'у', True)     # корень - корню
                            if stem == 'полудн':
                                self.send('полдень', 'у', True)
                            if not yer(stem):
                                self.send(pleno(stem) + 'ь', 'у', True)  # ноготь - ногтю

        elif lastlet == 'я':
            if endroot not in "чшжщкгх":
                if form.endswith('ся') and ('щ' in form[-6:] or 'ш' in form[-6:]):
                    if not form.endswith(('ася', 'ыся', 'эся')):
                        self.reflex = True
                        if 'щ' in form[-6:]:
                            e_nd = form[form.rfind('щ') + 1:]
                        else:
                            e_nd = form[form.rfind('ш') + 1:]
                        e_nd = e_nd[:-2]
                        if e_nd.startswith('и'):
                            e_nd = 'ы' + e_nd[1:]
                        elif e_nd.startswith('е'):
                            e_nd = 'о' + e_nd[1:]
                        if e_nd not in 'ая ой ую ою ое':
                            self.send(form[:-len(e_nd) - 2] + 'ийся', e_nd, False)
                        if e_nd not in 'ый ое ого ому':
                            self.send(form[:-len(e_nd) - 2] + 'аяся', e_nd, False)
                        if e_nd not in 'ая ый ой ую ою':
                            self.send(form[:-len(e_nd) - 2] + 'ееся', e_nd, False)
                        if e_nd not in 'ый ая ое ой ого ому ым ою':
                            self.send(form[:-len(e_nd) - 2] + 'иеся', e_nd, False)
                        self.secform = form[:-len(e_nd) - 2] + 'ийся'
                        self.secend = e_nd

                else:

                    self.send(form, 'а', False)
                    if voc(endroot, 1):
                        self.send(stem + 'й', 'а', False)
                        if form.endswith(('ая', 'яя')):
                            stem = form[:-2]
                            if form.endswith('ая'):
                                t_stem = stem + ('ий' if stem.endswith(('ч', 'ж', 'ш', 'щ',
                                                                        'к', 'г', 'х')) else 'ый')
                                self.send(t_stem,
                                          'ая', False, True)  # мягкий - мягкая, старый - старая
                                self.send(form, 'ая', False)
                                self.send(stem + 'ой', 'ая', False)    # молодой - молодая
                            else:
                                self.send(stem + 'ий', 'ая', False)    # синий - синяя
                                self.send(form, 'ая', False)
                    else:
                        if endroot == 'ь':
                            base2 = stem[:-1]

                            s_stem = irregular(z_irrinf1, z_irrstr1, base2, self.contmrph)
                            if s_stem:
                                # FINNUM--
                                if s_stem.startswith('шури'):  # FIXME: what's this?
                                    s_stem = 'шурин'
                                if base2 + ' ' in z_irrstr1[:168]:
                                    t_end = 'ы'
                                else:
                                    t_end = 'а'
                                if s_stem == 'дно':
                                    t_abl = True
                                else:
                                    t_abl = False
                                self.send(s_stem, t_end, t_abl)
                                if s_stem == 'дно':
                                    self.send("донья", 'а', True)

                            self.send(base2 + 'ий', 'ая', False)  # лисий - лисья
                            self.send(base2 + 'ей', 'а', True)    # ручей - ручья
                            self.send(stem + 'е', 'а', False)     # копье - копья
                            self.send(base2 + 'ья', 'ая', False)  # ничья

                        else:
                            self.send(stem + 'ь', 'а', False)     # конь - коня
                            if not voc(second, 1):
                                if stem == 'полудн':
                                    self.send('полдень', 'а', True)
                                else:
                                    self.send(plene(stem) + 'ь', 'а', True)    # корень - корня
                                if not yer(stem):
                                    self.send(pleno(stem) + 'ь', 'а', True)    # ноготь - ногтя
                    if endroot not in 'аеоуыэюя':
                        self.send(stem + 'е', 'а', False)         # горе - горя
                    if endroot not in 'аоуыэюя':
                        self.send(form[:-1] + 'ий', 'а', False)   # синий - синя

        if not voc(lastlet, 1) and lastlet not in 'йь':

            stem = form[:-1]
            endroot = stem[-1:] or '^'
            second = stem[-2:-1] or '^'
            base = stem[:-1]

            s_stem = irregular(z_irrinf9, z_irrstr9, form, self.contmrph)
            if s_stem:
                # FINNUM--
                if form in 'небес чудес':
                    self.send(form + 'а', '0', False)
                elif form in 'деток щеняток':
                    self.send(s_stem, '0', True)
                elif s_stem in 'недостойный':
                    self.send(s_stem, '0', True)
                else:
                    self.send(s_stem, '0', False)
                self.nounseek(False, bu_ff, form)
                return None

            else:
                self.send(form + 'а', '0', False)            # дама - дам, ворота - ворот
                self.send(form + 'о', '0', False)            # село - сел
                self.send(form + ('и' if lastlet in 'кгхчжшщ' else 'ы'),
                          '0', False)  # вилы - вил, брюки - брюк
                if lastlet in 'чжщц':
                    if form == "яиц":
                        self.send("яйцо", '0', True)
                    else:
                        self.send(form + 'е', '0', False)       # солнце - солнц
                t_stem = form + ('ий' if lastlet in 'кгхжчшщ' else 'ый')
                self.send(t_stem, '0', False)  # рыжий - рыж, старый - стар
                if not self.secform or (t_stem.endswith(("омый", "емый")) and
                                        not t_stem.endswith(("номый", "томый", "момый",
                                                             "шемый", "щемый"))):
                    self.secform = t_stem
                if not self.secend:
                    self.secend = '0'
                self.send(form + 'ой', '0', False)  # молодой - молод
                if form.endswith(('ан', 'ян', 'ар', 'яр', 'господ')):
                    self.send(form + 'ин', 'ов*', False)
                elif form.endswith(('ын', 'ин', 'ов', 'ев')):
                    self.send(form, 'ый', False)

                if endroot in 'ое':
                    if len(form) > 3 or form == 'зол':
                        if endroot == 'о':
                            if lastlet == 'к' or (lastlet in 'глн' and second in 'лрхкг'):
                                self.send(base + lastlet + 'а', '0', True)  # кукла - кукол
                                self.send(base + lastlet + ('и' if lastlet in 'чжшщкгх' else 'ы'),
                                          '0', True)  # мохны - мохон, санки - санок
                                self.send(base + lastlet + 'о', '0', True)  # окно - окон
                                self.send(base + lastlet + ('ий' if lastlet in 'кгхчжшщ' else 'ый'),
                                          '0', True)  # мягкий - мягок, полный - полон
                            elif form == 'зол':
                                self.send('зло', '0', True)
                                self.send('злой', '0', True)
                            if lastlet == 'н' and second == 'ш':
                                self.send(base + lastlet + 'ой', '0', True)  # смешной - смешон
                            if form.endswith(('яток', 'аток')):
                                stem = form[:-4]
                                self.send(stem + ('оночек' if form.endswith('аток') else 'еночек'),
                                          'ов', True)
                        elif endroot == 'е':
                            if lastlet == 'н':
                                add_let = ('ь' if second == 'л' else '')
                                if voc(second, 1):
                                    add_let = 'й'
                                self.send(base + add_let + lastlet + 'я',
                                          '0', True)  # спальня - спален, бойня - боен
                                # pl.t. здесь как будто бы только ставни - ставен (наряду с ставней)
                                self.send(base + add_let + lastlet + 'а',
                                          '0', True)  # копна - копен
                                self.send(base + add_let + lastlet + 'ий',
                                          '0', True)  # излишний - излишен, бескрайний - бескраен
                                self.send(base + add_let + lastlet + 'ый',
                                          '0', True)  # славный - славен, стройный - строен
                                self.send(base + add_let + lastlet + 'ой', '0',
                                          True)  # чудной - чуден, больной - болен
                                self.send(base + add_let + lastlet + 'о',
                                          '0', True)  # полотно - полотен
                                self.send(base + add_let + lastlet + 'ы',
                                          '0', True)  # ножны - ножен
                            else:
                                if not voc(second, 1) and second not in 'чжщшцкгх':
                                    if lastlet in 'бмкгц':
                                        self.send(base + 'ь' + lastlet + 'а',
                                                  '0', True)  # серьга - серег
                                        if form == 'писем':
                                            self.send('письмо', '0', True)
                                        self.send(base + 'ь' + lastlet +
                                                  ('и' if lastlet in 'кг' else 'ы'),
                                                  '0', True)  # деньги - денег, пяльцы - пялец
                                        self.send(base + lastlet + ('и' if lastlet in 'кг'
                                                                    else 'ы'),
                                                  '0', True)  # сумерки - сумерек
                                    if lastlet == 'к' and second in 'рн':
                                        self.send(base + 'ь' + lastlet + ('ий' if lastlet == 'к'
                                                                          else 'ый'),
                                                  '0', True)  # горький - горек
                                    if lastlet in 'глрц' and second in 'пбвдзнрст':
                                        self.send(base + lastlet + 'о', '0', True)  # ведро - ведер
                                        self.send(base + lastlet + 'а',
                                                  '0', True)  # дверца - дверец
                                        self.send(base + lastlet + 'ый',
                                                  '0', True)  # хитрый - хитер
                                    if lastlet == 'ц':
                                        self.send(base + 'ь' + lastlet + 'о',
                                                  '0', True)  # сельцо - селец
                                        self.send(base + 'ь' + lastlet + 'е',
                                                  '0', True)  # рыльце - рылец
                                        self.send(base + lastlet + 'е',
                                                  '0', True)  # блюдце - блюдец
                                else:
                                    add_let = ('й' if voc(second, 1) else '')
                                    self.send(base + add_let + lastlet + 'а',
                                              '0', True)  # кошма - кошем, кайма - каем
                                    self.send(base + add_let + lastlet + 'о',
                                              '0', True)  # окошко - окошек
                                    self.send(base + add_let + lastlet + ('и' if lastlet in 'кгх'
                                                                          else 'ы'),
                                              '0', True)  # штанишки - штанишек, обойки - обоек
                                    self.send(base + add_let + lastlet +
                                              ('ий' if lastlet in 'кгхчжшщ' else 'ый'),
                                              '0', True)  # сторожкий - сторожек
                                    if base.endswith(("ж", "ч", "ш")):  # что есть кроме рожка?
                                        self.send(base + 'о' + lastlet, '0', True)  # рожок - рожек
            if form in 'семян стремян':
                self.send(form[:-1], 'ов', False)
            elif form.endswith('мен') and form not in 'семен темен стремен':
                self.send(form[:-3] + 'мя', 'ов', False)
            if lastlet == 'н':
                self.send(form + 'ный', '0', True, True)
                self.send(form + 'ний', '0', True)
                if form.endswith(('ин', 'ын')):
                    self.send(form, 'ый', False)

        self.nounseek(False, bu_ff, form)
        return None

    def nounseek(self, usdic, bu_ff: Optional[str], form: str):
        """Test the generated nominal hypotheses against dictionary data."""

        # LOCAL allbegin, i, z, perhaps, _begin, ;
        #  n, pl, next_pl, inf_string, plurtant, p_1, m_part, p_2, genus, ;
        #  _class, t_acc, comp, alter, start, c_str, e_str, q
        # LOCAL n_entry
        # MEMVAR contmrph, reflex, success

        if self.virtbuff:
            bu_ff = self.virtbuff

        if not self.noun_hypotheses:
            return None

        anim: bool = False
        shock: bool = False
        subst: bool = True
        nonnom: bool = False
        infor: str = ""
        ad_len: int = 0
        if self.reflex:
            form = form[:-2]

        # FIXME: ifdef USE_DICT_HEADERS
        # Seems to be an optimization to filter out hypotheses non-existing in
        # dictionary. Makes no sense now
        #  allbegin = bu_ff.find("\r\n" + commonprefix([h.source for h in self.noun_hypotheses], 1))
        #  if allbegin >= 0:
        if True:
            for i, hypo in enumerate(self.noun_hypotheses, start=(1 if self.contmrph else 0)):
                if hypo.ending:  # means that the form was really generated:
                    if not usdic:
                        n_entry = 1

                    perhaps = True

                    nhypo = hypo

                    while perhaps:
                        bu_ff = dict_get_entry(False, hypo.source, n_entry)
                        n_entry += 1
                        if bu_ff:
                            if nhypo.res_str:
                                nhypo.succnom = True
                                break

                            nhypo.source = hypo.source
                            anim = shock = nonnom = False
                            subst = True

                            inf_string = bu_ff

                            if nhypo.res_str:
                                # слово обнаружено в prstring; проверить, нет ли дубликата
                                nhypo.succnom = True
                                nhypo = NounHypothesis()
                                self.noun_hypotheses.append(nhypo)
                                continue

                            infor = inf_string.lstrip()

                            nhypo.n_acc_pl = first(infor)
                            nhypo.mess = infor = rest(infor)

                            infor = first(infor, "\x04")
                            infor = first(infor, " % ")

                            if "#6" in infor:  # дождина etc.
                                infor = "ж 1а"

                            if "*" in hypo.ending:
                                # обработка окончания (в случае с "сахар-а" etc.)
                                hypo.ending = hypo.ending.replace('*', '')
                                if "**" not in infor:
                                    continue

                            # бросаем первый взгляд на infor

                            if infor[:2] in 'н н;н:' or \
                                    infor[:3] + ' ' in '(_б (_ч пре сою час меж сра (_н ' or \
                                    ' 0' in infor:
                                # Несклоняемое слово

                                if form == nhypo.source and hypo.ending == '0':

                                    if "без удар" in infor:
                                        self.accentless = True

                                    nhypo.res_str = ' Indecl.'
                                    nhypo.succnom = True
                                    self.success = True

                                    if self.contmrph:
                                        return None

                                    nhypo = NounHypothesis()
                                    self.noun_hypotheses.append(nhypo)
                                continue
                            elif hypo.ending == '0' and voc(form, len(form)):
                                # Неправильная гипотеза - типа "мама, 0" -
                                # можно уже не рассматривать
                                continue

                            elif "св " in infor or "св, " in infor:
                                # попали на глагол - можно не продолжать
                                continue

                                # обработаем plur. tant.

                            elif infor.startswith('мн.') or '!!мн' in infor:
                                plurtant = True
                                if infor.startswith('мн.'):
                                    if infor.startswith('мн. _от_'):
                                        infor = infor[8:].lstrip()
                                        if infor.find(' ') > 2:
                                            infor = rest(infor).lstrip()
                                    elif infor.startswith('мн. неод.'):
                                        infor = infor[9:].lstrip()
                                    elif infor.startswith('мн. одуш.'):
                                        infor = infor[9:].lstrip()
                                        anim = True
                                    else:
                                        infor = infor[3:].lstrip()

                            else:
                                plurtant = False

                            # выясняем часть речи (_class), одушевленность (anim) и
                            # субстантивированность у прил./мест. (subst)

                            p_1 = first(infor)
                            q = p_1.find("#")
                            if q >= 0:
                                p_1 = p_1[:q]
                                ad_len = 5
                            m_part = rest(infor).lstrip() if ' ' in infor else ''
                            if m_part and m_part[0] in '0123456789':
                                p_2 = ''
                            else:
                                p_2 = first(m_part) if ' ' in m_part else ''
                            genus = ' '.join((p_1, p_2)).strip()

                            if 'п' in genus or 'мс' in genus:
                                _class = 'п'
                                if 'о' in genus:
                                    anim = True
                                elif plurtant:
                                    pass
                                elif '-п' in genus or 'п мс' in genus:
                                    subst = False
                                elif not ('м' in genus or 'ж' in genus or 'с' in genus):
                                    subst = False
                            elif 'жо' in genus:
                                _class = 'жо'
                            elif 'со' in genus:
                                _class = 'со'
                            elif 'мо' in genus:
                                _class = 'мо'
                            elif 'с' in genus:
                                _class = 'с'
                            else:
                                # FIXME: process secondary gender
                                _class = first(genus.replace(",", ""), "//")

                            # выясняем акц.парадигму (nhypo.accent) и ту ее часть (t_acc), которая
                            # существенна для анализа

                            nhypo.accent = \
                                infor[len(genus) + ad_len + 3 - 1:].replace('*', '').lstrip()

                            if nhypo.accent[1:2] == '/':
                                if "''" in nhypo.accent:
                                    nhypo.accent = nhypo.accent[:5]
                                elif "'" in nhypo.accent:
                                    nhypo.accent = nhypo.accent[:4]
                                else:
                                    nhypo.accent = nhypo.accent[:3]
                            elif nhypo.accent[2:3] == "'":
                                nhypo.accent = nhypo.accent[:3]
                            else:
                                nhypo.accent = nhypo.accent[:2 if nhypo.accent[1:2] == "'" else 1]
                            t_acc = nhypo.accent[:1]

                            # акцентные проверки:

                            if hypo.source.endswith(('ча', 'жа', 'ша', 'ща', 'я')):
                                if _class in 'жо' and hypo.ending == '0' and \
                                        not form.endswith(('ей', 'ий')):
                                    if not (t_acc in 'аD' or
                                            form in 'шестерен простынь госпож деревень'):
                                        continue

                            if hypo.ending == '0' or hypo.ending == 'ей':
                                q = infor.find("_Р. мн._")
                                if q >= 0:
                                    if form != "ребенок":  # individual case: ребенок - детей
                                        # no loop should be done
                                        if form not in deacc(infor[q + 9:]):
                                            continue
                                else:
                                    if hypo.source.endswith(('ья', 'ье')):
                                        if form.endswith('ий') and t_acc not in 'аD':
                                            continue
                                        elif form.endswith('ей') and t_acc in 'аD':
                                            continue

                            elif hypo.ending in 'ой ом ою ов':
                                if t_acc not in 'асе':
                                    if form[-len(hypo.ending) - 1:][:2] in 'чежешещеце':
                                        if not (_class in 'мо жо' and form.endswith('ей') and
                                                hypo.ending == 'ов'):
                                            # этому условию удовлетворяют только
                                            # формы типа вшей, лжей, левшей
                                            continue
                                else:
                                    if form[-len(hypo.ending) - 1:][:2] in 'чожошощоцо':
                                        continue

                            # проверки на совместимость извлеченных из словаря классов
                            # и сгенерированных окончаний

                            if _class != 'п':

                                if _class in 'жо':
                                    if hypo.ending == 'ей' and t_acc in 'аD':
                                        if '"2"' not in infor:
                                            continue
                                    if '"2"' in infor and '"2"]' not in infor and \
                                            hypo.ending == '0':
                                        continue
                                elif _class in 'со':
                                    if hypo.ending == 'ей' and t_acc in 'аD':
                                        continue
                                    elif '8' in infor and hypo.ending.startswith('ы'):
                                        if 'мени' not in form:
                                            continue

                                if hypo.source.endswith('й'):
                                    if not form.endswith(("й", "я", "ю", "е", "ем", "и", "ев",
                                                          "ям", "ях", "ями")):  # *герой-*геры
                                        continue
                                    elif hypo.ending == '0' and len(form) < len(hypo.source):
                                        continue
                                    elif form.endswith("я") and not yer(form) and not voc(form, -2):
                                        # гений - *геня:
                                        continue
                                    elif form.endswith("ем") and not yer(form) and \
                                            not voc(form, -3):
                                        # гений - *генем, кремний - *кремнем:
                                        continue
                                elif hypo.source.endswith("ая") and form.endswith(("ой", "ою")):
                                    continue

                            else:

                                if "нет_" in infor or infor.endswith("- "):
                                    if "_пф нет_" in infor and len(hypo.ending.strip()) > 1:
                                        continue
                                    elif ("кф м нет_" in infor or infor.endswith("- ")) and \
                                            hypo.ending == '0':
                                        continue
                                    elif "кф и сравн. нет_" in infor and \
                                            hypo.ending + ' ' in '0 а о ее ':
                                        continue

                                if "!!ф." in infor and hypo.ending in ("0", "а", "о", "ее"):
                                    continue

                                if "@" in infor:
                                    if hypo.ending == 'ее':
                                        q = infor.find("_сравн._")
                                        if q >= 0:
                                            comp = first(infor[q + 8:], ';').strip()
                                            if form not in (
                                                f.strip()
                                                for f in deacc(re.sub(r' *\(_[^)]*_\) *', ' ', comp)
                                                               .replace(' _и_ ', '//')).split('//')
                                            ):
                                                continue

                                if 'мс' in genus:
                                    if not nhypo.source.endswith(('н', 'в')):
                                        if 'мс-п' not in genus:
                                            if hypo.ending != 'ый' and 'ь' not in form[-4:]:
                                                continue
                                    elif hypo.ending == 'е' and "п " not in genus:  # топтыгине:
                                        hypo.ending = 'ом'
                                    elif hypo.ending == 'ом' and "п " not in genus:  # *топтыгином:
                                        continue
                                else:
                                    if form.endswith(('ья', 'ью', 'ьи', 'ье', 'ьим', 'ьих', 'ьем',
                                                      'ьей', 'ьими', 'ьего', 'ьему')):
                                        continue

                                if nhypo.source.endswith('нный'):
                                    if '"1"]' in infor:
                                        if form.endswith('нн'):
                                            continue
                                        elif hypo.ending != '0' and 'нн' not in form:
                                            continue
                                    elif '"1"' in infor:
                                        if hypo.ending == '0' and nhypo.source != form + 'ный':
                                            continue
                                        elif hypo.ending != '0' and 'нн' not in form:
                                            continue
                                    elif '"2"' in infor:
                                        if form.endswith(('нна', 'нно', 'нны')):
                                            continue
                                        elif hypo.ending == '0' and nhypo.source != form + 'ный':
                                            continue
                                    elif hypo.ending in 'а о ы ' and 'нн' not in form:
                                        continue
                                    elif hypo.ending == '0' and not form.endswith('нен'):
                                        continue

                                if hypo.ending == '0':
                                    if form.endswith("й"):
                                        continue
                                    elif '?' in infor:
                                        continue
                                elif hypo.ending in 'аы':
                                    if len(form) >= len(hypo.source):
                                        if not ('мс' in genus or 'пф нет' in infor):
                                            continue
                                elif hypo.ending == 'ее':
                                    if '~' in infor:
                                        continue

                            # проверяем беглый гласный

                            alter = (('*' in infor and not ('**' in infor or ' *' in infor)) or
                                     ('3**' in infor))

                            if not alter and hypo.ablaut:
                                continue
                            elif alter and hypo.ablaut:

                                if form.endswith(("шек", "жек", "чек")):
                                    if hypo.ending == '0':
                                        if t_acc not in 'аD':
                                            continue
                                        nonnom = True
                                    else:
                                        shock = True
                                elif form.endswith(("шок", "жок", "чок")):
                                    if hypo.ending == '0':
                                        if t_acc in 'аD':
                                            continue
                                    else:
                                        shock = True

                            elif alter and not hypo.ablaut:
                                if not plurtant:
                                    if _class in 'мо' and hypo.ending != '0':
                                        continue
                                    elif '8' in infor and "#8" not in infor:
                                        if hypo.ending not in '0ью':
                                            continue
                                    elif _class in 'жосоп' and hypo.ending == '0':
                                        continue
                                elif hypo.ending == '0':
                                    continue

                            nhypo.end_str = ''
                            nhypo.case_str = (z_a_cases if _class.startswith("п") else z_cases)

                            if plurtant:
                                start = (80 if _class.startswith("п") else 30)

                            if _class + ' ' in 'мо жо со ':
                                anim = True

                            if _class in 'мо':
                                nhypo.end_str = z_m_str
                                if '"1"]' in infor:
                                    nhypo.end_str = nhypo.end_str + "а  а  "
                                elif '"1"' in infor:
                                    nhypo.end_str = nhypo.end_str.replace("ы", "а")
                                if '"2"]' in infor:
                                    nhypo.end_str = nhypo.end_str + "      0  0  "
                                elif '"2"' in infor:
                                    nhypo.end_str = nhypo.end_str.replace("ов", "0 ")
                            elif _class in 'жо':
                                nhypo.end_str = (z_f_str2 if '8' in infor else z_f_str)
                            elif _class in 'со':
                                nhypo.end_str = (z_men_str if '8' in infor else z_n_str)
                                if '["1"' in infor:
                                    nhypo.end_str = nhypo.end_str + 'ы  ы  '
                                elif '"1"' in infor:
                                    nhypo.end_str = nhypo.end_str.replace("а  0", "ы  0")
                                if '"2"]' in infor:
                                    nhypo.end_str = nhypo.end_str.replace("ей", "ов")
                                elif '"2"' in infor:
                                    nhypo.end_str = nhypo.end_str.replace("0  ей", "ов   ")
                                if genus.startswith('мо со'):
                                    nhypo.end_str = nhypo.end_str.replace("о  о", "о  а")

                            elif _class.startswith('п'):
                                if not subst:
                                    nhypo.end_str = z_a_str
                                else:
                                    nhypo.end_str = z_a_str[:-20]
                                if '-п' in genus or 'мс' in genus:
                                    nhypo.end_str = z_a_str[:-20]

                            if plurtant:
                                nhypo.end_str = nhypo.end_str[start:]
                                nhypo.case_str = nhypo.case_str[start:]

                            if ' ' + hypo.ending + ' ' in ' ' + nhypo.end_str:
                                self.contret()
                                c_str = nhypo.case_str
                                e_str = nhypo.end_str
                                if _class != 'п' and \
                                        (' ' + nhypo.source + ' ' in z_irrinf + " рожок сапожок "):
                                    r_str = nompar(nhypo.source, infor, c_str,
                                                   e_str, t_acc, nhypo.n_acc_pl,
                                                   form,
                                                   contmrph=self.contmrph)[1]
                                    # FIXME: Here the original code could get
                                    # source[n] changed by nompar (reflexive suffix
                                    # cutoff). Not sure if it made any sense
                                    if r_str:
                                        self.success = True
                                    nhypo.res_str = r_str
                                else:
                                    nhypo.res_str = searchcase(hypo.ending, c_str, e_str,
                                                               nonnom=nonnom, shock=shock,
                                                               infor=infor, subst=subst,
                                                               genus=genus, _class=_class,
                                                               anim=anim, form=form)

                            if nhypo.res_str:
                                self.success = True
                                nhypo.succnom = True

                        else:
                            perhaps = False
                        nhypo = NounHypothesis()
                        self.noun_hypotheses.append(nhypo)
        return self.success

    def polyvoc(self, string: str) -> bool:
        """Return True if the string contains more than 1 vowel.

        Sets self.accentless flag if the string contains no vowels.
        """

        counter = polyvocal(string)
        if counter == 0:
            self.accentless = True
        return counter > 1

    def rmorph(self, fform: str,
               delim: str = None,
               full: bool = False,
               build_par: bool = False,
               transinf: bool = False,
               virtbuff: Optional[str] = None):
        '''
        Basic morphological analysis, both for Russian and English.
        The parameters signify the following:
        1) FFORM is the form passed to RMORPH. if no other parameters:
           are specified, RMORPH produces a formatted screen output of
           the generated analysis.
        2) if the parameter DELIM is passed, the function output is:
           formatted as a string with delimiters ( = DELIM).
        3) if the parameter FULL is passed, the output is just as in [2], :
           but includes complete information from Zalizniak's dictionary.
        4) if the parameter BUILD_PAR is specified, the:
           function performs a complete paradigm construction for all
           successfully generated source forms.
        5) The parameter virtbuff may contain a morphological index compatible
           with Zaliznyak's notation. if it is the case, this information:
           is substituted for the information obtained from dictionary files:
           useful to build particular paradigms from particular sources.
        '''

        make: bool = True
        noshow: bool = False
        bu_ff: Optional[str] = None
        t_buff: Optional[str] = None

        self.virtbuff = virtbuff or ''
        form = fform
        locsucc = False

        is_english: bool = not is_cyrillic(form[0])

        if len(form) > 3:
            if form.startswith(('по-', 'кое-')):
                form = rest(form, '-')
            elif form.endswith(('-то', '-ка', '-же')):
                form = form[:-3]
            elif form.endswith(('-либо', 'ибудь')):
                form = re.sub(r'-либо|-нибудь', '', form)
        if self.contmrph and len(form) < 3:
            return True if delim is None else ""
        if form[0].isupper() and not is_english:
            return True if delim is None else ""

        po_removed: bool = False
        while True:

            # Variable self.secform is a copy of FORM for replacement operations

            self.secform = form
            self.secend = ""
            make = True
            if not is_english:
                # Processing irregular forms contained in PRSTRING
                pr_hypo = prstring_get(form, only_success=self.contmrph)
                if pr_hypo:
                    if self.contmrph:
                        self.success = True
                        break

                    self.noun_hypotheses.append(pr_hypo)

                    spaced_form = ''.join((' ', form, ' '))
                    if spaced_form not in z_exstring:
                        make = False

                    self.success = True
                    if not isinstance(delim, list) and not make:
                        if delim == "\r\n":
                            return ''.join("%s %s; " % (cc, sf)
                                           for cs, sf in grouper(pr_hypo.mess.split(' '), 2)
                                           for cc in cs.split(","))
                        return "%s [%s ]%s" % (pr_hypo.source, pr_hypo.mess, pr_hypo.res_str)
                    if form == "кое":
                        self.noun_hypotheses.append(NounHypothesis(source=form, mess="част.",
                                                                   succnom=True))

            if make:
                if not is_english:
                    bu_ff = dict_get_entry(is_english, form, 1)
                else:
                    bu_ff = dict_get_entry(is_english, re.sub(r"['-]", "", form), 1)

                    self.success = self.engseek(form, bu_ff, build_par)
                    if not self.success:
                        try:
                            self.success = self.engseek(form,
                                                        self.user_dictionary_subset(form),
                                                        build_par)
                        except SuccessException:
                            self.success = True

                        if not self.success and not self.contmrph:
                            if build_par:
                                self.engstring = "Not found"
                                raise Exception("Not found")
                    break

                if self.contmrph:
                    if bu_ff:
                        self.success = True
                        break

                self.noun(form, bu_ff)  # Noun analysis

                if not self.success:  # forms like сомненья:
                    if re.search(r"нье|нья|ньи|нью", form):
                        form = form.replace("нь", "ни")
                        continue

                # forms like покороче need to be processed once more
                # FIXME: just once - added po_removed flag to fix parsing попобеднее as бедный Cmp
                # FIXME: not just "е" but "ей" as well
                if form.startswith("по") \
                        and form.endswith(("е", "ей")) \
                        and self.compfound \
                        and not po_removed:
                    form = form[2:]
                    po_removed = True
                    secform_before_po_removal: str = self.secform
                    secend_before_po_removal: str = self.secend
                    continue

                if self.contmrph and self.success:
                    break

                if po_removed:
                    # FIXME: the po removal is for noun only. recheck
                    self.secform = secform_before_po_removal
                    self.secend = secend_before_po_removal
                    form = "по" + form

                locsucc = self.verb(form, bu_ff)
                if self.contmrph and self.success:
                    break

                llocsucc = False

                if not locsucc or form.endswith('м'):  # CHANGED:
                    # with locsucc (after a successful verbal analysis) the only
                    # case to check once more is the short form of the present
                    # passive participle
                    if self.secform.endswith(('щийся', 'шийся', 'нный', 'ущий', 'ющий',
                                              'ащий', 'ящий', 'вший', 'бший', 'гший',
                                              'дший', 'зший', 'кший', 'пший', 'рший',
                                              'сший', 'тший', 'хший', 'тый', 'мый')):
                        if self.secform.startswith("не"):  # latest addition! - S.A.S:
                            self.secform = self.secform[2:]
                            # FIXME: here the original logic seems to be
                            # "first_off = findoffset(form, dict_subset(secform))"
                            bu_ff = dict_get_entry(is_english, form, 1)

                        llocsucc = self.verb(self.secform, bu_ff)

                if locsucc or llocsucc:
                    self.success = True

                if not self.success and len(form) > 2:
                    try:
                        t_buff = self.user_dictionary_subset(form)
                    except SuccessException:
                        self.success = True

                    if not self.success and t_buff:
                        nsucc = self.nounseek(True, t_buff, form)
                        vsucc = self.verbseek(True, t_buff, form)
                        if nsucc or vsucc:
                            self.success = True

            if self.contmrph:
                break

            if delim is not None and not isinstance(delim, list):
                noshow = False
                break
            elif isinstance(delim, list):
                noshow = True
            else:
                noshow = False

            if not self.contmrph:

                # It seems the following code holds an assertion that delim is list
                assert isinstance(delim, list)

                qresults = []

                for vhypo in (h for h in self.verb_hypotheses if h.succverb):
                    if vhypo.inf:
                        qresult = QResult(
                            infin=vhypo.inf + (self.rform(vhypo.inf)
                                               if vhypo.voice.startswith('act.')
                                               else ''),
                            inform=vhypo.info,
                            v_pl_ac=vhypo.v_acc_pl,
                        )
                        qresults.append(qresult)

                        if not noshow:
                            dresult = DResult(cargo=['verb',
                                                     demacc(qresult.infin, qresult.v_pl_ac),
                                                     len(qresults)])
                        else:
                            dresult = DResult(
                                source=fform,
                                sform=qresult.infin,
                                info=vhypo.info,
                                aspect="i" if vhypo.info.startswith("нсв ") else "p",
                                trans="i" if " нп " in vhypo.info or self.refl else "t",
                            )
                        partcond = ((vhypo.is_participle or vhypo.gender.startswith('p.'))
                                    and vhypo.voice.startswith('act.')
                                    and form.endswith("мый"))
                        dresult.voice = vhypo.voice
                        if vhypo.mood != 'ger.' and not partcond:
                            dresult.mood = vhypo.mood
                            if vhypo.mood.startswith('inf.'):
                                dresult.pspeech = 'v.'
                            elif vhypo.mood.startswith('imp.'):
                                dresult.person = '2'
                        if vhypo.mood != 'inf.':
                            if vhypo.mood != 'ger.' and not partcond:
                                if vhypo.tense.startswith('p./f.'):
                                    dresult.person = vhypo.person
                                else:
                                    if vhypo.cnumber.startswith('s.') and vhypo.mood != 'imp.':
                                        dresult.gender = vhypo.gender
                                dresult.number = vhypo.cnumber[:1]
                            if vhypo.mood != "imp.":
                                dresult.tense = vhypo.tense
                            if partcond:
                                dresult.pspeech = 'part.'
                                splitcase(delim, len(delim) - 1,
                                          searchcase(self.secend, z_a_cases, z_a_str, form=form))
                            elif vhypo.mood.startswith("ger."):
                                dresult.pspeech = 'ger.'
                            else:
                                dresult.pspeech = 'v.'
                        delim.append(dresult)

                self.ret_mess = ('Word not in dictionary. '
                                 'Press F5 to view hypotheses, any other key - to return.')

                for nhypo in (h for h in self.noun_hypotheses if h.succnom):
                    if nhypo.source:

                        if not noshow:
                            self.ret_mess = 'Press any key...'
                        if not noshow:
                            qresult = dict(examp=nhypo.source,
                                           inform=nhypo.mess,
                                           n_pl_ac=nhypo.n_acc_pl,
                                           pad_str=nhypo.case_str,
                                           ok_str=nhypo.end_str,
                                           acc_nt=nhypo.accent,
                                           indecl=nhypo.res_str)
                            qresults.append(qresult)
                            dresult = dict(cargo=['noun', demacc(nhypo.source, nhypo.n_acc_pl),
                                                  len(qresults) - 1])
                            delim.append(dresult)
                        else:
                            dresult = dict(source=fform,
                                           sform=deacc(nhypo.source),
                                           info=nhypo.mess)
                            delim.append(dresult)
                            if nhypo.mess.startswith(("м ", "мо", "ж ", "жо", "с ", "со ", "п ",
                                                      "ч ", "мп", "мс", "мн.", "сравн.",
                                                      "м//", "ж//", "c//")):
                                if nhypo.mess.startswith(("мп", "мс-п")):
                                    dresult.pspeech = "a."
                                    nhypo.res_str = (", " if nhypo.mess.startswith("мп") else "") \
                                        + nhypo.res_str.lstrip()
                                    splitcase(delim, len(delim) - 1, nhypo.res_str)
                                    if fform in "это":  # Special case - treat "это" as a noun!
                                        # FIXME: and то?
                                        delim.append(dict(dresult,
                                                          pspeech="pron."))
                                        splitcase(delim, len(delim) - 1, nhypo.res_str)
                                elif nhypo.mess.startswith("мс"):
                                    dresult.pspeech = "pron."
                                    splitcase(delim, len(delim) - 1, nhypo.res_str)
                                elif nhypo.mess.startswith("мн."):
                                    dresult.pspeech = "n."
                                    dresult.gender = "mfn"["мжс".index(nhypo.mess[4])]
                                    dresult.anim = "ai"["о ".index(nhypo.mess[5])]
                                    splitcase(delim, len(delim) - 1, nhypo.res_str)
                                elif nhypo.mess.startswith("п"):
                                    dresult.pspeech = "a."
                                    splitcase(delim, len(delim) - 1, nhypo.res_str)
                                elif nhypo.mess.startswith("ч"):
                                    dresult.pspeech = "num."
                                    dresult.number = "p"
                                    splitcase(delim, len(delim) - 1, nhypo.res_str)
                                elif nhypo.mess.startswith("сравн."):
                                    dresult.pspeech = "ad."
                                else:
                                    dresult.pspeech = "n."
                                    dresult.gender = "mfn"["мжс".index(nhypo.mess[0])]
                                    dresult.anim = "ai"["о ".index(nhypo.mess[1])]
                                    splitcase(delim, len(delim) - 1, nhypo.res_str)
                            else:
                                if "предл" in nhypo.mess:
                                    dresult.pspeech = "prep."
                                elif "част" in nhypo.mess:
                                    dresult.pspeech = "prt."
                                elif "межд" in nhypo.mess:
                                    dresult.pspeech = "intr."
                                elif "союз" in nhypo.mess:
                                    dresult.pspeech = "conj."
                                elif "предик." in nhypo.mess:
                                    dresult.pspeech = "v."
                                elif nhypo.mess.startswith("н"):
                                    dresult.pspeech = "ad."
                                elif "числ.-п" in nhypo.mess:
                                    dresult.pspeech = "a."
                                    splitcase(delim, len(delim) - 1, nhypo.res_str)

                if not noshow:
                    pass

            else:
                if not self.success:
                    break

            if not self.trus:
                break
            else:
                self.noun_hypotheses = []

        if self.build_stage >= 0:
            return self.listing(fform)

        if noshow:
            return delim

        if self.contmrph:
            return self.success

        if delim is not None:
            result = ''
            if self.success:
                for hypo in chain(self.noun_hypotheses, self.verb_hypotheses):
                    if isinstance(hypo, VerbHypothesis):
                        hypo.reflexive = self.refl
                        result += hypo.to_delimited(delimiter=delim, build_par=build_par,
                                                    transinf=transinf, full=full,
                                                    form=form, secend=self.secend)
                    elif isinstance(hypo, NounHypothesis):
                        result += hypo.to_delimited(delimiter=delim, build_par=build_par,
                                                    transinf=transinf, full=full)

                    if self.virtbuff and ((isinstance(hypo, NounHypothesis) and hypo.succnom) or
                                          (isinstance(hypo, VerbHypothesis) and hypo.succverb)):
                        break

            if is_english:
                result = self.engstring + " "

            return result[:-1]

        return self.success

    def showpar(self, tempsour, tempinf, tempcas,
                tempend, accarr, akz_pl) -> list:
        """Display a nominal paradigm for a generated hypothesis."""

        if not accarr:
            return []

        locarr: list = [None] * len(accarr)
        t_accarr: list = []
        t_inf: list = []
        k: int = 0

        akz_pl = str(akz_pl)
        for i, accarr_elt in enumerate(accarr):
            if tempsour.endswith(("жо", "цо", "чо", "шо", "що")):
                if not lastacc(tempsour, int(akz_pl)):
                    if 'н ' not in tempinf:
                        continue

            elif tempsour.endswith("е"):
                if lastacc(tempsour, take_int(akz_pl)):
                    if 'н ' not in tempinf:
                        continue

            elif accarr_elt == 'а':
                if ('п ' in tempinf and int(akz_pl) == len(tempsour) - 1) or 'в' in tempinf:
                    continue
                elif voc(tempsour, len(tempsour)) and int(akz_pl) == len(tempsour):
                    continue
                elif '*' in tempinf and lastacc(tempsour, int(akz_pl)):
                    continue

            elif accarr_elt in 'се':
                if not firstacc(tempsour, int(akz_pl)):
                    continue

            elif accarr_elt in "вDD'FF''":  # FIXME what could that mean?:
                if accarr_elt in "DD'FF''" and 'мн. ' in tempinf:
                    if lastacc(tempsour, int(akz_pl)):
                        continue
                if 'п ' in tempinf:
                    if int(akz_pl) != len(tempsour) - 1 or 'а' in tempinf:
                        continue
                elif voc(tempsour, len(tempsour)) and int(akz_pl) < len(tempsour):
                    continue
                elif not lastacc(tempsour, int(akz_pl)):
                    continue

            if "x" in tempinf:
                t_inf.append(tempinf.replace("x", accarr_elt))
            else:
                t_inf.append(tempinf)
            k += 1
            t_accarr.append(accarr_elt)

            locarr[i] = demacc(tempsour, akz_pl) + " " + t_inf[k]

        if k == 0:
            return []

        locarr = locarr[:len(t_accarr)]

        if self.build_stage == 2:
            # some locarr elements turn out to be empty arrays:
            # delete them here - S.A.S
            for i in range(len(locarr) - 1, 0, -1):
                if not isinstance(locarr[i], str):
                    del locarr[i]
            return locarr

        locoption = self.build_locoption
        if not t_accarr[locoption]:
            result = tempsour + " " + akz_pl + " " + t_inf[locoption]
        else:
            # FIXME: this call originally passed all params by reference
            result = nompar(tempsour, t_inf[locoption], tempcas, tempend, t_accarr[locoption],
                            akz_pl, "", "", True)

        if self.build_stage == 3:
            return result

        dictwrite(tempsour, akz_pl + ' ' + t_inf[locoption])
        return []

    def send(self, _source: str, _ending: str, _ablaut: bool,
             _second: bool = False) -> int:
        """Fill the resulting arrays with the result of a particular nominal
        morphological analysis."""

        if not isvoc(_source):  # or FINNUM >= MAXNUM:
            return -1  # FINNUM
        if _second:
            self.secform = _source
            self.secend = _ending
        self.noun_hypotheses.append(NounHypothesis(source=_source, ending=_ending, ablaut=_ablaut))
        return len(self.noun_hypotheses)

    def showvpar(self, tempsour: str, tempinf: str, akz_pl, form) -> list:
        """Display verbal paradigm for a generated hypothesis."""

        akz_pl = str(akz_pl)

        loc_type: str = rest(tempinf)[:-1]
        start: int = 1
        limit: int = 3

        if loc_type in "2 7":
            if not lastacc(tempsour, take_int(akz_pl)):
                limit = 1
            else:
                limit = 2
        elif loc_type in "1 3**":
            if loc_type == "3":
                if lastacc(tempsour, take_int(akz_pl)):
                    start = 2
                else:
                    limit = 1
            else:
                if loc_type == "3**" and lastacc(tempsour, take_int(akz_pl)):
                    return []
                limit = 1
        elif loc_type in "6**" and tempsour.endswith("ять"):
            if lastacc(tempsour, take_int(akz_pl)):
                start = 2
                limit = 2
            else:
                limit = 1
        else:
            if not lastacc(tempsour, take_int(akz_pl)):
                limit = 1
            else:
                start = 2
                limit = 3

        locarr: List[Optional[str]] = [None] * (limit - start + 1)
        t_inf: List[Optional[str]] = [None] * (limit - start + 1)
        k = start
        for i in range(limit - start + 1):
            while True:
                _t_inf_i = tempinf.replace("x", "авс"[k - 1])
                t_inf[i] = _t_inf_i
                # Check if the form is indeed within the paradigm:
                result = verbpar(tempsour, _t_inf_i, akz_pl, True).replace("'", "")
                k += 1
                if " " + form + ";" in result or " " + form + " " in result:
                    locarr[i] = demacc(tempsour, akz_pl) + " " + _t_inf_i
                    break
                i -= 1
                limit -= 1
                t_inf = t_inf[:-1]
        if i == 0:
            return []

        if self.build_stage == 2:
            # The next cycle is just in case: maybe not needed at all - S.A.S
            for i in range(len(locarr) - 1, -1, -1):
                if not isinstance(locarr[i], str):
                    del locarr[i]
            return locarr

        locoption = self.build_locoption
        # FIXME: pass by ref
        result = verbpar(tempsour, t_inf[locoption], akz_pl, True)
        if self.build_stage == 3:
            return result

        dictwrite(tempsour, akz_pl + ' ' + t_inf[locoption])
        return []

    def user_dictionary_subset(self, form):
        t_buff = ""
        # FIXME: userdict support
        '''
        t_buff = "\n"
        for rusword, wordtype in userdict_subset(form[:3]):
            if self.contmrph and rusword == form:
                raise SuccessException
            t_buff += '%s %s\n' % (rusword, wordtype)
        '''
        return t_buff

    def verb(self,
             verb: str,
             bu_ff: Optional[str]) -> bool:
        """Verbal morphological analyzer."""

        # LOCAL lastlet, stem, end_ing, ger_test, t_verb
        # MEMVAR refl

        # These are local context should be made available to subroutines
        _endstem, _mood, _tense, _number, _gender, _person = [''] * 6
        _participle = False
        locsucc: bool = False
        trus: bool = False

        def parttest(stem: str) -> None:
            """Subroutine of VERB, generating information about participial forms."""

            nonlocal _tense, _endstem, _participle

            stemend: str = stem[-1]
            _tense = 'past'
            if voc(stemend, 1):
                vsend(stem + 'ить', '4 ')
            else:
                base: str = stem[:-1]
                _endstem = stemend

                if stemend == 'б':  # погребенный:
                    vsend(base + 'сти', '7 ')

                elif stemend == 'д':
                    if stem.endswith('вид'):      # увиденный
                        vsend(stem + 'еть', '5 ')
                    elif stem in z_eatstring + z_eatstr2 or stem.endswith('ъед'):
                        vsend(base + 'сть', '@')  # съеденный
                    elif stem.endswith('йд'):     # найденный
                        vsend(base + 'ти', '@')
                    elif stem == 'обкрад':        # обкраденный
                        vsend('обокрасть', '7 ')
                    elif stem == 'сбонд':         # сбонденный
                        vsend('сбондить', '4 ')
                    else:
                        if stem.endswith('жд'):   # награжденный
                            vsend(stem[:-2] + 'дить', '4 ')
                            vsend(stem[:-2] + 'здить', '4 ')
                            # пригвожденный
                            #v_type[2] = '4 '  # FIXME: what's this?
                        else:
                            # The following condition was incorrect
                            # if stem <> 'обокрад':
                            vsend(base + 'сть', '7 ')  # украденный
                            vsend(base + 'сти', '7 ')  # соблюденный

                elif stemend == 'ж':
                    if not (stem in 'вонж пронж обуржуаж обремиж сбонж' or stem.endswith('виж')):
                        if stem.endswith('жж'):
                            vsend(devoc(stem[:-2], 'жж') + 'жечь', '8 ', endstem='г')  # обожженный
                        else:
                            vsend(stem + 'ить', '4 ')   # обнаженный
                            vsend(base + 'дить', '4 ')  # загаженный
                            vsend(base + 'деть', '5 ')  # обиженный
                            vsend(base + 'зить', '4 ')  # зараженный
                            vsend(base + 'чь', '8 ', endstem='г')    # запряженный

                elif stemend == 'з':
                    if stem in 'вонз пронз обуржуаз обремиз слямз':
                        vsend(stem + 'ить', '4 ')
                    else:
                        vsend(stem + 'ть', '7 ')  # обгрызенный
                        vsend(stem + 'ти', '7 ')  # завезенный

                elif stemend in 'рл':
                    if not stem.endswith(('клеймл', 'мертвл')):
                        if stem == 'поколебл':
                            vsend('поколебать', '6 ')
                        else:
                            vsend(stem + 'ить', '4 ')  # сверленный
                            vsend(stem + 'еть', '5 ')  # рассмотренный
                            vsend(stem + 'еть', '1 ')  # преодоленный, запечатленный
                            if stem.endswith('мышл'):
                                vsend(base[:-1] + 'слить', '4 ')
                            elif stem.endswith(('бл', 'пл', 'вл', 'фл', 'мл')):
                                if stem == 'умерщвл':
                                    vsend('умертвить', '4 ')
                                else:  # оскорбленный
                                    if base.endswith('шиб'):
                                        vsend(base + 'ить', '@')
                                    else:
                                        vsend(base + 'ить', '4 ')
                                        vsend(base + 'еть', '5 ')

                elif stemend == 'м':
                    if stem.endswith('клейм'):
                        vsend(stem + 'ить', '4 ')

                elif stemend == 'н':
                    if stem.endswith(('равн', 'ровн')):
                        vsend(stem + 'ять', '1 ')
                    elif stem.endswith('мутн'):
                        vsend(base + 'ить', '4 ')  # замутненный
                    else:
                        vsend(stem + 'ить', '4 ')  # устраненный
                    if stem.endswith('сравн'):
                        vsend(stem + 'ить', '4 ')

                elif stemend == 'с':
                    if stem in 'обезопас облес обезлес пропылесос сляпс':
                        vsend(stem + 'ить', '4 ')
                    else:
                        vsend(stem + 'ти', '7 ')  # спасенный

                elif stemend == 'т':
                    if stem.endswith('чт'):
                        vsend(devoc(stem[:-2], 'чт') + 'честь', '7 ')  # прочтенный
                        vsend(stem + 'ить', '4 ')
                    else:
                        vsend(base + 'сти', '7 ')  # подметенный

                elif stemend == 'ч':
                    if stem.endswith('щекоч'):
                        vsend(base + 'тать', '6 ')
                    elif stem.endswith('толч'):
                        vsend(base + 'очь', '8 ', endstem='к')
                    else:
                        vsend(stem + 'ить', '4 ')
                        vsend(base + 'тить', '4 ')
                        vsend(base + 'теть', '5 ')
                        vsend(base + 'чь', '8 ', endstem='к')

                elif stemend == 'ш':
                    if stem not in 'обезопаш обезлеш облеш пропылесош сляпш':
                        vsend(stem + 'ить', '4 ')
                        vsend(base + 'сить', '4 ')

                elif stemend == 'щ':
                    vsend(stem + 'ить', '4 ')
                    vsend(base + 'стить', '4 ')
                    vsend(base + 'тить', '4 ')
            return None

        def pass_test(hypo: VerbHypothesis, p_stem: str) -> None:
            """Check if a "reflexive" wordform may be passive or not."""

            nonlocal locsucc

            hypo.succverb = False
            if 'нсв' in hypo.info and (hypo.mood != 'ger.' or hypo.is_participle):
                bu_ff = dict_get_entry(False, derform(p_stem, self.refl), 1)
                if bu_ff:
                    p_info = bu_ff[len(derform(p_stem, self.refl)) + 3:]
                    phypo = VerbHypothesis(v_acc_pl=first(p_info))
                    self.verb_hypotheses.append(phypo)
                    p_info = rest(p_info)
                    if '_имеется страд.' in p_info or \
                            ('нсв' in p_info and
                             not (' нп ' in p_info or 'безл.' in p_info)):
                        phypo.succverb = True
                        locsucc = True  # FIXME this is parent locsucc
                        self.contret()
                        phypo.inf = derform(p_stem, self.refl)
                        phypo.voice = 'pass.'
                        phypo.mood = hypo.mood
                        phypo.tense = hypo.tense
                        phypo.person = hypo.person
                        phypo.gender = hypo.gender
                        phypo.cnumber = hypo.cnumber
                        phypo.v_type = hypo.v_type
                        phypo.endstem = hypo.endstem
                        phypo.info = p_info

        def pasttest(stem: str, end_ing: str) -> None:
            """A subroutine of VERB, analyzing past forms."""

            nonlocal _endstem, _gender, _mood, _number, _tense

            _tense = 'past'
            if end_ing.startswith('в'):
                _mood = 'ger.'
            else:
                _mood = 'ind.'
            if end_ing == 'ли':
                _number = 'pl.'
            else:
                _number = 's.'
            if end_ing == 'ла':
                _gender = 'f.'
            elif end_ing == 'ло':
                _gender = 'n.'
            elif end_ing == 'ли':
                _gender = ''
            elif end_ing == 'л':
                _gender = 'm.'
            stemend: str = stem[-1]
            if not voc(stemend, 1):
                _endstem = stemend
                base = stem[:-1]

                if stemend == 'б':
                    vsend(base + 'сти', '7 ')
                    vsend(stem + 'нуть', '3** ')
                    if _mood.startswith('ind.') and stem.endswith('шиб'):
                        vsend(stem + 'ить', '@')

                elif stemend in 'гк':
                    if stem.endswith('жг'):
                        vsend(devoc(stem[:-2], 'жг') + 'жечь', '8 ')
                    elif stem.endswith('толк') and len(end_ing) > 1:
                        vsend(stem[:-1] + 'очь', '8 ')
                    else:
                        vsend(base + 'чь', '8 ')
                        vsend(stem + 'нуть', '3** ')

                elif stemend in 'дт':
                    if _mood.startswith('ger.') and stem[-2:] != 'а':
                        if stem.endswith('шед'):
                            if stem == 'шед':
                                vsend('идти', '@')
                            elif _participle:
                                vsend(stem[:-3] + 'йти', '@')
                        elif stem.endswith('чет'):
                            vsend(base + 'сть', '7 ')
                        else:
                            vsend(base + 'сти', '7 ')

                elif stemend == 'з':
                    vsend(stem + 'ть', '7 ')
                    vsend(stem + 'ти', '7 ')
                    vsend(stem + 'нуть', '3** ')

                elif stemend == 'р':
                    vsend(stem + 'еть', '9 ')

                elif stemend == 'с':
                    if stem.endswith('рос'):
                        vsend(stem[:-3] + 'расти', '7 ', endstem='ст')
                    else:
                        vsend(stem + 'ти', '7 ')
                        vsend(stem + 'нуть', '3** ')

                elif stemend in 'пх':
                    vsend(stem + 'нуть', '3** ')

                elif stemend == 'ш':
                    if stem == 'ш':
                        vsend('идти', '@')
                    else:
                        vsend(stem[:-1] + 'йти', '@')

                elif stemend == 'ч':
                    if _gender != 'm.':
                        base = stem[:-1]
                        vsend(devoc(base, 'ч') + 'честь', '7 ', endstem='т')
            else:

                if stemend in 'еая':
                    if stem + 'д' in z_eatstring + z_eatstr2 or stem.endswith("ъе"):
                        vsend(stem + 'сть', '@')
                    elif stem + 'д' in z_givestring:
                        vsend(stem + 'ть', '@')
                    elif stem.endswith('ше') and not stem.endswith(('кише', 'мше', 'хороше')):
                        if _mood != 'ger.' and end_ing + ' ' not in 'ла ло ли ':
                            vsend('идти' if stem == 'ше' else (stem[:-2] + 'йти'), '@')
                    elif stem.endswith('кля'):
                        vsend(stem + 'сть', '@')
                    elif stem.endswith('еха'):
                        vsend(stem + 'ть', '@')
                    else:
                        if stem.endswith(('па', 'се', 'че', 'кра', 'пря', 'кла')):
                            if not (stem.endswith('че') and end_ing + ' ' in 'в ла ло ли '):
                                if not form.endswith(stem + "нный"):
                                    # to avoid *кранный etc.
                                    vsend(stem + 'сть', '7 ', endstem='тд')
                        elif stem.endswith(('ме', 'ве', 'цве', 'пле', 'бре',
                                            'гря')):
                            if _mood != 'ger.':
                                vsend(stem + 'сти', '7 ', endstem='тд')
                            if stem.endswith('реве'):
                                if not stem.endswith('ереве'):
                                    vsend(stem + 'ть', '@')

                        if not stem.endswith('ье'):
                            vsend(stem + 'ть', '1 2 5 6 12 13 14 15 ',
                                  endstem='имнщ')

                        if _mood.startswith('ger.') and not _participle:
                            if stem.endswith('ре'):
                                vsend(stem + 'ть', '1 5 9 ')
                        if stem.endswith('вя'):
                            vsend(stem + 'нуть', '3** ')

                else:
                    if stem.endswith(("бы", "зыби", "ижди")):
                        vsend(stem + 'ть', '@')
                    elif stem.endswith(('ро', 'ло')):
                        vsend(stem + 'ть', '10 ')
                    else:
                        if len(stem) > 1 and stem[-2] == 'т':
                            vsend(stem + 'ть', '3 3** 4 11 12 16 ',
                                  endstem='тщ ')
                        else:
                            vsend(stem + 'ть', '3 3** 4 11 12 16 ')
                        if stem.endswith('сты'):
                            vsend(stem + 'нуть', '3** ')
                        elif stem.endswith('блю'):
                            if _mood != 'ger.':
                                _endstem = 'тд'
                                vsend(stem + 'сти', '7 ')

        def prestest(stem: str, end_ing: str, form: str) -> None:
            """Subroutine of VERB for testing present forms."""

            nonlocal _endstem, _mood, _number, _person, _tense

            # local itest, root, stemend, base, t_type

            if 'у' in end_ing and (voc(stem, -1) or stem.endswith(('п', 'л', 'ь'))):
                return None
            if 'а' in end_ing and not stem.endswith(("ч", "ж", "ш", "щ")):
                return None
            if not voc(stem, -1):
                if ('ю' in end_ing or end_ing == 'ят') and \
                        stem.endswith(("ч", "ж", "ш", "щ", "ц")):
                    return None
                if 'ю' in end_ing and not stem.endswith(("л", "р", "н", "ь")):
                    return None

            if end_ing in 'ийь':
                _mood = 'imp.'
                _number = 's.'
                _person = '2'

            elif end_ing in 'ую':
                _number = 's.'
                _person = '1'

            elif end_ing in 'ешь ишь':
                _number = 's.'
                _person = '2'

            elif end_ing in 'ет ит':
                _number = 's.'
                _person = '3'

            elif end_ing in 'ем им':
                _number = 'pl.'
                _person = '1'

            elif end_ing in 'ете ите йте ьте':
                _number = 'pl.'
                _person = '2'

            elif end_ing in 'я':
                _mood = 'ger.'

            elif end_ing in 'ут ют ат ят':
                _number = 'pl.'
                _person = '3'

            else:
                return None

            stemend = stem[-1]
            base = stem[:-1]

            if voc(stemend, 1):
                itest = (end_ing.startswith('и') or end_ing == 'ят')

                if stemend == 'а':
                    if stem in z_givestring:
                        if not itest and _mood == 'imp.':
                            vsend(stem + 'ть', '@')
                        elif not itest and _mood != 'ger.' and not self.pasprt:
                            vsend(stem + 'вать', '13 ')
                    else:
                        if not itest:
                            if stem == 'поезжа':
                                vsend('поехать', '@')
                            else:
                                if _mood == 'ger.' and stem.endswith(('жажда', 'стона')):
                                    t_type = '6 '
                                else:
                                    t_type = '1 '
                                vsend(stem + 'ть', t_type)
                                #*vsend(stem + 'ять', '1 ')
                                if _mood == 'ind.' and stem.endswith(('да', 'зна', 'ста')):
                                    if not self.pasprt:
                                        vsend(stem + 'вать', '13 ')
                                vsend(stem + 'ять', '6 ')
                        if (itest or end_ing in 'юяйте') and stem.endswith(('дра', 'та')):
                            vsend(stem + 'ить', '4 ')
                        if end_ing in 'юяйте' and stem.endswith('муча'):  # Igor's addition for #16
                            vsend(stem[:-1] + 'ить', '4 ')
                        if (self.pasprt or _mood != 'ind.') and stem.endswith('ава'):
                            vsend(stem + 'ть', '13 ')

                elif stemend == 'е':
                    if not itest:
                        if ' ' + stem + ' ' in z_shavestring:
                            vsend(base + 'ить', '12 ')
                            if stem == 'добре':
                                vsend(stem + 'ть', '1 ')
                        else:
                            vsend(stem + 'ть', '1 ')
                            if stem.endswith('гре'):
                                vsend(stem + 'ть', '12 ')
                            else:
                                vsend(stem + 'ять', '6 ')
                            if end_ing.startswith('й'):
                                vsend(base + 'ить', '11 ')
                            if end_ing in 'юяйте':
                                vsend(stem + 'ить', '4 ')
                    else:
                        vsend(stem + 'ить', '4 ')

                elif stemend in 'ия':
                    if not itest:
                        if stem == 'вопи':
                            vsend(stem + 'ять', '6 ')
                        else:
                            if stemend == 'и':
                                t_type = '12 '
                            else:
                                t_type = '1 '
                            vsend(stem + 'ть', t_type)
                            if stem.endswith('меря'):  # Igor's addition for #16
                                vsend(stem[:-1] + 'ить', '4 ')

                elif stemend == 'о':
                    if end_ing in "ите":
                        vsend(stem + 'ить', '4 ')
                        vsend(stem + 'ять', '5 ')
                    else:
                        if stem + ' ' in z_singstring:
                            if itest:
                                vsend(stem + 'ить', '4 ')
                            else:
                                vsend(base + 'еть', '12 ')
                            if end_ing in 'юя':
                                vsend(stem + 'ить', '4 ')
                        elif stem.endswith(('сто', 'бо')):
                            if end_ing + ' ' not in 'ешь ет ем ете ют ':
                                vsend(stem + 'ять', '5 ')
                            if itest or end_ing in 'юяйте':
                                vsend(stem + 'ить', '4 ')
                        else:
                            if not itest and base.endswith(('в', 'м', 'н', 'р')):
                                vsend(base + 'ыть', '12 ')
                            if itest or end_ing in 'юяйте':
                                vsend(stem + 'ить', '4 ')

                elif stemend == 'у':
                    if not itest:
                        if ' ' + stem + ' ' in " живопису хиротонису ":
                            vsend(stem[:-1] + 'ать', '1 ')
                        elif stem == "обязу":
                            vsend("обязывать", '1 ')
                        elif stem in z_blowstring:
                            vsend(stem + 'ть', '12 ')
                        elif stem in z_smellstring:
                            vsend(stem + 'ять', '6 ')
                        else:
                            if stem[-2] not in "чжшц":
                                vsend(base + 'овать', '2 ')
                            else:
                                vsend(base + 'евать', '2 ')
                        if end_ing in 'юяйте':
                            vsend(stem + 'ить', '4 ')
                    else:
                        vsend(stem + 'ить', '4 ')

                elif stemend == 'ю':
                    if not itest:
                        vsend(base + 'евать', '2 ')
            else:
                _endstem = stemend
                if _mood != 'imp.' and end_ing + ' ' in 'ит ишь им ите ат ят ':
                    if stem in z_eatstring + z_eatstr2 or stem.endswith("ъед"):
                        if end_ing not in 'ит ишь ат':
                            vsend(base + 'сть', '@')
                    elif stem.endswith('дад') and stem in z_givestring:
                        if end_ing not in 'ит ишь ат ят':
                            vsend(base + 'ть', '@')
                    elif stem.endswith('киш'):
                        vsend(stem + 'еть', '5 ')
                    elif stem.endswith('сп'):
                        vsend(stem + 'ать', '5 ')
                    elif stem.endswith('гон'):
                        if stem.endswith('изгон'):
                            vsend(stem[:-3] + 'гнать', '5 ')
                        else:
                            vsend(vocal(stem[:-3]) + 'гнать', '5 ')
                    else:
                        if stemend == 'т':
                            vsend(stem + 'ить', '4 ', endstem='тщ ')
                        else:
                            vsend(stem + 'ить', '4 ')
                        if not form.endswith(('хотит', 'хотишь', 'бежат', 'бежащий')):
                            vsend(stem + ('ать' if stem[-1] in 'чшщжц' else 'еть'), '5 ')
                else:

                    if stemend == 'б':
                        if _mood == 'ind.':
                            vsend(base + 'сти', '7 ')
                            if stem.endswith('шиб'):
                                vsend(stem + 'ить', '@')
                        else:
                            vsend(base + 'сти', '7 ')
                            if not stem.endswith('шиб'):
                                t_type = '4 '
                            else:
                                t_type = '@'
                            vsend(stem + 'ить', t_type)
                            vsend(stem + 'еть', '5 ')

                    elif stemend == 'в':
                        if stem.endswith('рв'):
                            vsend(stem + 'ать', '6 ')
                        elif stem.endswith('зов'):
                            vsend(vocal(stem[:-3]) + 'звать', '6 ')
                        elif stem.endswith('рев'):
                            vsend(stem + 'еть', '@')
                        elif stem.endswith(('ив', 'ыв')):
                            vsend(base + 'ть', '16 ')
                        if _mood != 'ind.':
                            vsend(stem + 'ить', '4 ')

                    elif stemend == 'г':
                        if end_ing[0] not in 'ея':
                            if stem.endswith('бег'):
                                vsend(base + 'жать', '5 ')
                            else:
                                if stem.endswith('жг'):
                                    vsend(devoc(stem[:-2], 'жг') + 'жечь', '8 ')
                                elif stem.endswith('ляг'):
                                    vsend(base[:-1] + 'ечь', '8 ')
                                else:
                                    vsend(base + 'чь', '8 ')
                                    if self.dialect == 'aos':
                                        vsend(base + 'кчи', '8 ')
                                        vsend(base + 'гчи', '8 ')
                                    if stem.endswith('лг'):
                                        vsend(base + 'гать', '6 ')

                    elif stemend == 'д':
                        if not stem.endswith('сед'):
                            if stem.endswith('зижд'):
                                vsend(stem + 'ить', '@')
                            elif stem.endswith('жд'):
                                if _mood != 'ger.' or stem == 'жд':  # ед. (затр.) форма - ждя
                                    vsend(stem + 'ать', '6 ')
                            elif stem.endswith('сяд'):
                                vsend(base[:-1] + 'есть', '7 ')
                            elif stem.endswith('буд'):
                                if end_ing not in 'яите':
                                    vsend(stem[:-3] + 'быть', '@')
                                elif _mood != 'ind.':
                                    vsend(stem + 'ить', '4 ')
                            elif stem in z_eatstring + z_eatstr2 or stem.endswith("ъед"):
                                if stem.endswith("ед"):
                                    if _mood != 'imp.':
                                        vsend(base + 'хать', '@')
                            elif stem == 'ид':
                                vsend('идти', '@')
                            elif stem == 'прид':
                                if _mood != 'imp.':  # S.A.Krylov report dated 20061001
                                    _tense = 'past'  # Igor's correction
                                vsend('прийти', '@')
                            elif stem.endswith('йд'):
                                if _mood != 'imp.':  # S.A.Krylov report dated 20061001
                                    _tense = 'past'  # Igor's correction
                                vsend(base + 'ти', '@')
                            else:
                                if stem == 'обкрад':
                                    vsend('обокрасть', '7 ')
                                elif stem + ' ' in z_givestring:
                                    if _mood == 'ind.':
                                        vsend(base + 'ть', '@')
                                else:
                                    vsend(base + 'сти', '7 ')
                                    vsend(base + 'сть', '7 ')
                                    if _mood != 'ind.':
                                        vsend(stem + 'ить', '4 ')
                                        vsend(stem + 'еть', '5 ')

                    elif stemend == 'ж':
                        if end_ing == 'ут':
                            vsend(base + 'зать', '6 ')
                            vsend(base + 'гать', '6 ')
                            vsend(base + 'дать', '6 ')
                            vsend(base + 'жать', '6 ')
                        elif end_ing.startswith('е'):
                            if not stem.endswith('леж'):
                                if stem.endswith('жж') and _mood == 'ind.':
                                    vsend(devoc(stem[:-2], 'жж') + 'жечь', '8 ', endstem='г')
                                elif stem.endswith('ляж') and _mood == 'ind.':
                                    vsend(base[:-1] + 'ечь', '8 ', endstem='г')
                                else:
                                    if _mood == 'ind.':
                                        vsend(base + 'чь', '8 ', endstem='г')
                                    vsend(base + 'гать', '6 ')
                                    vsend(base + 'зать', '6 ')
                                    vsend(base + 'жать', '6 ')
                                    vsend(base + 'дать', '6 ')
                        else:
                            if not form.endswith(('бежу', 'бежи')):
                                if not (form.endswith('бежите') and _mood == 'imp.'):
                                    vsend(stem + 'ать', '5 ')
                            vsend(stem + 'ить', '4 ')
                            vsend(base + 'зать', '6 ')
                            if not base.endswith(('ж', 'л')):
                                vsend(base + 'дать', '6 ')
                                vsend(base + 'гать', '6 ')
                            if _mood == 'ind.':
                                vsend(base + 'зить', '4 ')
                                vsend(base + 'дить', '4 ')
                                vsend(base + 'деть', '5 ')
                            elif _mood == 'imp.':
                                vsend(stem + 'ать', '6 ')

                    elif stemend == 'з':
                        vsend(stem + 'ти', '7 ')
                        vsend(stem + 'ть', '7 ')
                        if _mood != 'ind.':
                            vsend(stem + 'ить', '4 ')

                    elif stemend == 'к':
                        if _mood != 'ger.':
                            if stem.endswith('тк'):
                                vsend(stem + 'ать', '6 ')
                            elif not end_ing.startswith('е'):
                                if stem.endswith('толк'):
                                    vsend(base + 'очь', '8 ')
                                else:
                                    vsend(base + 'чь', '8 ')

                    elif stemend == 'м':
                        if _mood != 'ger.':
                            t_type = '14 '
                            if stem.endswith('жм'):
                                vsend(devoc(stem[:-2], 'жм') + 'жать', t_type)
                            elif stem.endswith(('йм', 'дым')):
                                vsend(base[:-1] + 'нять', t_type, endstem='им')
                            elif stem == "изым":
                                vsend('изъять', t_type)
                            elif stem == 'возьм':
                                vsend('взять', t_type)
                            elif stem.endswith('ним'):
                                vsend(stem[:-2] + 'ять', t_type, endstem='им')
                            elif stem.endswith('прим'):
                                vsend(base + 'нять', t_type)
                        if _mood != 'ind.':
                            vsend(stem + 'ить', '4 ')
                            vsend(stem + 'еть', '5 ')

                    elif stemend == 'н':
                        if form == 'гоня':
                            vsend('гнать', '5 ')
                        elif stem.endswith('гон') and end_ing in 'ю ите':
                            if stem.endswith('изгон'):
                                vsend(stem[:-3] + 'гнать', '5 ')
                            else:
                                vsend(vocal(stem[:-3]) + 'гнать', '5 ')
                        elif end_ing in 'ю':
                            vsend(stem + 'ить', '4 ')
                            vsend(stem + 'еть', '5 ')

                        elif 'ю' not in end_ing:

                            if _mood != 'ind.':
                                vsend(stem + 'ить', '4 ')
                                vsend(stem + 'еть', '5 ')

                            if stem.endswith('стон'):
                                if _mood != 'ger.':
                                    vsend(stem + 'ать', '6 ')
                            elif stem.endswith('клян') and end_ing not in 'ьте':
                                vsend(base + 'сть', '@ ')

                            elif stem.endswith(('ачн', 'почн', 'жн')) and _mood != 'ger.':
                                if stem.endswith('жн'):
                                    vsend(devoc(stem[:-2], 'жн') + 'жать', '14 ')
                                else:
                                    vsend(base + 'ать', '14 ')
                            elif ' ' + stem in ' пропн распн' and _mood != 'ger.':
                                vsend(base + 'ять', '14 ')
                            elif stem.endswith('мн') and _mood != 'ger.':
                                if stem == 'вздремн':
                                    vsend(stem + 'уть', '3 ')
                                else:
                                    vsend(devoc(stem[:-2], 'мн') + 'мять', '14 ')
                            elif stem.endswith('тян'):
                                vsend(stem + 'уть', '3 ')
                            else:
                                if _mood != 'ger.':
                                    vsend(stem + 'уть', '3 3** ')
                                    vsend(base + 'ть', '15 ')

                    elif stemend == 'п':
                        if stem.endswith("сып"):
                            vsend(stem + 'ать', '6 ')
                        if _mood != 'ind.':
                            vsend(stem + 'ить', '4 ')
                            vsend(stem + 'еть', '5 ')
                            if stem.endswith('сп'):
                                vsend(stem + 'ать', '5 ')

                    elif stemend == 'ф':
                        if _mood != 'ind.':
                            vsend(stem + 'ить', '4 ')

                    elif stemend == 'р':
                        if end_ing.startswith('у'):

                            if stem.endswith(('бер', 'дер')):
                                root = stem[-3:]
                                if stem.endswith('избер'):
                                    vsend(stem[:-3] + 'брать', '6 ')
                                else:
                                    vsend(vocal(stem[:-3]) + root[0] + 'рать', '6 ')
                            elif stem.endswith(('мр', 'тр', 'пр')):
                                root = stem[-2:]
                                if stem == 'обопр':
                                    vsend('опереть', '9 ')
                                elif stem == 'припр':
                                    vsend('припереть', '9 ')
                                else:
                                    vsend(devoc(stem[:-2], root) + root[0] + 'ереть', '9 ')
                                    # This works for everything but "припр" - so a special fork
                                    # has to be added above!
                            else:
                                if not stem.endswith(('бр', 'др')):
                                    vsend(stem + 'ать', '6 ')

                        elif end_ing.startswith('ю'):
                            vsend(stem + 'оть', '10 ')
                            if end_ing != 'ют':
                                vsend(stem + 'ить', '4 ')
                                vsend(stem + 'еть', '5 ')
                        elif end_ing.startswith(('е', 'и', 'ь', 'я')):
                            if stem.endswith(('бер', 'дер')):
                                root = stem[-3:]
                                if stem.endswith('избер'):
                                    vsend(stem[:-3] + 'брать', '6 ')
                                else:
                                    vsend(vocal(stem[:-3]) + root[0] + 'рать', '6 ')
                            elif stem.endswith(('мр', 'тр', 'пр')) and _mood != 'ger.':
                                root = stem[-2:]
                                if stem == 'обопр':
                                    vsend('опереть', '9 ')
                                else:
                                    vsend(devoc(stem[:-2], root) + root[0] + 'ереть', '9 ')
                            elif not stem.endswith(('бр', 'др')):
                                if stem.endswith(('пор', 'бор')):
                                    vsend(stem + 'оть', '10 ')
                                vsend(stem + 'ать', '6 ')
                            if _mood != 'ind.':
                                vsend(stem + 'ить', '4 ')
                                vsend(stem + 'еть', '5 ')

                    elif stemend == 'с':
                        if stem.endswith('сос'):
                            vsend(stem + 'ать', '6 ')
                        else:
                            vsend(stem + 'ти', '7 ')
                        if _mood != 'ind.':
                            vsend(stem + 'ить', '4 ')
                            vsend(stem + 'еть', '5 ')

                    elif stemend == 'т':
                        if stem.endswith('чт'):
                            if form in 'почтут почти чтя':
                                vsend(stem + 'ить', '4 ', endstem='тщ ')
                                vsend(stem[:-2] + 'честь', '7 ')
                            else:
                                vsend(devoc(stem[:-2], 'чт') + 'честь', '7 ')
                        elif stem.endswith('раст'):
                            vsend(base + 'ти', '7 ', endstem='ст')
                            if _mood != 'ind.':
                                vsend(stem + 'ить', '4 ')
                        else:
                            if base and voc(base, -1):
                                vsend(base + 'сти', '7 ')
                            if _mood != 'ind.':
                                if stemend == 'т':  # FIXME: extraneous check
                                    _endstem = 'тщ '
                                vsend(stem + 'ить', '4 ')
                                vsend(stem + 'еть', '5 ')

                    elif stemend == 'ч':
                        if not stem.endswith(('тч', 'чч')):
                            if end_ing == 'ут':
                                vsend(base + 'кать', '6 ')
                                vsend(base + 'тать', '6 ')
                            elif end_ing.startswith('е'):
                                if stem.endswith('хоч'):
                                    vsend(base + 'теть', '5 ')
                                if stem.endswith('толч'):
                                    vsend(base + 'очь',
                                          '8 ', _mood, _tense, _person, _number, 'к', _gender, '')
                                else:
                                    vsend(base + 'кать', '6 ')
                                    vsend(base + 'тать', '6 ')
                                    vsend(stem + 'ь',
                                          '8 ', _mood, _tense, _person, _number, 'к', _gender, '')
                            else:
                                vsend(base + 'кать', '6 ')
                                vsend(base + 'тать', '6 ')
                                vsend(stem + 'ать', '5 ')
                                vsend(stem + 'ить', '4 ')
                                if _mood == 'ind.':
                                    vsend(base + 'тить', '4 ')
                                    vsend(base + 'теть', '5 ')

                    elif stemend == 'ш':
                        if end_ing == 'ут' or end_ing.startswith('е'):
                            vsend(base + 'сать', '6 ')
                            vsend(base + 'хать', '6 ')
                        elif stem.endswith('киш'):
                            vsend(stem + 'еть', '5 ')
                        else:
                            vsend(base + 'сать', '6 ')
                            vsend(base + 'хать', '6 ')
                            vsend(stem + 'ать', '5 ')
                            vsend(stem + 'ить', '4 ')
                            if _mood == 'ind.':
                                vsend(base + 'сить', '4 ')
                                vsend(base + 'сеть', '5 ')

                    elif stemend == 'щ':
                        if not stem.endswith('блищ'):
                            vsend(base + 'тать', '6 ')
                            if stem.endswith('блещ'):
                                vsend(stem[:-2] + 'истать', '6 ')
                            else:
                                vsend(base + 'стать', '6 ')
                            vsend(base + 'скать', '6 ')
                            if not (end_ing.startswith('е') or end_ing == 'ут'):
                                vsend(stem + 'ать', '5 ')
                                vsend(stem + 'ить', '4 ')
                                if _mood == 'ind.':
                                    vsend(base + 'стить', '4 ')
                                    vsend(base + 'тить', '4 ')
                                    vsend(base + 'стеть', '5 ')

                    elif stemend == 'ь':
                        if _mood != 'ger.':
                            root = stem[-2:]
                            vsend(devoc(stem[:-2], root) + root[0] + 'ить', '11 ')

                    elif stemend == 'л':
                        if stem == 'умерщвл' and end_ing == 'ю':
                            vsend('умертвить', '@')
                        elif stem.endswith('зыбл'):
                            vsend(base + 'ить', '@')
                        elif stem.endswith('стел'):
                            vsend(vocal(stem[:-4]) + 'стлать', '6 ')
                            if end_ing.startswith('и'):
                                vsend(stem + 'ить', '4 ')
                        elif stem.endswith('глагол'):
                            vsend(stem + 'ать', '6 ')
                        elif stem.endswith('мел'):
                            vsend(stem[:-3] + 'молоть', '10 ')
                            if end_ing.startswith(('и', 'ю')):
                                vsend(stem + 'ить', '4 ')
                        elif len(stem) >= 2 and voc(stem, -2):
                            if stem.endswith(('кол', 'пол')):
                                vsend(stem + 'оть', '10 ')
                            if end_ing in "яюитеьте":
                                vsend(stem + 'ить', '4 ')
                                vsend(stem + 'еть', '5 ')
                        elif stem.endswith('шл'):
                            vsend(stem[:-2] + 'слать', '6 ')
                            if end_ing in 'юите':
                                vsend(stem + 'ить', '4 ')
                        elif len(stem) >= 2 and stem[-2] in "бпвфм":
                            if end_ing.startswith(('е', 'и', 'я')) or end_ing == 'ют':
                                if end_ing not in ('ит', 'ят'):
                                    vsend(base + 'ать', '6 ')
                            else:
                                if stem.endswith('спл') and end_ing == 'ю':
                                    vsend(base + 'ать', '5 ')
                                else:
                                    vsend(stem + 'ить', '4 ')
                                    if _mood == 'ind.':
                                        vsend(base + 'ить', '4 ')
                                        vsend(base + 'еть', '5 ')
                                        vsend(base + 'ать', '6 ')
                        else:
                            if end_ing.startswith(('и', 'ю', 'я')) and end_ing != 'ют':
                                vsend(stem + 'ить', '4 ')

        def vsend(vform: str, vtype: str,
                  mood: str = None,
                  tense: str = None,
                  person: str = None,
                  cnumber: str = None,
                  endstem: str = None,
                  gender: str = None,
                  voice: str = None,
                  succverb: bool = False):
            """Analog of SEND for the verb."""

            h = VerbHypothesis(inf=vform, v_type=vtype,
                               mood=mood or _mood,
                               tense=tense or _tense,
                               person=person or _person,
                               cnumber=cnumber or _number,
                               endstem=endstem or _endstem,
                               gender=gender or _gender,
                               voice="pass." if self.paspst else (voice or "act."),
                               succverb=succverb,
                               is_participle=_participle)
            self.verb_hypotheses.append(h)

        start: int = len(self.verb_hypotheses)

        self.pasprt = False
        self.paspst = ''

        form = verb
        self.refl = False
        if len(verb) <= 1:
            return False
        if not trus:
            if verb.endswith('сь') and not verb.endswith(('крась', 'квась', 'удесь', 'весь', 'сось',
                                                          'рось', 'бась', 'пась', 'лесь', 'дось',
                                                          'высь')):
                if len(verb) < 4 or not voc(verb, -3):
                    return False
                if verb.endswith('трусь'):
                    trus = True
                verb = verb[:-2]
                self.refl = True
        else:
            trus = False

        if verb.endswith('ся'):
            if len(verb) > 3 and not (voc(verb, -3) or verb.endswith(('акся', 'орся', 'япся'))):
                self.refl = True
                verb = verb[:-2]

        if verb.endswith('щий'):
            verb = verb[:-3] + 'т'
            _participle = True
        if verb.endswith('ший'):
            verb = verb[:-1]
            _participle = True
        if not self.refl:
            if verb.endswith('мый') and verb != "емый":
                if verb.endswith('омый'):
                    t_verb = verb[:-4]
                    if t_verb == "влек":
                        verb = verb[:-5] + "чем"  # влекомый
                    elif t_verb.endswith(("д", "с", "з")):
                        verb = verb[:-4] + 'ем'  # несомый, ведомый, везомый
                else:
                    verb = verb[:-2]
                self.pasprt = True
                self.paspst = 'мый'
                _participle = True
            if len(verb) > 3 and verb.endswith('тый'):
                stem = verb[:-3]
                if (voc(stem, -1) and stem + "д" not in z_givestring) or stem.endswith("р"):
                    verb = (stem + 'л') if voc(stem, -1) else stem
                    _participle = True
                    self.paspst = 'тый'
            elif verb.endswith(('анный', 'янный')):
                if verb.endswith('цованный'):
                    verb = verb[:-8] + 'цевал'
                else:
                    verb = verb[:-4] + 'л'
                _participle = True
                self.paspst = 'анный'

        lastlet = verb[-1]

        if lastlet == 'а':
            if len(verb) > 2 and verb.endswith('ла'):
                _mood = 'ind.'
                stem = verb[:-2]
                end_ing = 'ла'
                pasttest(stem, end_ing)
            elif verb[-2:-1] in ('ч', 'ж', 'ш', 'щ'):
                _tense = 'p./f.'
                stem = verb[:-1]
                end_ing = 'я'
                prestest(stem, end_ing, form)

        elif lastlet == 'в':
            if not self.refl:
                if voc(verb, -2):
                    stem = verb[:-1]
                    pasttest(stem, 'в')

        elif lastlet in 'бгзклпрсх':
            if verb.endswith('ляг'):
                vsend(first(verb, 'ляг') + 'лечь',
                      '8 ', 'imp.', 'p./f.', '2', 's.', 'г', '', '')
            else:
                _mood = 'ind.'
                if lastlet == 'л':
                    if voc(verb, -2):
                        pasttest(verb[:-1], lastlet)
                else:
                    pasttest(verb, 'л')

        elif lastlet == 'е':
            _tense = 'p./f.'
            if len(verb) > 3 and verb.endswith('те'):
                if verb.endswith('ите'):
                    stem = verb[:-3]
                    end_ing = 'ите'
                    _mood = 'ind.'
                    prestest(stem, end_ing, form)
                    _mood = 'imp.'
                    prestest(stem, end_ing, form)
                elif verb.endswith('ете'):
                    _tense = 'p./f.'
                    _mood = 'ind.'
                    stem = verb[:-3]
                    end_ing = 'ете'
                    prestest(stem, end_ing, form)
                else:
                    stem = verb[:-3]
                    if verb.endswith('лягте'):
                        vsend(verb[:-5] + 'лечь', '8 ', 'imp.', 'p./f.', '2', 'pl.', 'г', '', '')
                    elif verb.endswith('ешьте') and verb[:-4] + "д" in z_eatstring + z_eatstr2:
                        vsend(verb[:-5] + 'есть',
                              '@', 'imp.', 'p./f.', '2', 'pl.', _endstem, '', '')
                    elif len(verb) > 4:
                        end_ing = verb[-3:]
                        _mood = 'imp.'
                        prestest(stem, end_ing, form)

        elif lastlet == 'и':
            ger_test = False

            if self.dialect == 'aos' and verb.endswith(('гчи', 'кчи')):
                vsend(verb, '', 'inf.', '', '', '', _endstem, '', '')
            if verb.endswith(('зти', 'йти', 'сти', 'дти')):
                vsend(verb, '', 'inf.', '', '', '', '', '', '')
                if not verb.endswith('сти'):
                    ger_test = True
            elif len(verb) > 2 and verb.endswith('ли'):
                stem = verb[:-2]
                pasttest(stem, 'ли')
            elif len(verb) > 3 and verb.endswith('ши'):
                if verb.endswith('вши'):
                    if voc(verb, -4):
                        stem = verb[:-3]
                        end_ing = 'в'
                        pasttest(stem, end_ing)
                        ger_test = True
                elif not voc(verb, -3):
                    if not verb.endswith(('рши', 'мши')):
                        ger_test = True
                    stem = verb[:-2]
                    end_ing = 'в'
                    pasttest(stem, end_ing)

            if not ger_test:
                stem = verb[:-1]
                end_ing = 'и'
                _mood = 'imp.'
                prestest(stem, end_ing, form)

        elif lastlet == 'й':
            if len(verb) > 5 and verb.endswith('енный') and not self.refl:
                stem = verb[:-5]
                self.paspst = 'нный'
                _participle = True
                parttest(stem)
            else:
                stem = verb[:-1]
                if voc(stem, -1):
                    end_ing = 'й'
                    _mood = 'imp.'
                    _number = 's.'
                    prestest(stem, end_ing, form)

        elif lastlet == 'м':
            stem = verb[:-1]
            if stem.endswith(('а', 'е', 'и')):
                _mood = 'ind.'
                _tense = 'p./f.'
                if stem == 'пое' and not _participle:
                    self.contret()
                    locsucc = True
                    vsend('поесть',
                          '@', 'ind.', 'p./f.', '1', 's.', _endstem, '', '', succverb=True)
                    vsend('петь',
                          '12 ', 'ind.', 'p./f.', '1', 'pl.', _endstem, '', '', succverb=True)
                elif (stem + "д" in z_eatstring or " " + stem + "д" in z_eatstr2 or
                      stem + "д" in z_givestring):
                    # * == 'перее' or stem == 'прое' or stem.endswith(("ъе", "да")))
                    vsend(stem + ('ть' if stem.endswith("да") else 'сть'),
                          '@', 'ind.', 'p./f.', '1', 's.', _endstem, '', '')
                elif verb.endswith(('ем', 'им')):
                    stem = verb[:-2]
                    end_ing = verb[-2:]
                    prestest(stem, end_ing, form)

        elif lastlet == 'о':
            if len(verb) > 2 and verb.endswith('ло'):
                stem = verb[:-2]
                pasttest(stem, 'ло')

        elif lastlet == 'т':
            _mood = 'ind.'
            if verb.endswith(('ест', 'аст')):
                if verb[:-2] + "д" in z_eatstring + z_eatstr2 + z_givestring:
                    vsend((verb[:-2] + 'ть') if verb.endswith('аст') else (verb + 'ь'),
                          '@', 'ind.', 'p./f.', '3', 's.', _endstem, '', '')
            else:
                if len(verb) > 2 and verb.endswith(('ит', 'ет', 'ут', 'ют', 'ат', 'ят')):
                    _tense = 'p./f.'
                    stem = verb[:-2]
                    end_ing = verb[-2:]
                    prestest(stem, end_ing, form)
                if not form.endswith(('перет', 'терет')) and not self.refl:
                    vsend((verb[:-1] + 'еть') if verb.endswith('рт') else (verb + 'ь'),
                          ' 3 9 10 11 12 14 15 16 ', _mood, 'past', '', '',
                          ('имнщ' if form.endswith(("ат", "ят")) else _endstem), 'p.', 'pass.')

        elif lastlet in 'ую':
            _mood = 'ind.'
            _tense = 'p./f.'
            stem = verb[:-1]
            end_ing = lastlet
            prestest(stem, end_ing, form)

        elif lastlet == 'ь':
            stem = verb[:-1]
            end_ing = 'ь'
            if len(stem) < 2 or voc(stem, -1):
                return False
            if verb.endswith(('ть', 'чь')):
                vsend(verb, '', 'inf.', '', '', '', _endstem, '', '')
                _mood = 'imp.'
                _tense = 'p./f.'
                prestest(stem, end_ing, form)
            elif verb.endswith('шь'):
                stem = verb[:-2]

                if stem == 'пое':
                    self.contret()
                    locsucc = True
                    vsend('поесть', '@', 'ind.', 'p./f.', '2', 's.', _endstem, '', '',
                          succverb=True)
                    vsend('поесть', '@', 'imp.', '', '2', 's.', _endstem, '', '', succverb=True)
                    vsend('петь', '12 ', 'ind.', 'p./f.', '2', 's.', _endstem, '', '',
                          succverb=True)

                elif stem + "д" in z_eatstring or " " + stem + "д" in z_eatstr2 or \
                        stem + "д" in z_givestring:
                    # *if (stem in z_eatstring or stem == 'перее' or stem == 'прое' or \
                    #      stem.endswith(("ъе", "да"))):
                    vsend(stem + ('ть' if stem.endswith("да") else 'сть'), '@', 'ind.', 'p./f.',
                          '2', 's.', _endstem, '', '')
                    if not stem.endswith('да'):
                        vsend(stem + 'сть', '@', 'imp.', '', '2', 's.', _endstem, '', '')

                else:
                    if verb.endswith(('ешь', 'ишь')):
                        _mood = 'ind.'
                        _tense = 'p./f.'
                        stem = verb[:-3]
                        end_ing = verb[-3:]
                        prestest(stem, end_ing, form)
                    stem = verb[:-1]
                    end_ing = 'ь'
                    _mood = 'imp.'
                    _number = 's.'
                    prestest(stem, end_ing, form)
            else:
                _mood = 'imp.'
                _number = 's.'
                prestest(stem, end_ing, form)

        elif lastlet == 'я':
            stem = verb[:-1]
            if not stem.endswith(('г', 'ж', 'й', 'к', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'э')):
                _tense = 'p./f.'
                prestest(stem, lastlet, form)
        locsucc = self.verbseek(False, bu_ff, form, start=start)
        return locsucc

    def verbseek(self, usdic: bool, bu_ff: Optional[str], form: str, start: int = 0):
        """Test generated verbal hypotheses against dictionary data."""

        # local t_type, perhaps, _begin, o_begin, m, pl, v_begin, ;
        #  next_pl, inf_string, infor, v_acc, type_pl, akzent, ;
        #  frm, tc1, tc2, pl_1, pl_2, i, add_type, ;
        #  dfile, hd2
        # MEMVAR fhandle, refl

        if self.virtbuff:
            bu_ff = self.virtbuff

        locsucc: bool = False
        i = start

        orig_len = len(self.verb_hypotheses)  # changed in loop, we need not follow
        while i < orig_len:
            hypo = self.verb_hypotheses[i]
            if hypo.inf is not None:
                if not usdic:
                    n_entry = 1
                    n_refl_entry = 1
                perhaps = True
                _begin = 0
                o_begin = 1

                while perhaps:
                    p_test = False
                    if not usdic:
                        bu_ff = dict_get_entry(False, hypo.inf + self.rform(hypo.inf), n_refl_entry)
                        n_refl_entry += 1
                        if bu_ff is None and self.refl:
                            bu_ff = dict_get_entry(False, hypo.inf, n_entry)
                            n_entry += 1
                            if bu_ff is None:
                                perhaps = False
                            else:
                                p_test = True
                                hypo.voice = 'pass.'
                        pl = -1 if bu_ff is None else 0
                    else:
                        assert isinstance(bu_ff, str)
                        # FIXME: userdict subsetting
                        pl = bu_ff[_begin:].find("\r\n" + hypo.inf + self.rform(hypo.inf) + ' ')
                        if pl < 0 and self.refl:
                            pl = bu_ff[o_begin:].find("\r\n" + hypo.inf + ' ')
                            if pl < 0:
                                perhaps = False
                            else:
                                p_test = True
                                hypo.voice = 'pass.'
                    if pl >= 0:
                        assert isinstance(bu_ff, str)
                        v_begin = (o_begin if p_test else _begin)
                        next_pl = bu_ff[v_begin + pl:].find("\r", 1)
                        if next_pl >= 0:
                            inf_string = bu_ff[v_begin + pl:][:next_pl]
                        else:
                            inf_string = bu_ff[v_begin + pl:]
                            bu_ff += "\r"
                        v_acc = first(inf_string)
                        infor = rest(inf_string)
                        if usdic:
                            _begin = _begin + pl + next_pl
                            if p_test:
                                o_begin = v_begin + pl + next_pl
                        if "св" in infor:  # it's a verb
                            if "нет_" in infor:
                                if "деепр. нет_" in infor and hypo.mood == 'ger.' and \
                                        hypo.tense == 'p./f.' and not hypo.is_participle:
                                    continue

                                elif "прош. нет_" in infor:
                                    if "прич. прош. нет_" in infor:
                                        if hypo.is_participle and hypo.tense == 'past':
                                            if self.paspst not in ('тый', 'нный', 'анный'):
                                                continue
                                    elif hypo.tense == 'past':
                                        continue

                                elif ("_буд. нет_" in infor or "_буд. в лит. языке нет_" in infor) \
                                        and hypo.tense == 'p./f.':
                                    continue

                                elif "1 ед. нет_" in infor and hypo.tense == 'p./f.' and \
                                        hypo.person == '1':
                                    continue

                                elif "страд. нет_" in infor and \
                                        (hypo.voice == 'pass.' or hypo.gender == 'p.' or
                                         self.paspst in ('тый', 'нный', 'анный')):
                                    continue

                                elif "повел. нет_" in infor and hypo.mood == 'imp.':
                                    continue

                            if self.paspst in ('тый', 'нный', 'анный') \
                                    or hypo.gender.startswith('p.'):
                                if 'нп' in infor or ('нсв' in infor and '$' in infor) or \
                                        "?" in infor:

                                    # Здесь тонкость: нсв $ обозначает, что у данного глагола есть
                                    # пара совершенного вида, от которой и образуется причастие;
                                    # в противном случае причастие может быть образовано только при
                                    # специальной помете "_имеется прич.", а также в редких случаях
                                    # типа "ассигновать", когда после $ идет пробел.

                                    if not ("_имеется прич." in infor or "_имеется страд" in infor
                                            or '$ ' in infor):
                                        continue
                                    if "_имеется прич. страд. наст." in infor:
                                        continue
                            if self.pasprt:
                                if 'нсв' not in infor:
                                    continue
                                if 'нп' in infor and not ("_имеется страд" in infor or
                                                          "_имеется прич. страд." in infor):
                                    continue
                            if hypo.is_participle \
                                    and hypo.tense.startswith('p./f.') \
                                    and 'нсв' not in infor:
                                continue
                            if 'безл.' in infor:
                                if not (hypo.is_participle
                                        and self.paspst in ('тый', 'нный', 'анный')
                                        and 'имеется прич. страд.' in infor):
                                    if hypo.mood.startswith('imp.'):
                                        continue
                                    elif hypo.tense.startswith('p./f.') and \
                                            (hypo.person != '3' or hypo.cnumber != 's.'):
                                        continue
                                    elif hypo.tense.startswith('past') and \
                                            (hypo.cnumber.startswith('pl.') or hypo.gender != 'n.'):
                                        continue

                            if hypo.v_type.startswith("@"):
                                self.contret()
                                hypo.succverb = True
                                locsucc = True
                                hypo.info = infor
                                hypo.v_acc_pl = v_acc
                                perhaps = False
                                if self.refl and p_test:
                                    pass
                                    # FIXME: find out how to fix it
                                    #pass_test(hypo,
                                    #          p_stem=hypo.inf + self.rform(hypo.inf))
                                break
                            if hypo.mood != 'inf.':
                                if hypo.mood.startswith('ger.') and hypo.tense.startswith('p./f.'):
                                    if '"9"' in infor and 'нсв' not in infor:
                                        hypo.tense = 'past'
                                        # Here we shall allow analysis of perfective present/future
                                        # gerunds, see a similar relaxation of rules in
                                        # paradigm.prg - S.A.S
                                    elif 'нсв' not in infor and \
                                            form.endswith(("ая", "яя", "уя", "юя")):
                                        continue
                                if "нп" in infor:
                                    type_pl = infor.find("нп") + 3
                                else:
                                    type_pl = ((infor.find("нсв") + 4) if "нсв" in infor
                                               else (infor.find("св") + 3))
                                if "св, " in infor or "нп, " in infor:
                                    type_pl += 1
                                t_type = str(take_int(infor[type_pl:])) + ' '
                                if self.paspst == 'тый' or hypo.gender.startswith('p.'):
                                    # if !passparttest(form, infor):
                                    #     continue
                                    if ' ' + t_type not in ' 3 9 10 11 12 14 15 16 ':
                                        if not hypo.inf.endswith('пеленать'):
                                            continue
                                elif self.paspst == 'анный':
                                    if not passparttest(form, infor):
                                        continue
                                    if ' ' + t_type in ' 3 9 10 11 12 14 15 16 ' or \
                                            hypo.inf.endswith('пеленать'):
                                        continue
                                elif self.paspst == 'нный':
                                    if not passparttest(form, infor):
                                        continue
                                    if form.endswith('жденный') and \
                                            "_прич. страд._ -жд-" not in infor:
                                        continue
                                    elif form.endswith('женный') and \
                                            "_прич. страд._ -жд-" in infor:
                                        continue
                                if take_int(t_type) == 3:
                                    add_type = infor[type_pl + 1:][:2]
                                    if add_type == '**':
                                        t_type = '3**'
                                        if '"5"' in infor and '["5"' not in infor:
                                            if hypo.tense.startswith('past') and \
                                                    hypo.gender.startswith('m.') and \
                                                    not form.endswith('нул'):
                                                continue
                                        if '"6"' in infor and '"6"]' not in infor:
                                            if hypo.mood.startswith('ger.') and \
                                                    not form.endswith(('нув', 'нувший')):
                                                continue
                                        if form.endswith(("нул", "ула", "уло", "ули")):
                                            if "5" not in infor or \
                                                    form.endswith(("нула", "нуло", "нули")):
                                                continue
                                        if form.endswith(('нув', 'нувший')):
                                            if "6" not in infor:
                                                continue
                                akzent = infor[type_pl + len(t_type.strip()):].replace('*', '')[0]
                                if self.pasprt:
                                    if akzent == 'а':
                                        if not (t_type in '1 2 4 5 7 12 13 ' or
                                                (t_type == '6 ' and hypo.inf.endswith('ять'))):
                                            if hypo.inf != 'колебать':
                                                continue
                                    elif t_type not in '4 5 6 7 8 13 ':
                                        continue
                                frm = derform(form, self.refl)
                                if hypo.mood.startswith('imp.') and frm.endswith(('и', 'ите')) and \
                                        len(frm) >= 2 + (1 if frm.endswith('и') else 3):
                                    tc1 = (frm[-3] if frm.endswith('и') else frm[-5])
                                    tc2 = (frm[-2] if frm.endswith('и') else frm[-4])
                                    if '"3"' in infor and frm.endswith('ите'):
                                        continue
                                    if '"2"' in infor and '["2"' not in infor:
                                        continue
                                    if not voc(tc2, 1):
                                        if akzent == 'а' and \
                                            not ('вы' in frm or (not voc(tc1, 1) and
                                                                 not voc(tc2, 1)) or tc2 == 'щ'):
                                            if not ((frm.endswith('и') and '"3"' in infor)
                                                    or '["2"' in infor):
                                                continue
                                    else:
                                        if not (t_type == '4 ' and (akzent in 'вс' or 'вы' in frm)):
                                            continue
                                if hypo.mood.startswith('imp.') and \
                                        frm.endswith(('й', 'ь', 'йте', 'ьте')):
                                    tc1 = (frm[-3] if frm.endswith(('й', 'ь')) else frm[-5])
                                    tc2 = (frm[-2] if frm.endswith(('й', 'ь')) else frm[-4])
                                    if frm.endswith('ь') and '"3"' in infor and '"3"]' not in infor:
                                        continue
                                    if not voc(tc2, 1):
                                        if not ('"2"' in infor or '"3"' in infor):
                                            if akzent in 'вс' or (not voc(tc1, 1) and
                                                                  not voc(tc2, 1)) or tc2 == 'щ' \
                                                    or 'вы' in frm:
                                                continue
                                    else:
                                        if t_type == '4 ' and (akzent in 'вс' or 'вы' in frm):
                                            if '"2"' not in infor:
                                                continue
                                if "(-" in infor and "-)" in infor and "(-а́-)" not in infor and \
                                        "(-о́-)" not in infor and "(-о-)" not in infor \
                                        and "(-ё-)" not in infor:
                                    pl_1 = infor.index("(-") + 2
                                    pl_2 = infor.index("-)")
                                    test_cons: str = infor[pl_1:pl_2]
                                    if test_cons in hypo.endstem and t_type in hypo.v_type:
                                        self.contret()
                                        hypo.succverb = locsucc = True
                                        hypo.info = infor
                                        hypo.v_acc_pl = v_acc
                                    else:
                                        hypo.succverb = False
                                        continue
                                elif hypo.endstem.startswith('щ'):
                                    if not (hypo.inf[-5:][:2] in 'стск' or hypo.inf[-4] == 'щ'):
                                        continue
                                    elif t_type in hypo.v_type:
                                        self.contret()
                                        hypo.succverb = locsucc = True
                                        hypo.info = infor
                                        hypo.v_acc_pl = v_acc
                                    else:
                                        hypo.succverb = False
                                elif t_type in hypo.v_type:
                                    self.contret()
                                    hypo.succverb = locsucc = True
                                    hypo.info = infor
                                    hypo.v_acc_pl = v_acc
                                else:
                                    hypo.succverb = False
                                    continue
                            else:
                                self.contret()
                                hypo.succverb = locsucc = True
                                hypo.info = infor
                                hypo.v_acc_pl = v_acc
                        else:
                            hypo.succverb = False
                            continue
                        if hypo.succverb:
                            if self.refl and p_test:
                                pass
                                # FIXME: find out how to fix it
                                # pass_test(hypo, p_stem=hypo.inf + self.rform(hypo.inf))
                            mhypo = copy(hypo)
                            hypo.succverb = False
                            self.verb_hypotheses.append(mhypo)
                            continue

                    else:
                        perhaps = False

            i += 1
        return locsucc


def dettype(source, number, ending):
    """Return the declension type for Zalizniak's index."""

    endstem = source[-number]
    if voc(endstem, 1):
        if endstem == 'и':
            type = "7"
        else:
            type = "6"
    else:
        if endstem == "ц":
            type = "5"
        elif endstem in "чжшщ":
            type = "4"
        elif endstem in "кгх":
            type = "3"
        elif endstem in "йь":
            type = "6"
        elif ending in "яеь":
            type = "2"
        else:
            type = "1"
    return type


def nomindex(source, ending, ablaut):
    """Generate the nominal "Zalizniak"-type index: used in the
    LISTING function.
    The function generates the index as a specially formatted
    string and passes it to the BUILDIND function."""

    abladd = "*" if ablaut else ""
    if not ablaut:
        if voc(source, -1) or source.endswith(("ой", "ый", "ий")):
            if ending != "0":
                extradd = 1 if source.endswith(("ой", "ый", "ий")) else 0
                if not voc(source, -2 - extradd) and not voc(source, -3 - extradd):
                    abladd = "[*]"
        elif source[-2] == "о":
            if not voc(source, -3):
                abladd = "[*]"
        elif source[-2] == "е":
            abladd = "[*]"

    if source.endswith("ие"):
        if " " + ending + " " in z_n_str:
            type = dettype(source, 1, source[-1])
            retind = "с(о) " + type + abladd + "x"
        elif " " + ending + " " in z_a_str:
            if voc(source, -3):
                type = "6"
            elif source[-3] in 'кгх':
                type = "3"
            elif source[-3] in 'чжшщ':
                type = "4"
            else:
                type = "2"
        retind = "{мн. одуш. п " + type + abladd + "x}" + ", {мн. неод. п " + type + abladd + "x}"

    elif source.endswith(("ый", "ий", "ой", "ая", "яя", "ое", "ее", "ые")):
        if ending + " " in z_a_str:
            if source.endswith(("ый", "ые")):
                type = "1"
            elif source.endswith(("ий", "ее")):
                if voc(source, -3):
                    type = "6"
                elif source[-3] in 'кгх':
                    type = "3"
                elif source[-3] in 'чжшщ':
                    type = "4"
                else:
                    type = "2"
            elif source.endswith(("ой", "ое", "ая")):
                if source[-3] in 'кгх':
                    type = "3"
                elif source[-3] in 'чжшщ':
                    type = "4"
                else:
                    type = "1"
            elif source.endswith(("яя")):
                if voc(source, -3):
                    type = "6"
                else:
                    type = "2"

            if source.endswith(("ый", "ий", "ой")):
                addition = ''
                if source.endswith(("ский", "цкий")):
                    addition = "!"
                    abladd = ''
                elif source.endswith("ской"):
                    addition = "?"
                if source.endswith(("ский", "цкий", "енький", "онький")):
                    addition += "~"
                if ending + ' ' in "ый ого ому ом ые ых ым ыми ":
                    retind = "{м(о) п}, {п}"
                else:
                    retind = "п"
                if source.endswith("ой"):
                    retadd = " " + type + abladd + "в"
                else:
                    retadd = " " + type + abladd + "а"
                retind = retind.replace("п", "п" + retadd)
                if addition:
                    if "{п}" in retind:
                        retind = retind[:-1] + addition + "}"
                    else:
                        retind += addition
            elif source.endswith(("ая", "яя")):
                retind = "ж(о) п " + type + abladd + "x"
            elif source.endswith(("ое", "ее")):
                retind = "с(о) п " + type + abladd + ("x" if source.endswith("ое") else "а")
            elif source.endswith("ые"):
                retind = "".join(("{мн. одуш. п ", type, abladd, "x}",
                                  ", {мн. неод. п ", type, abladd, "x}"))
        else:
            if source.endswith("й"):
                retind = "м(о) 6" + abladd + "x"
            elif source.endswith("я"):
                retind = "ж(о) 6" + abladd + "x"
            else:
                retind = "с(о) 6" + abladd + "x"

    elif source.endswith(("ы", "и")):
        type = dettype(source, 1, source[-1])
        if ending + ' ' in "ов ей ":
            retind = "мн. м " + type + abladd + "x"
        elif ending == "0":
            retind = "мн. ж " + type + abladd + "x"
        else:
            retind = "{мн. м " + type + abladd + "x}, {мн. ж " + type + abladd + "x}"

    elif source.endswith(("а", "я")):
        type = dettype(source, 1, source[-1])
        retind = "ж(о) " + type + abladd + "x"
        if source.endswith(("ина", "ына", "ова", "ева")):
            retind = "{" + retind + "}, {жо мс 1x}"

    elif source.endswith(("о", "е")):
        if source[-1] == "е" and source[-2] not in 'чжшщцлр':
            retind = ""
        else:
            type = dettype(source, 1, source[-1])
            retind = "с(о) " + type + abladd + "x"

    elif source[-1] == "ь":
        type = dettype(source, 1, source[-1])
        if ending + ' ' in z_m_str and ending + ' ' in z_f_str2:
            retind = "{м(о) " + type + abladd + "x}" + ", {ж(о) 8" + abladd + "x}"
        elif ending + ' ' in z_m_str:
            retind = "м(о) " + type + abladd + "x"
        else:
            retind = "ж(о) 8" + abladd + "x"

    else:
        type = dettype(source, 0, source[-1])
        if not voc(source, -1):
            retind = "м(о) " + type + abladd + "x"
            if source.endswith(("ин", "ын", "ов", "ев")):
                retind = "{" + retind + "}, {мо мс 1x}"
        else:
            retind = ""

    return retind


def buildind(source: str, index: str, indarr: List[str]) -> int:
    """Fashion the array (INDARR) of Zalizniak's indices for a given
    form (SOURCE) using the INDEX string formatted within the
    NOMINDEX function."""

    subind1, subind2 = '', ''
    abladd = False
    if "[*]" in index:
        index = index.translate(REMOVE_BRACKETS)
        abladd = True
    q = index.rfind("{")
    if q >= 0:
        subind1 = index[:q - 2].translate(REMOVE_CURLY)
        if "м(о) п" in subind1:
            abladd = False
            subind1 = subind1.replace('*', '')
        subind2 = index[index.index("}") + 2:].translate(REMOVE_CURLY)
    else:
        subind1 = index

    q = subind1.rfind('(')
    if q >= 0:
        aaddch(indarr, addcase(source + ' ' + subind1[:q - 1] + rest(subind1, ")")))
        aaddch(indarr, addcase(source + ' ' + subind1.translate(REMOVE_PARENS)))
        if abladd:
            subind1 = subind1.replace('*', '')
            aaddch(indarr, addcase(source + ' ' + subind1[:q] + rest(subind1, ")")))
            aaddch(indarr, addcase(source + ' '
                                   + subind1.translate(REMOVE_PARENS)))  # FIXME: 2nd REMOVE_PARENS?
    else:
        aaddch(indarr, addcase(source + ' ' + subind1))
        if abladd:
            subind1 = subind1.replace('*', '')
            aaddch(indarr, addcase(source + ' ' + subind1))

    if subind2:
        q = subind2.find("(")
        if q >= 0:
            aaddch(indarr, addcase(source + ' ' + subind2[:q - 1] + rest(subind2, ")")))
            aaddch(indarr, addcase(source + ' ' + subind2.translate(REMOVE_PARENS)))
            if abladd:
                subind2 = subind2.replace('*', '')
                aaddch(indarr, addcase(source + ' ' + subind2[:q - 1] + rest(subind1, ")")))
                aaddch(indarr, addcase(source + ' ' + subind2.translate(REMOVE_PARENS)))
        else:
            aaddch(indarr, addcase(source + ' ' + subind2))
            if abladd:
                subind2 = subind2.replace('*', '')
                aaddch(indarr, addcase(source + ' ' + subind2))
    return len(indarr)


def addcase(index: str) -> str:
    """Add the case information to the index string.
    This information is not displayed on the screen, but is
    utilized for subsequent paradigm building (when the
    generated index is passed to the PARADIGM program."""

    start: int = 0

    if index.endswith("н"):
        return index + "<>{}"

    if "мн. " in index:
        start = 80 if " п " in index else 30

    if re.search(r' п | мс ', index):
        c_str = z_a_cases[start:]
        if re.search(r' м | мо | ж| с', index):
            e_str = z_a_str[start:start + len(z_a_str) - 20]
        else:
            e_str = z_a_str[start:]
    else:
        c_str = z_cases[start:]
        if re.search(r' м | мо ', index):
            e_str = z_m_str[start:]
        elif ' с' in index:
            e_str = z_n_str[start:]
        elif '8' in index:
            e_str = z_f_str2[start:]
        else:
            e_str = z_f_str[start:]

    return index + "<" + e_str + ">" + "{" + c_str + "}"


def firstvoc(string):
    """Position of the 1st vowel within a string"""
    return next((i for i in range(1, len(string)) if voc(string, i)), 0)


def lastacc(string, akz_pl):
    """Returns True if the accent is on the last vowel of STRING."""
    return not any(voc(string, i) for i in range(akz_pl + 1, len(string)))


def firstacc(string, akz_pl):
    """Returns True if the accent is on the first vowel of STRING."""
    return not any(voc(string, i) for i in range(akz_pl - 1, 1, -1))


def buildvind(inf, v_type: str, endstem, indarr):
    """Build a verbal "Zalizniak"-type index for a generated hypothesis."""

    v_type = v_type.lstrip()
    if not v_type:
        if inf.endswith(("овать", "евать")):
            v_type = "2 "
        elif inf.endswith(("ать", "ять")):
            v_type = "1 5 6 "
            if inf.endswith(("тать", "тять")):
                endstem = "тщ "
        elif inf.endswith("еть"):
            v_type = "1 5 "
            if inf.endswith("теть"):
                endstem = "тщ "
        elif inf.endswith("ить"):
            v_type = "4 "
            if inf.endswith("тить"):
                endstem = "тщ "
        elif inf.endswith("нуть"):
            v_type = "3 "
        elif inf.endswith(("сти", "сть", "зти", "зть")):
            v_type = "7 "
            if inf.endswith(("сти", "сть")):
                endstem = "тсдб "
        elif inf.endswith("чь"):
            v_type = "8 "
            endstem = "гк "

    typearr: List[str] = []
    while v_type:
        loc_type = first(v_type)
        int_loc_type = take_int(loc_type)
        if int_loc_type < 9:
            if int_loc_type == 2 and not inf.endswith(("овать", "евать")):
                pass
            elif int_loc_type == 5 and inf.endswith("ать") and \
                    not inf.endswith(("чать", "шать", "жать", "пать", "нать")):
                pass
            elif int_loc_type == 4 and not inf.endswith("ить"):
                # тип 4 для глаголов не на -ить проскочил случайно;
                # здесь мы его отсеем!
                pass
            elif int_loc_type == 3 and not inf.endswith("нуть"):
                pass  # аналогично:
            else:
                typearr.append(loc_type + "x")
            v_type = rest(v_type)

    stemarr: List[str] = []
    for type_ in typearr:
        if take_int(type_) > 3:
            if endstem:
                added = False
                for k in range(len(endstem)):
                    nextlet = endstem[k]
                    if nextlet == "и":
                        nextlet = "м"
                        k += 1
                    elif nextlet == "с" and endstem[k + 1] == "т":
                        nextlet = "ст"
                        k += 1

                    if nextlet == "щ" and inf.endswith(("тить", "тать",
                                                        "теть")):
                        stemarr.append(type_)
                        nextadd = " (-" + nextlet + "-)"
                    elif nextlet in "стдбгк" and take_int(type_) > 6:
                        nextadd = " (-" + nextlet + "-)"
                    else:
                        nextadd = ''
                    if nextadd:
                        stemarr.append(type_ + nextadd)
                        added = True
                if not added:
                    stemarr.append(type_)
            else:
                stemarr.append(type_)
        else:
            stemarr.append(type_)

    for stem in stemarr:
        aaddch(indarr, inf + ' нсв нп ' + stem)
        aaddch(indarr, inf + ' нсв ' + stem)
        aaddch(indarr, inf + ' св ' + stem)
        aaddch(indarr, inf + ' св нп ' + stem)

    return len(indarr)


def dictwrite(rw: str, wt: str):
    """Place the word (RW) and comments to it (WT) into USER.DIC."""

    if ord(rw) < 128:   # necessary preparations if the word is English
        if rw.startswith(("'", "`", "-")):
            rw = rw[1:]
        if rw.endswith(("'", "`", "-")):
            rw = rw[:-1]
        rw = engf(rw)
        wt = "[ ] " + wt

    # FIXME: actual userdict write
    # replace rusword with rw
    # replace wordtype with wt
    return (True)


def aaddch(array: List[str], string: str) -> None:
    """Add the STRING to the ARRAY, if it is not there already."""
    if string not in array:
        array.append(string)


def add_varia(form: str, source_forms: Iterable[str]) -> str:
    varmess: str = ""

    r'''
    if False:  # file(DICTPATH+"varia.dbf"):
        curson = r"\I"
        cursoff = r"\i"

        source_forms = set(chain([form], source_forms))
        sql = """SELECT * FROM varia WHERE word IN ? ORDER BY word"""
        for row in hypothetical_fetch(sql, source_forms):
            hword = row['word']
            if row['upcase']:
                hword = hword[0].upper() + hword[1:]
            varmess += hword + " [" + row['language'] + "]: " + '; '.join(
                k.capitalize() + " " + curson + row[k] + cursoff
                for k in row.keys()
                if k not in ('word', 'upcase', 'language') and row['k']
            ) + "\r\n"

        sql = """SELECT * FROM varia WHERE fts_match(?) AND word not in ?"""
        for row in hypothetical_fetch(sql, (" OR ".join(source_forms),
                                            source_forms)):
            varmess += "\r\n".join(
                ''.join(("[", row["language"], "] ", row['word'], " : (", k.capitalize(), ") ",
                         curson, row[k], cursoff))
                for k in row.keys()
                if k not in ('word', 'upcase', 'language') and row['k']
            )
    '''
    return varmess


def elastacc(string: str) -> bool:
    """An analog of LASTACC, but for English transcribed wordforms."""

    accented: bool = False
    voccount: int = 0
    prev_c, prev_prev_c = ".", "."
    for c in string:
        if c == "'":
            accented = True
        if accented:
            if c in "aeiouƛłŋəɔ" and prev_c not in "aeiouƛłŋəɔ":
                voccount += 1
        if c == "]":
            if prev_c in "nl" and prev_prev_c not in "aeiouƛłŋəɔ":
                voccount += 1
            break
        prev_prev_c, prev_c = prev_c, c
    return voccount <= 1


def depal(letter):
    """Morphonological "depalatalization"."""
    return 'кгх'['чжш'.index(letter)]


def devoc(string, root):
    """Deletes morphonologically excessive vowels from verbal prefixes."""

    if string.endswith(("во", "со")):
        return string[:-1]
    elif string.endswith(('ото', 'обо', 'подо', 'надо', 'редо')):
        if len(string) > 2:
            return string[:-1]
    elif string.endswith(('разо', 'низо', 'возо', 'изо', 'взо')):
        if root[0] in 'кпстфхцчшщ':
            return string[:-2] + 'с'
        else:
            return string[:-1]
    return string


def searchcase(ending, c_str: str, e_str: str,
               form: str,
               nonnom: bool = False,
               shock: bool = False,
               infor: str = "",
               subst: bool = False,
               genus: str = "",
               _class: str = "",
               anim: bool = False) -> str:
    """Returns current noun case utilizing a variety of information."""

    res_str: str = ''

    def walk_cases(cases: str, endings: str, ending: str):
        pos: int = -1
        while True:
            pos = endings.find(ending + ' ', pos + 1)
            if pos < 0:
                break
            casename: str = cases[pos:pos + 3]
            if casename.startswith('p'):
                # Happens when looking for -а in -męn endings, it's the fastest
                # way to deal with it without switching data structures
                continue
            yield casename

    for casename in walk_cases(c_str, e_str, ending):
        if casename.startswith(('Ns', 'As')) and nonnom:
            continue
        if casename.startswith('Gp'):
            if shock:
                continue
            elif "Р. мн. нет_" in infor:
                continue
        if casename.startswith('G2') and 'Р2' not in infor:
            continue
        if casename.startswith('L2') and not ('П2' in infor or 'п2' in infor):
            continue
        if subst:
            s_genus: str = genus.replace('мс', '')
            if _class == 'п':
                if 'ж' in s_genus and ('m' in casename or 'n' in casename):
                    continue
                elif 'м' in s_genus and ('f' in casename or 'n' in casename):
                    continue
                elif 'с' in s_genus and ('m' in casename or 'f' in casename):
                    continue
            if 'a' in casename and not anim:
                continue
            elif 'i' in casename and anim:
                continue
        elif '!!ф.' in infor and _class == 'п' and 'n' in casename:
            # FIXME: Forbid neutral gender for family names (макарове < макаров Lsn); recheck
            continue

        res_str += ',' + casename
        # if res_str:
        #     self.success = True  # FIXME was it really needed?
        if not subst:
            if 'Nsm' in res_str:
                if form.endswith('ой'):
                    res_str += ',Asi,Gsf,Dsf,Isf,Lsf'
                    break
    return res_str


def splitcase(delim: List[dict], subnum, cases: str) -> List[dict]:
    if "Indecl." in cases:
        cases = ",Ns,As,Np,Ap,Gs,Ds,Is,Ls,Gp,Dp,Ip,Lp"
        # *elseif "Cmp" in cases
        # *cases = ",Ns,As"
        # *delim[subnum][S_PSPEECH] = "n."
        # *delim[subnum][S_GENDER] = "n"
        # *delim[subnum][S_ANIM] = "i"
    if cases[0] != ",":   # FIXME: always true?
        cases = "," + cases.lstrip()
    acase: List[str] = cases.split(",")
    ptr = delim[subnum]
    for i, case in enumerate(acase):
        if i > 0:
            ptr = copy(ptr)
            delim.append(ptr)
        if case == "Cmp":
            case = "C"
        for testlet in case:
            if testlet in "NGDAILSC":
                ptr['case'] = testlet
                if ptr['pspeech'] == 'num.':
                    if testlet == 'N':
                        ptr['pspeech'] = 'numn.'
                        ptr['number'] = 'sp'
                    elif testlet == 'A':
                        if 'a' not in case:
                            ptr['pspeech'] = 'numn.'
                            ptr['number'] = 'sp'
            elif testlet in "ps":
                ptr['number'] = testlet
            elif testlet in "mfn":
                ptr['gender'] = testlet
            elif testlet in "ai ":
                ptr['anim'] = testlet.strip()
                if ptr['pspeech'] in "a.part.":  # "Asi" and "Asa" - only masculine:
                    if ptr['number'].startswith('s'):
                        ptr['gender'] = 'm'

    return delim


def grammar(string, full=False, delim=None):
    """
    Returns the results of morphological analysis for every word
    within a string. if the second parameter is passed, all information:
    from the dictionary is also appended to each analysis result.
    """

    buffer = ""

    for word in re.sub(r'-\r?\n', '', string).split():
        t_t_word = word
        t_word = letterize(t_t_word)
        skipt_word = False
        if t_word.startswith("-"):
            t_word = t_word[1:]
        if ruslat(t_word):
            if t_word >= chr(128) and not t_word.startswith(("ü", "ö", "е́")):
                t_word = rusf(t_word)
            elif t_word >= chr(65):
                t_word = engf(t_word)
        else:
            skipt_word = True
        if not skipt_word:
            parser = Parser()
            t_buf = parser.rmorph(t_word, delim=";" if delim is None else delim, full=full)
            success = len(t_buf) > 0

            '''
#ifdef STAR32
            if build_stage >= 0 and build_stage <= 2 or build_stage == 4:
                return t_buf
            elif build_stage == 3:
                build_stage = 2
                if not isinstance(t_buf, str):
                    # An incorrect hypothesis (usually an illegal accent variant)
                    # It is too late to remove it from the menu, but no paradigm
                    # should be suggested or generated.
                    return ""
                t_word = rmorph(t_word, delim, full)[build_locoption]
                t_word = "{} [{}]".format(t_word.split(" ", maxsplit=1))
                return "{%s } %s" % (t_word, t_buf)

            # insert semicolons and separate Roman numbers in English translations
            t_buf = for32(t_buf)
##endif
            '''
        else:
            success = False
            t_buf = t_word
        if not t_buf:  # happens with reflexives in perfective aspect:
            success = False
        buffer += "\x10%s" % word if not success else "{%s}" % t_buf

    return buffer


def for32(s: str) -> str:
    result: str = ''

    while '\x04' in s:
        result += s[:s.index('\x04') + 1]
        s = rest(s, "\x04") + " "
        addstring: str = "; "
        while s:
            res = first(s)
            s = rest(s)
            if not s:
                addstring = ""
            if res.endswith("]"):
                result += res + " "
                break
            m = re.match(r"^(.+)([IVX].*)$", res)
            if m:
                res = m.group(1) + " " + m.group(2) + addstring
            else:
                res = res + addstring
            result += res
    result += s
    return result


def ruslat(string: str) -> bool:
    return bool(re.match(r"[A-Za-zА-Яа-яäöü]", string))


def validword(word: str) -> bool:
    okey = False
    form = letterize(word)
    parser = Parser(contmrph=True)
    if isstandard(form):
        if form >= chr(128):
            okey = parser.rmorph(rusf(form))
        elif form >= chr(65):
            okey = parser.rmorph(engf(form))
    assert isinstance(okey, bool)
    # if (okey) != 'L':  # This is a Harbour thing: okey sometimes
    #     # (but apparently only in cases of successful analysis)
    #     # turns out to be NIL or an array... - S.A.S
    #     okey = True
    return (okey)


def accent(s: str) -> str:
    words_: str = grammar(s, False, ";")

    if polyvocal(s) == 0:
        return s

    if words_.startswith("\x10"):
        return words_

    if "//" in words_:
        words_ = words_.replace("//", ";")

    if "[" in words_ and "[_" not in words_:  # pronouns etc.
        words = re.match(r"^[^\[]*\[[^ ]* ([^\]]*)\]", words_) \
            .group(1).strip().split()  # type: ignore
    else:
        words = words_[words_.rindex("}") + 2:].split(";")
    p: str = s.lower().strip()
    success, s = checkacc(s, p, words)
    if not success and re.search(r"нье|ньи|нья|нью", p):
        p = p.replace("нь", "ни")
        success, s = checkacc(s, p, words)
    return s


def checkacc(s: str, p: str, words: List[str]) -> Tuple[bool, str]:
    success: bool = False
    for i in range(len(words)):
        words[i] = rest(words[i].strip())
        if " " in words[i]:  # infinitives
            words[i] = first(words[i])
        if words[i].translate(REMOVE_ACUTE_DIERESIS) == p:
            success = True
            for n, c in enumerate(words[i]):
                if c in "\u0301\u0308":
                    s = accproc(s, c, n + 1)
    return success, s


def accproc(s: str, accmark: str, k: int) -> str:
    t = 0
    for q in range(len(s)):
        t += 1
        if s[q] == accmark:
            t += 1
        if q == k - 2:
            if t < len(s):
                if s[t] != accmark:
                    s = s[:t] + accmark + s[t:]
            else:
                s += accmark
    return s


def passparttest(form: str, infor: str) -> bool:
    """New function: extracting the past passive participle from the
    dictionary entry and checking it against the input."""

    ps = "_прич. страд._"
    if ps in infor:
        pmark = infor[infor.index(ps) + len(ps) + 1:].lstrip()
        pmark = " " + deacc(first(pmark)) + " "
        # The following is for cases like обкраденный//обокраденный
        if "//" in pmark:
            pmark = pmark.replace("//", " ")
        if pmark == ' -жд- ':  # special case, we're not processing it here:
            return True
        if " " + form + " " in pmark:
            return True
        return False
    return True
