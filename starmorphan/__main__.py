import argparse
import os
import logging
import re
import signal
import socket
import socketserver
import sys
import traceback

from . import morpho
from . import util


FOUNDCOL = '<FONT COLOR="#FF0000">'
ENDFONT = '</FONT>'
RE_ROMAN_NUMS = re.compile("[IVX]")

RUSSIAN_MESSAGES = {
    "Word not found": "«®¢® ­¥ ­ ©¤¥­®",
    "Source form:": "áå®¤­ ï ä®à¬ :",
    "Dictionary information:": "«®¢ à­ ï ¨­ä®à¬ æ¨ï:",
    "Translation:": "¥à¥¢®¤:",
    "Morphological characteristics:": "®àä®«®£¨ç¥áª ï å à ªâ¥à¨áâ¨ª :",
}

IEOL = b"\r\n"
OEOL = b"\n"
STSEQ = OEOL + b"\x11" + OEOL
ENDSEQ = OEOL + b"\x12" + OEOL

STARTAB = r" ░  ▒  ▓  │  ┤   ╡  ╢  ╖   ╕  ╣  ╗  ╝  ╜  ╛  ┐  └  ┴  ┬  ├  ─  ┼  ╞  ╟  ╚  ╔  ╩  ╦  ╠  ╬   ╧  ╨  ╤  ╥  ╙  ╘   ╒  ╓   ╫  ╪  ┘  ┌  █  ▄  ▌  ▐  ▀  Ё  ё  Є  є   Ї  ї  Ў  ў  °  ∙  ·  √   №  ¤  ■     "
CODETAB = r' a: /  a" ,  a": c. c^ c^. d| e: `  "  e| ?_ _  c, g| G| h_ :  i: y  y: %  &  k. l| l^ l^. l/ L^ n| o: o" o": o| o|: p. q. S  ~  \  .  s^ t. >  t| u: u" u": @  @: ^  w| h| x| z| z^| z^ ?  ?| Y  '
RUSTAB = ' а  б  в  г  д  е  е" ж  з  и  й  к  л  м  н  о  п  р  с  т  у  ф  х  ц  ч  ш  щ   ъ  ы  ь  э  ю  я  '
LATTAB = " a  b  v  g  d  je jo zh z  i  j  k  l  m  n  o  p  r  s  t  u  f  kh c  ch sh shh `  y  '  e  ju ja "
STTAB = STARTAB + RUSTAB
CTAB = CODETAB + LATTAB


workers = []


def l18n(s: str, flags: str) -> str:
    return s if 'e' in flags else RUSSIAN_MESSAGES[s]


def starconv(string: str, _from: bool = False) -> str:
    target = ""

    if _from:
        for letter in string:
            if letter == " ":
                target += letter
            else:
                target += (CTAB[atpl:atpl + 3]
                           if (atpl := STTAB.index(letter)) > -1
                           else letter)
    else:
        k = 3
        mode = 0
        if "sup>" in string:
            string = string.replace("<sup>", r"\H").replace("</sup>", r"\h")
        i = 0
        while i < len(string):
            letter, nextlet = string[letter:letter + 2]
            if letter in "}]{[|":
                if nextlet == letter:
                    target += letter
                    i += 1
                else:
                    mode = max("}]{[|".index(letter) - 1, 0)
            elif letter == chr(29):  # "&"
                if nextlet != chr(38):  # "#"
                    mode = 0
                    target += chr(29)  # "&"
                else:  # \x1D\x26.\d\d\d
                    m = re.match(r"...(\d\d?\d?)", string)
                    if m and (letter := int(m.group(1))) > 0:
                        target += (chr(letter + 88) if letter <= 87
                                   else chr(letter + 136))
                        i += 6
                    else:
                        # some unrecognizable letter,e.g.accent mark:
                        # marked as &#0;
                        i += 3
            elif mode == 0:
                target += letter
            else:
                if mode == 2:
                    testtab = LATTAB
                    restab = RUSTAB
                elif mode == 3:
                    testtab = RUSTAB
                    restab = LATTAB
                else:  # mode == 1
                    testtab = CODETAB
                    restab = STARTAB
                if k == 0:
                    target += nextlet
                    i += 1
                    if i >= len(string):
                        break
                    k = 3
                if (atpl := testtab.index(" " + string[i:i + k] + " ")) > -1:
                    target += restab[atpl + 1:atpl + 4].strip()
                    i += k - 1
                    k = 3
                else:
                    k -= 1
                    i -= 1
            i += 1
    return target


def proctrans(string: str, sform: str, flags: str) -> str:
    """Insert crossreferences etc."""

    sform = "[" + starconv("|" + sform, False)
    refstr1 = '<A HREF="morph.cgi?flags=%s&word=' % flags
    refstr2 = '">'
    refstr3 = "</A>"
    pl = string.index(chr(4))
    start = string[:pl + 1]
    trans = (string[pl+2:].lstrip() + " ").replace("^", " ")
    result = ""
    while trans:
        res, trans = trans.split(" ", maxsplit=1)
        m = RE_ROMAN_NUMS.search(res)
        suffix = ";"
        if m and (pl := m.start()) > 0:
            res, numinf = res[:pl], res[pl:]
            suffix = " " + numinf + ";"
        result += ''.join((refstr1, res, "||", sform, refstr2, res, refstr3,
                           suffix))
    result = result[:-1] + "<p>"
    return start + result


def cosmetic(string: str, flags: str) -> str:
    """Does what theoretically a CGI script would have to do...: polish
       the morphological output for the browser"""

    def _(s: str) -> str:
        return l18n(s, flags)

    if string and ord(string[0]) < 65:
        return _("Word not found")

    result = ""
    counter = 1
    string = (string[1:-2].strip()
              .replace("_", " ")
              .replace("{", "<p>")
              .replace("}", "<p>"))
    if util.is_cyrillic(string[0]):
        while "]" in string:
            sform, dictline = string.split("[", maxsplit=1)
            dictline, string = dictline.split("]", maxsplit=1)
            output = "%d. %s %s<p>" % (counter, _("Source form:"), sform)
            dictline = _("Dictionary information:") + " " + dictline
            if chr(4) in dictline:
                dictline = (proctrans(dictline, sform, flags)
                            .replace(chr(4), "<p> " + _("Translation:") + " "))
            else:
                dictline += "<p>"
            string = string.lstrip()
            resan = util.first(string, ";")
            resan = _("Morphological characteristics:") + " " + resan + "<p>"
            result = result + output + dictline + resan + "<p>"
            counter += 1
        if counter == 2:
            result = result[3:]
    else:
        for i in range(1, 10):
            while (pl := string.index(" " + str(i))) > -1:
                _delim = ("<p>" if ")" in string[pl + 2:pl + 4] else "###")
                string = string[:pl] + _delim + string[pl + 1:]
        result = string.replace("\r\n", "<p>").replace("###", " ") + " "
        result = finaltouch(result)
    return result


def finaltouch(string: str) -> str:
    """Final touch: adjust font colors and sizes"""

    string = string.replace(" <p> ", "<p>")
    res = ""
    while string:
        pl = string.index("<p>")
        sub1, string = string[:pl + 2], string[pl + 3:]
        tword, rest = sub1.split(" ", maxsplit=1)
        if (not tword.isdigit()) or int(tword) == 0:
            sub1 = ''.join(('<FONT COLOR="#009900"><FONT SIZE=+2>', tword,
                            '</FONT></FONT>', rest))
            if "[" in sub1 and "]" in sub1:
                sub1 = (sub1.replace("[", '<FONT COLOR="#993366">[')
                        .replace("]", "]</FONT>"))
        res += sub1
    return res


def morphan(parm: bytes, utf8: bool = False) -> bytes:
    """Procedure for calling RMORPH from DCONNECT"""

    remote_addr, parm = parm.split(IEOL, maxsplit=1)
    http_referer, parm = parm.split(IEOL, maxsplit=1)
    parm = parm[4:]  # subtract "_MO_"

    # NB: transinf seems not to be used any more, hopefully because Yura
    # included everything into Convert.dll.
    # But I shall leave it - just in case...
    param1 = parm.strip().split(IEOL, maxsplit=1)[0]
    if b"\x0E" in param1:
        param1, cgiext = param1.split(b"\x0E", maxsplit=1)
        flags = cgiext[3:]
        cgiext = "." + cgiext[:3]
    if param1.startswith(b"-"):
        param2 = util.rest(param1, b" ")
        param1 = param1[1:2]
        parm = param2
        if param1.upper() == "A":
            analyse = True
            transinf = False
        elif param1.upper() == "T":
            if not util.is_cyrillic(parm[0]):
                return b""
            analyse = False
            transinf = True
        else:
            analyse = False
            transinf = False
    else:
        param2 = ""
        parm = param1
        analyse = False
        transinf = False

    # Strip CR from LINUX input
    parm = parm.replace(b"\r", b"")
    if b"||" in parm:
        parm, testmsg = parm.split(b"||", maxsplit=1)
        testmsg = starconv("[" + testmsg[1:], _from=False)
        testmsg = testmsg[:max(4, len(testmsg) - (5 if testmsg.endswith("âì")
                                                  else 3))]
    if b" " in parm:
        parm = util.first(parm)
    #parm = starconv(parm, _from=False)
    parm = parm.decode("UTF-8" if utf8 else "CP866")

    try:
        if analyse or not util.is_cyrillic(parm[:1]):
            a = cosmetic(morpho.grammar(parm, full=True), flags)
        else:
            parser = morpho.Parser()
            a = parser.rmorph(parm, delim="\r\n", build_par=True,
                              transinf=transinf).replace(chr(241) + "'", '¥"')
            if not a:
                a = l18n("Word not found", flags)
        if utf8:
            a = a.encode()
        else:
            a = a.replace("\u0301", "'").encode("CP866")
        return STSEQ + a + ENDSEQ
    except Exception as e:
        logging.error(e)
        traceback.print_exc()
    return b""


def sigterm_handler(sig, frame):
    for _ in workers:
        os.kill(_, signal.SIGINT)


def worker(host: str, port: int, utf8: bool):
    class Server(socketserver.TCPServer):
        allow_reuse_address = True
        allow_reuse_port = True  # backported from Python 3.11, required on OSX
        request_queue_size = 128

        def server_bind(self):
            """Called by constructor to bind the socket.
            May be overridden.
            """
            if self.allow_reuse_address:
                self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,
                                       1)
            if self.allow_reuse_port:
                self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT,
                                       1)
            self.socket.bind(self.server_address)
            self.server_address = self.socket.getsockname()

    class Handler(socketserver.StreamRequestHandler):
        utf8 = False

        def handle(self):
            data = self.rfile.read()
            if b"\n_MO_" in data:
                answer = morphan(data, utf8=self.utf8)
                if b"\n-2\n" not in answer:
                    self.wfile.write(answer)

    class Utf8Handler(Handler):
        utf8 = True

    with Server((host, port), Utf8Handler if utf8 else Handler,
                bind_and_activate=False) as server:
        server.server_bind()
        server.server_activate()
        server.serve_forever()


def runner(host, port, n_workers=1, utf8=False):
    logging.getLogger(__name__).info('%d workers listening on %s:%d',
                                     n_workers, host, port)
    if n_workers == 1:
        worker(host, port, utf8)
    else:
        if sys.platform.startswith('linux'):
            original_sigint_handler = signal.signal(signal.SIGINT,
                                                    signal.SIG_IGN)
        for _ in range(n_workers):
            pid = os.fork()
            if pid:
                workers.append(pid)
            else:
                worker(host, port, utf8)
                os._exit(0)
        if sys.platform.startswith('linux'):
            signal.signal(signal.SIGINT, original_sigint_handler)
        signal.signal(signal.SIGTERM, sigterm_handler)

        try:
            for _ in range(n_workers):
                os.wait()
        except KeyboardInterrupt:
            for _ in workers:
                os.kill(_, signal.SIGINT)


def analyse(args: argparse.Namespace):
    for wordform in args.WORDFORM:
        print(cosmetic(morpho.grammar(wordform, full=True), flags=flags))


def generate(args: argparse.Namespace):
    parser = morpho.Parser()
    for lexeme in args.LEXEME:
        a = parser.rmorph(lexeme, delim="\r\n", build_par=True,
                          transinf=args.transinf).replace(chr(241) + "'", '¥"')
        if not a:
            a = l18n("Word not found", flags=flags)
        print(parser.rmorph(lexeme, delim="\n", build_par=True,
                            transinf=args.transinf))


def serve(args: argparse.Namespace):
    runner(args.HOST, args.PORT, args.n_workers, args.utf8)


argparser = argparse.ArgumentParser(
    description="Morphological analysis and synthesis",
    prog="python -m starmorphan",
)
argparser.add_argument('-r', dest='russian', action='store_true',
                       help='Russian localized output')
argparser.set_defaults(func=lambda x: argparser.print_help())
subparsers = argparser.add_subparsers(help='sub-command help')
parser_analyse = subparsers.add_parser('analyse', help='analyse wordform')
parser_analyse.add_argument('WORDFORM', nargs='+',
                            help='wordforms to analyse')
parser_analyse.set_defaults(func=analyse)
parser_generate = subparsers.add_parser('generate', help='generate paradigm')
parser_generate.add_argument('-t', dest='transinf', action='store_true',
                             help='use transinf format')
parser_generate.add_argument('LEXEME', nargs='+',
                             help='lexemes to generate paradigm for')
parser_generate.set_defaults(func=generate)
parser_serve = subparsers.add_parser('serve', help='start server')
parser_serve.add_argument('--host', default="127.0.0.1", dest='HOST',
                          help='host to listen at [127.0.0.1]')
parser_serve.add_argument('-j', default=4, type=int, dest='n_workers',
                          help='worker count [4]')
parser_serve.add_argument('-u', dest='utf8', action='store_true',
                          help="use UTF-8")
parser_serve.add_argument('PORT', default=8001, type=int, nargs='?',
                          help='port to listen [8001]')
parser_serve.set_defaults(func=serve)

args = argparser.parse_args()

flags = "r" if args.russian else "e"

args.func(args)
