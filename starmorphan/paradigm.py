"""Module for Russian paradigm building.
Author: S.A.Starostin
Works in conjunction with MORPHO.PRG.

LIST OF FUNCTIONS
NOMPAR
MORPHON [S]
VOCTRANS[S]
ACCENT  [S]
ACCTRANS [S]
VERBPAR
PALAT
VACC
VTRANS
STRSHIFT
ACCSHIFT
ONEACC
YOTR
APROPOS
GETSTR [S]
"""


from collections import OrderedDict
from functools import partial
import re
from typing import Callable, List, Optional, Tuple

from .tables import (
    z_a_cases_base,
    z_a_str_base,
)
from .util import (
    deacc, demacc, derform, first, pr_acc, rest, rform, sec_acc, stuff,
    take_int, voc, vocal,
    REMOVE_PARENS,
    SuccessException
)


REMOVE_RIGHTPAREN_SEMICOLON = str.maketrans("", "", ");")
REMOVE_SPACE_COMMA_HYPHEN = str.maketrans("", "", " ,-")


def paradigm_scope(_reflex: bool = False,
                   _dialect: Optional[str] = None):
    dialect: Optional[str] = _dialect
    glob_num: int
    yo: bool
    now_yo: bool
    full_akz: str
    themvoc: str
    endroot: str
    features: str
    akzpl: str
    infinitive: str
    retvalue: str = ""
    retstring: bool = False
    reflex: bool = _reflex
    refl: bool

    def accent(noun: str,
               casus: str,
               n_acc: str,
               a_place: str,
               alter: bool,
               infor: str,
               word: str) -> str:
        """Accentuating a nominal form."""

        nonlocal yo
        result: str = ''

        if '*' in noun:
            a_place = str(take_int(a_place) + (1.1 if '.' in a_place else 1))

        yo = ("," in a_place or " ё " in infor + ' ')
        acctrans = partial(acctrans_, yo=yo)

        if casus.startswith("Cmp"):
            if noun.endswith("ее"):
                if n_acc == "а":
                    result = acctrans(noun, a_place, casus, alter)
                else:
                    result = acctrans(noun, str(len(noun) - 1), casus, alter)
                result = ' // '.join((result,
                                      "по" + result,
                                      result[:-1] + "й",
                                      "по" + result[:-1] + "й"))
            else:
                stress_pos = noun[:-2].rfind("\u0301")
                for u in range(len(noun) - 2, stress_pos, -1):
                    if voc(noun, u):
                        result = acctrans(noun, str(u), casus, alter)
                        return " // ".join((result, "по" + result))
                if stress_pos >= 0:
                    return " // ".join((noun, "по" + noun))

        elif "S" in casus:
            n_acc = rest(n_acc, "/") if "/" in n_acc else n_acc
            if n_acc.startswith("а"):
                result = acctrans(noun, a_place, casus, alter)
                if n_acc.startswith("а'") and casus.startswith("Ssf"):
                    result += "//" + acctrans(noun, str(len(noun)), casus,
                                              alter)
            elif n_acc.startswith("в"):
                u = next((u for u in range(len(noun), 0, -1) if voc(noun, u)),
                         0)
                if u:
                    result = acctrans(noun, str(u), casus, alter)
                if n_acc.startswith("в'") and casus.startswith("Sp"):
                    b = next((b for b in range(u - 1, 0, -1)
                              if voc(result, b)), 0)
                    if b:
                        return "//".join((result, acctrans(noun, str(b),
                                                           casus, alter)))
            elif n_acc.startswith("с"):
                if casus.startswith("Ssf"):
                    return acctrans(noun, str(len(noun)), casus, alter)
                elif casus.startswith("Ssm"):
                    if not word.endswith('ой'):
                        return acctrans(noun, a_place, casus, alter)
                    else:
                        _in = 1
                        if len(noun) > 3:
                            if noun.startswith('не'):
                                _in = 3
                            elif noun.startswith('полу'):
                                _in = 5
                        u = next((u for u in range(_in, len(noun) + 1)
                                  if voc(noun, u)), 0)
                        if u:
                            return acctrans(noun, str(u), casus, alter)
                else:

                    if noun.startswith("не"):  # cases like "нередко"
                        _in = 3
                    else:
                        _in = 1

                    u = next((u for u in range(_in, len(noun) + 1)
                              if voc(noun, u)), 0)
                    if u:
                        result = acctrans(noun, str(u), casus, alter)
                    if n_acc.startswith("с''"):
                        if casus.strip() in ("Ssn", "Sp"):
                            result += "//" + acctrans(noun, str(len(noun)),
                                                      casus, alter)
                    elif n_acc.startswith("с'"):
                        if casus.startswith("Sp"):
                            result += "//" + acctrans(noun, str(len(noun)),
                                                      casus, alter)
        else:
            if "L2" in casus:
                result = acctrans(noun, str(len(noun)), casus, alter)
            elif n_acc.startswith("а"):
                result = acctrans(noun, a_place, casus, alter)
            elif n_acc.startswith("в") or n_acc.startswith("F"):
                if n_acc.startswith("F''"):
                    f_string = "Np Api Is "
                elif n_acc.startswith("F'"):
                    f_string = "Np Api Asa Asi "
                else:
                    f_string = "Np Api "
                if casus.strip() == 'Ip':
                    result = acctrans(noun, str(len(noun) - 2), casus, alter)
                elif casus.strip() in ("Np", "Api") and noun.endswith(("ые",
                                                                       "ие")):
                    result = acctrans(noun, str(len(noun) - 1), casus, alter)
                elif n_acc.startswith("в'") and casus.startswith("Is"):
                    result = acctrans(noun, a_place, casus, alter)
                elif n_acc.startswith("F") and casus.strip() + ' ' in f_string:
                    u = next((u for u in range(1, len(noun) + 1)
                              if voc(noun, u)), 0)
                    if u:
                        return acctrans(noun, str(u), casus, alter)
                else:
                    u = next((u for u in range((len(noun) - 1)
                                               if noun.endswith(('ею', 'ою'))
                                               else len(noun),
                                               0, -1)
                              if voc(noun, u)), 0)
                    if u:
                        result = acctrans(noun, str(u), casus, alter)
                    if n_acc.startswith("F''") and \
                            casus.strip() in ('Gs', 'Ds', 'Ls'):
                        result += "//" + acctrans(noun, a_place, casus, alter)
            elif n_acc.startswith("с") or n_acc.startswith("е"):
                if "s" in casus or "G2" in casus or \
                        (n_acc.startswith("е") and
                         casus.strip() in ("Np", "Api")):
                    result = acctrans(noun, a_place, casus, alter)
                elif casus.startswith("Ip"):
                    result = acctrans(noun, str(len(noun) - 2), casus, alter)
                else:
                    k = next((k for k in range((len(noun) - 1)
                                               if noun.endswith(("ею", "ою"))
                                               else len(noun),
                                               0, -1)
                             if voc(noun, k)), 0)
                    if k:
                        return acctrans(noun, str(k), casus, alter)
            elif n_acc.startswith("D"):
                if "s" in casus and not (n_acc.startswith("D'") and
                                         "As" in casus):
                    k = next((k for k in range((len(noun) - 1)
                                               if noun.endswith(("ею", "ою"))
                                               else len(noun),
                                               0, -1)
                             if voc(noun, k)), 0)
                    if k:
                        return acctrans(noun, str(k), casus, alter)
                elif casus.startswith("Ip"):
                    u = next((u for u in range(len(noun) - 3, 0, -1)
                             if voc(noun, u)), 0)
                    if u:
                        return acctrans(noun, str(u), casus, alter)
                else:
                    v_pos, counter = 0, 0
                    for u in range(len(noun), 0, -1):
                        if voc(noun, u):
                            if casus.strip() in ("Gp", "Apa"):
                                if not (noun.endswith(("ев", "ей", "ов")) or
                                        alter):
                                    return acctrans(noun, str(u), casus, alter)
                            counter += 1
                            if counter == 2:
                                return acctrans(noun, str(u), casus, alter)
                            v_pos = u
                    result = acctrans(noun, str(v_pos), casus, alter)
        return result

    def accshift(string: str, number: int) -> str:
        """Shift accent to the preceding syllable, counting from the NUMBER
        position within a form to the left."""

        for u in range(number, 0, -1):
            if voc(string, u) and string[:u] != 'пере':
                return vtrans(string, u)
        return string

    def acctrans_(form: str,
                  number: str,
                  casus: str,
                  alter: bool,
                  yo: bool) -> str:
        """Marking accented vowels within a nominal form. A subroutine called
        from ACCENT."""

        result: str = form
        yo_pl: int = take_int(rest(number, ",")) if "," in number else 0
        sec_pl: int = take_int(rest(number, ".")) if "." in number else 0
        pr_pl: int = take_int(number)

        if yo_pl > 0:
            if form[yo_pl - 1] == "е":
                result = stuff(form, yo_pl - 1, 1, "ё")
                if full_akz in 'асе' and 's' in casus:
                    return result
            else:
                u = next((u for u in range(yo_pl, 0, -1) if voc(form, u)), 0)
                if u:
                    return stuff(form, u - 1, 1, pr_acc(form[u - 1]))

        if pr_pl <= len(form) and voc(form, pr_pl):
            if form[pr_pl - 1] == 'е' and casus.strip() in ("Gp", "Ssm") and \
                    alter and form[-1] not in 'цй' and len(form) - pr_pl == 1:
                if form[pr_pl - 2] == 'ц':
                    stuff_str = 'о́'
                elif yo:
                    stuff_str = 'ё'
                else:
                    stuff_str = 'а́'
                return stuff(form, pr_pl - 1, 1, stuff_str)
            if form[pr_pl - 1] == 'е' and yo:
                if form[pr_pl + 1] != 'е':
                    if (casus.strip() in ("Ds", "Ls") and pr_pl == len(form)) or \
                            (casus.strip() in ("Gp", "Apa", "Cmp") and
                             form.endswith(("ей", "ее")) and
                             pr_pl == len(form) - 1):
                        st_str = 'а́'
                    else:
                        st_str = 'ё'
                elif casus.startswith('Gp'):
                    st_str = 'ё'
                else:
                    st_str = 'а́'
                result = stuff(result, pr_pl - 1, 1, st_str)
            else:
                result = stuff(result, pr_pl - 1, 1, pr_acc(result[pr_pl - 1]))
                if result.endswith(("а́м", "а́й", "а́ю")) and \
                        casus.startswith('Is'):
                    result = stuff(result, pr_pl - 1, 1, "ё")
                elif result.endswith('а́в') and \
                        casus.strip() in ('Gp', 'Apa') and \
                        result[-4] in 'ьаеиоуыэюя':
                    result = stuff(result, pr_pl - 1, 1, "ё")
                elif result.endswith('а́') and casus.startswith('Ssn'):
                    result = stuff(result, pr_pl - 1, 1, "о́")
        if sec_pl > 0:
            result = stuff(result, sec_pl - 1, 1, sec_acc(result[sec_pl - 1]))
        return result

    def getstr_(string: str, info: str, akzent: str = "",
                out: OrderedDict = None):
        """A complex function. In the most usual case just puts
        a GET-like string on the screen (with STRSAY).
        If, however, the flag RETSTRING is set (i. e. string-
        formatted rather than screen-formatted output was requested),
        the form is added to the static RETVALUE string, with
        additional use of the PLUSTRANS function (a primary accented
        vowel being transcribed as vowel + "+", a secondary accented
        vowel - as vowel + "|". Additionally, all participles in
        a verbal paradigm are being processed and their paradigms
        (normally absent on the screen) are being added to RETVALUE.
        Note that in this case the GETSTR function recursively calls
        NOMPAR again."""

        if not string:
            return ""
        assert isinstance(string, str)

        nonlocal retvalue, reflex

        # Make a local copy of RETVALUE
        locvalue: str = retvalue

        info += " "

        if retstring and "//" in string:
            if getstr_(first(string, "//"), info, akzent, out=out):
                # A cosmetic fix for appending variant forms like мерк//меркнул
                locvalue = retvalue[:-2] + "//"
                info = ""
                string = rest(string, "//")

        if retstring:
            string = string.replace(" ", "")

        if not retstring:
            pass
        elif akzent:
            # build additional paradigm for participles by a recursive call
            # of NOMPAR
            add_cases: str = "Ssm Ssf Ssn Sp  "
            add_ends: str = "0   а   о   ы   "

            def map_end(s: str, *args):
                for i, arg in enumerate(args[::2]):
                    if i == len(args) - 1:
                        return arg
                    if s.endswith(arg):
                        return args[i + 1]

            t_infor, t_akzent, t_cases_ends = map_end(
                string,
                "ённый", ('п 1*а/в"2"', "а/в", None),
                "нный", ('п 1*а"2"', "а", None),
                "мый", ("п 1а", "а", None),
                "тый", (("п 1а/с", "а/с", None) if "/с" in akzent
                        else ("п 1а", "а", None)),
                ("ший", "щий", "щийся", "шийся"), ("п 4а", "а", ("", "")),
                (None, None, None)
            )
            if not t_infor:  # Some non-participle!
                return False
            if t_cases_ends:
                add_cases, add_ends = t_cases_ends

            reflex = string.endswith("ся")

            t_c_chain: str = z_a_cases_base + add_cases
            t_e_chain: str = z_a_str_base + add_ends

            if "ё" in string:
                t_akzpl = ','.join([str(string.index("ё") + 1)] * 2)
                string = string.replace("ё", "е")
            elif "\u0301" in string:
                t_akzpl = str(string.index("\u0301"))
                string = deacc(string)
            else:
                t_akzpl = '0'

            locvalue += \
                info + nompar(string, t_infor, t_c_chain, t_e_chain,
                              t_akzent, t_akzpl, build_par=True)[0] + ";"
        # //  elif string = '*':
        # // Do we need the following line? - S.A.S
        # //  elif string+"; " in locvalue:
        else:
            locvalue += info + string + "; "
            if out:
                out[info] = string
        retvalue = locvalue
        return True

    def nompar(word, infor,
               c_chain: str = '', e_chain: str = '', akzent: str = '',
               akzpl: str = '', form: str = '', build_par: bool = False,
               contmrph: bool = False) -> Tuple[str, str]:
        """Building the noun paradigm. If the parameter BUILD_PAR is passed,
        the output is formatted as a string; otherwise the paradigm is
        displayed on the screen."""

        #local r,p,o,a,u,i,;
        #  inf_part,spec,compform,compstring,shortstr,plurstring,pl_stem,pl_string,;
        #  casvar,casstring,alter,adj,ln,ext_p,endst,stem,bef_end,o_end,c_case,c_end,;
        #  beg_end,end_end,e_len,c_k,okey,words_ch,p_1,p_2,t,stem1,stem2,;
        #  t_akzpl,bef_end1,bef_end2

        rus_name = ['', 'И', 'Р', 'Д', 'В', 'Т', 'П']
        lat_name = ['', 'N', 'G', 'D', 'A', 'I', 'L']
        nonlocal full_akz, reflex, retstring, retvalue

        r_str: str = ""
        retvalue = ""
        if not word.endswith("ся"):
            reflex = False
        if not build_par:
            retstring = False
        else:
            retstring = True
        if "\x04" in infor:
            infor = first(infor, "\x04")
        adj = False
        full_akz = akzent
        akzent = akzent[0]

        pron: bool = (not c_chain)
        kase: list = [None] * 34
        words: list = [None] * 34

        if pron:
            r = 0
            infor = rest(infor) + ' '
            inf_part, spec, alter = ("", "", False)  # Silence a warning
            while infor:
                kase[r] = first(infor)
                words_ch = rest(infor)
                words[r] = first(words_ch)
                infor = rest(words_ch)
                r += 1
        else:
            subst, neutr, anim, pl_hard, m_hard, s_hard, very_hard, compnot, \
                f_hard = [False] * 9
            spec = ''
            compform = ''
            inf_part = infor
            if "(" in infor:
                inf_part = first(infor, "(")
                spec = infor[infor.find("("):]
            if ";" in inf_part:
                inf_part = first(infor, ";")
                spec = infor[infor.find(";"):]
            if "@" in inf_part:
                inf_part = first(infor, "@")
                spec = infor[infor.find("@"):]
            if ":" in inf_part:
                inf_part = first(infor, ":")
                spec = infor[infor.find(":"):]
            inf_part = infor = inf_part.replace(',', '')
            if reflex:
                word = word[:-2]
            if re.search(r'жо |мо |со |одуш\.', inf_part):
                anim = True
                subst = True
                if re.search(r'(^|[^123456789*/ми])со ', inf_part):
                    neutr = True
            elif 'ж ' in inf_part or 'м ' in inf_part or 'неод.' in inf_part:
                if '_кф м ' in inf_part:
                    m_hard = True
                elif '_кф ж ' in inf_part:
                    f_hard = True
                else:
                    subst = True
            elif re.search(r'(^|[^123456789*/ми])с ', inf_part):
                neutr = True
                subst = True

            plur: list = [None] * 7
            c_name: list = [None] * 7
            short: list[Optional[str]] = [None] * 5
            if spec:
                if "_сравн._" in spec:
                    compform = first(rest(spec, "_сравн._").lstrip())
                if "_кф" in spec:
                    shortstr = first(spec[spec.find("_кф") + 4:].lstrip(), ";")
                    if shortstr.startswith('м_'):
                        short[1] = shortstr[2:].lstrip()
                    elif shortstr.startswith('мн._'):
                        short[4] = shortstr[3:].lstrip()
                    else:
                        shortstr += ','
                        p = 1
                        while p <= 4:
                            short[p] = first(shortstr, ",").lstrip()
                            shortstr = rest(shortstr, ",")
                            p += 1
                if "_мн._" in spec:
                    plurstring = rest(spec, "_мн._").lstrip()
                    pl_stem = first(plurstring)
                    pl_string = rest(plurstring).lstrip()
                    plur[1] = (pl_stem + first(pl_string, ",")) \
                        .translate(REMOVE_SPACE_COMMA_HYPHEN)
                    c_name[1] = 'Np'
                    pl_string = rest(pl_string).lstrip()
                    plur[2] = (pl_stem + first(pl_string, ",")) \
                        .translate(REMOVE_SPACE_COMMA_HYPHEN)
                    c_name[2] = 'Gp'
                    pl_string = rest(pl_string).lstrip()
                    plur[3] = (pl_stem + first(first(pl_string, ")"))) \
                        .translate(REMOVE_SPACE_COMMA_HYPHEN)
                    c_name[3] = 'Dp'
                    plur[4] = plur[2] if anim else plur[1]
                    c_name[4] = 'Ap'
                    plur[5] = plur[3][:-1] + 'ми'
                    c_name[5] = 'Ip'
                    plur[6] = plur[3][:-1] + 'х'
                    c_name[6] = 'Lp'

                for i in range(1, 7):
                    casvar = rus_name[i] + ". мн"
                    if casvar in spec:
                        casstring = spec[spec.find(casvar) + 6:].lstrip()
                        if ',' in casstring:
                            casstring = first(casstring, ',')
                        if ';' in casstring:
                            casstring = first(casstring, ';')
                        if '%' in casstring:
                            casstring = first(casstring, '%')
                        plur[i] = casstring.replace('_', '').strip()
                        c_name[i] = lat_name[i] + 'p'
                if plur[2]:
                    if anim:
                        plur[4] = plur[2]
                    else:
                        plur[4] = plur[1]
                    c_name[4] = 'Ap'
            if " мс " in inf_part:
                if "6*" in inf_part:
                    e_chain = ("ый  ья  ье  ьегоьей ьегоьемуьей ьемуый  ьего"
                               "ью  ье  ьим ьей ьею ьим ьем ьей ьем ьи  ьих "
                               "ьим ьи  ьих ьимиьих ")
                elif " 1" in inf_part and "@" not in spec:
                    e_chain = (e_chain
                               .replace("ый", "0 ")
                               .replace("ая", "а ")
                               .replace("ую", "у ")
                               .replace("ое", "о ")
                               .replace("ого", "а  ")
                               .replace("ому", "у  ")
                               .replace("ые", "ы "))
                    if "п " not in inf_part:
                        e_chain = e_chain.replace("ом", "е ")

            if '*' in inf_part and '**' not in inf_part:
                alter = True
            else:
                alter = False

            if 'п ' in inf_part or 'мс' in inf_part:
                if 'п ' in inf_part:
                    adj = True
                ln = 4
                ext_p = 2
                if "-" in inf_part:
                    m_hard = True
                if "!" in inf_part:
                    s_hard = True
                if "?" in inf_part:
                    very_hard = True
                if "~" in inf_part:
                    compnot = True

            elif "8**" in inf_part:
                c_chain = "Ns   Gs   Ds   Asi  Is   Ls   Np   Gp   Dp   Api  Ip   Lp   "
                e_chain = "а    ени  ени  а    енем ени  ена  ен   енам ена  енамиенах "
                ln = 5
                ext_p = 1
            elif "1**" in inf_part:
                c_chain = "Ns  Gs  Ds  Asa Is  Ls  Np  Gp  Dp  Apa Ip  Lp  "
                e_chain = "ин  ина ину ина иномине е   0   ам  0   ами ах  "
                ln = 4
                ext_p = 2
            elif "3**" in inf_part:
                if word.endswith('нок'):
                    c_chain = "Ns   Gs   Ds   Asa  Asi  Is   Ls   Np   Gp   Dp   Apa  Api  Ip   Lp   "
                    e_chain = "енок енка енку енка енок енкоменке ата  ат   атам ат   ата  атамиатах "
                    ln = 5
                    ext_p = 4
                else:
                    c_chain = "Ns     Gs     Ds     Asa    Is     Ls     Np     Gp     Dp     Apa    Ip     Lp     "
                    e_chain = "еночек еночка еночку еночка еночкоменочке атки   аток   аткам  аток   атками атках  "
                    ln = 7
                    ext_p = 6
            else:
                ln = 3
                ext_p = 1

            if 'п ' not in inf_part or 'с п ' in inf_part:
                if "-" in inf_part and 'мо-жо' not in inf_part and \
                        'мс-п' not in inf_part:
                    pl_hard = True

            if ' ' + word[-ext_p:] in ' ый ой ое ая ые' or '1**' in inf_part:
                endst = '0'
                stem = word[:-ext_p]
                bef_end = stem[-1]
            elif ' ' + word[-ext_p:] in \
                    ' ий яя ья ье ее ие ь й енок онок еночек оночек':
                stem = word[:-ext_p]
                bef_end = stem[-1]
                if bef_end in 'кгх':
                    endst = '0'
                elif bef_end in 'чжшщ' and ' ' + word[-ext_p:] in ' ий ее ие' \
                        and ' мс' not in inf_part:
                    endst = '0'
                else:
                    endst = 'ь'
            elif word[-2:] in 'на ва':  # женские фамилии!
                endst = '0'
                stem = word[:-1]
                bef_end = word[-1]
            else:
                endst = '0'
                stem = word
                bef_end = word[-1:]

            r = 0
            p = 1
            o = 0
            o_end = len(c_chain)
            infor_spec = infor + spec
            while o < o_end:
                c_case = c_chain[o:o + ln].lstrip()
                c_end = e_chain[o:o + ln].lstrip()
                o += ln
                kase[r] = c_case
                if "нет_" in infor_spec:
                    if "пф нет_" in infor_spec:
                        if "S" not in c_case:
                            continue
                    if "кф м нет_" in infor_spec:
                        if "Ssm" in c_case:
                            continue
                    if "кф и сравн нет_" in infor_spec:
                        if "S" in c_case or "Cmp" in c_case:
                            continue
                if c_case.startswith('G2') and 'Р2' not in infor_spec:
                    continue
                if c_case.startswith('L2') and \
                        not ('П2' in infor_spec or 'п2' in infor_spec):
                    continue
                if subst:
                    if anim and 'i' in c_case:
                        continue
                    elif not anim and 'a' in c_case:
                        continue
                    if 'S' in c_case or c_case.startswith('Cmp'):
                        continue
                if c_end.startswith('ый'):
                    if word.endswith('ой'):
                        c_end = 'ой'
                if ('п ' in inf_part or 'мс' in inf_part) and subst:
                    if ('м ' in inf_part or 'мо ' in inf_part) and \
                            ('f' in c_case or 'n' in c_case):
                        continue
                    elif ('ж ' in inf_part or 'жо ' in inf_part) and \
                            ('m' in c_case or 'n' in c_case):
                        continue
                    elif neutr and ('f' in c_case or 'm' in c_case):
                        continue
                if c_end:
                    beg_end = c_end[0]
                    end_end = c_end[1:].strip()
                    if not plur[2]:
                        if c_case.startswith(('Gp', 'Apa')) and \
                                c_end.startswith(('0', 'ей')):
                            if re.search('ж |жо |с |со ', inf_part):
                                if word[-1:] in 'аоы' and \
                                        word[-2:][0] not in 'чжшщц' and \
                                        c_end.startswith('ей'):
                                    continue
                                elif akzent in 'аD':
                                    if c_end.startswith('ей'):
                                        if '"2"' not in infor_spec:
                                            continue
                                    elif c_end.startswith('0'):
                                        if '"2"' in infor_spec and \
                                                '["2"' not in infor_spec:
                                            continue
                                else:
                                    if not voc(word, -2):
                                        if word.endswith(("ца", "це", "цо")) \
                                                and c_end.startswith('ей'):
                                            continue
                                        elif word.endswith(("я", "е", "ча",
                                                            "жа", "ша", "ща",
                                                            "чо", "жо", "шо",
                                                            "що")) and \
                                                c_end.startswith('0'):
                                            if not plur[2]:
                                                if not word.endswith('це'):
                                                    continue
                                    elif c_end.startswith('ей'):
                                        continue
                    if end_end.startswith('в') and \
                            (bef_end in 'чшщж' or endst == 'ь'):
                        end_end = ('ев' if ((word[-1:] in 'ий' and
                                             voc(word, -2))
                                            or '"2"' in infor_spec)
                                   else 'ей')
                        words[r] = voctrans(stem, c_end, alter, spec, c_case,
                                            infor, akzent) + end_end
                    else:
                        if "//" in stem:
                            stem2 = rest(stem, "//")
                            bef_end2 = bef_end
                            stem1 = stem[:len(stem2)]
                            bef_end1 = stem1[-1:]
                            words[r] = ''.join((
                                voctrans(stem1, c_end, alter, spec, c_case,
                                         infor, akzent),
                                morphon(endst, beg_end, bef_end1, inf_part,
                                        akzent),
                                end_end,
                                "//",
                                voctrans(stem2, c_end, alter, spec, c_case,
                                         infor, akzent),
                                morphon(endst, beg_end, bef_end2, inf_part,
                                        akzent),
                                end_end,
                            ))
                        else:
                            words[r] = ''.join((
                                voctrans(stem, c_end, alter, spec, c_case,
                                         infor, akzent),
                                morphon(endst, beg_end, bef_end, inf_part,
                                        akzent),
                                end_end,
                            ))
                        if 'p' in c_case and pl_hard:
                            words[r] = '*' + words[r]

                        if 'S' in c_case:

                            if c_case.startswith('Ssf'):
                                if f_hard:
                                    words[r] = '*' + words[r]

                            if c_case.startswith('Ssm'):
                                e_len = (3 if words[r].endswith("ень") else 2)
                                if '"1"]' in infor_spec:
                                    words[r + 1] = words[r][:-e_len]
                                    kase[r + 1] = 'Ssm'
                                    r = r + 1
                                elif '"1"' in infor_spec:
                                    words[r] = words[r][:-e_len]
                                elif '"2"' in infor_spec:
                                    words[r] = "//".join(
                                        f[:-2] for f in words[r].split("//")
                                    )
                                if m_hard or s_hard or reflex:
                                    words[r] = '*' + words[r]
                                elif very_hard:
                                    words[r] = ' -  '
                            else:
                                if '"2"' in infor_spec and \
                                        '"1"' not in infor_spec:
                                    # confusion in forms like заданный!
                                    words[r] = "//".join(
                                        f[:f.rfind("н")] + f[-1:]
                                        for f in words[r].split("//")
                                    )
                                if s_hard or very_hard or reflex:
                                    if "Sp" not in c_case:
                                        # This is just for NBARS...
                                        words[r] = '*' + words[r]

                            if short[p]:
                                words[r] = short[p]
                                p = p + 1

                        if c_case in 'Gp Apa':
                            if plur[2] and c_end.startswith('ей'):
                                continue
                            if words[r].endswith('ьей'):
                                words[r] = words[r][:-3] + 'ей'
                            elif words[r].endswith('ьь'):
                                words[r] = words[r][:-2] + 'ий'

                    if 'п мс ' in inf_part and word.endswith('н'):
                        if c_case in 'Gsm Gsn Asam':
                            words[r] = words[r] + "//" + words[r][:-1] + "ого"
                        if c_case in 'Dsm Dsn':
                            words[r] = words[r] + "//" + words[r][:-1] + "ому"

                    if c_case.startswith('Cmp'):
                        if compform:
                            words[r] = compform
                        elif compnot or reflex:
                            words[r] = ' -  '
                        elif words[r][-3:] in 'кее гее хее':
                            p_1 = words[r][:-3]
                            if words[r][-3:] == 'кее':
                                p_2 = 'че'
                            else:
                                p_2 = ('же' if words[r].endswith('гее')
                                       else 'ше')
                            words[r] = p_1 + p_2

                    if words[r].endswith('ень') and "2*а" in inf_part:
                        if c_case in 'Gp Apa Ssm ':
                            words[r] = words[r][:-1]
                    if reflex:
                        words[r] = words[r] + 'ся'
                    r = r + 1
                    if r > 32:
                        break
            words = words[:r]
            for i in range(1, 7):
                if plur[i]:
                    t = next((t for t, v in enumerate(kase) if v.startswith(c_name[i])), -1)
                    if t >= 0:
                        words[t] = plur[i]
        if build_par:
            if pron:
                for t_word, t_kase in zip(words, kase):
                    getstr_(t_word, t_kase)
            else:
                c_k = False
                for t_word, t_kase in zip(words, kase):
                    okey = (t_word.rfind("\u0301") >= 0)

                    if adj and not ('C' in t_kase or 'S' in t_kase):
                        if "//" in t_word and \
                                t_kase in "Gsm Gsn Dsm Dsn Asam Ip  " and \
                                "п мс " not in inf_part and \
                                "п 4а" not in inf_part:
                            t_akzpl = (",".join([str(take_int(akzpl) + 1)] * 2)
                                       if "," in akzpl
                                       else str(take_int(akzpl) + 1))
                        else:
                            t_akzpl = akzpl
                        t_word = demacc(t_word, t_akzpl)

                    elif 'Ns' in t_kase or ('Asi' in t_kase and
                                            'ж ' not in infor + spec):
                        t_word = demacc(t_word, akzpl)
                    else:
                        if "//" in t_word:
                            okey = False
                        if not okey:
                            t_word = accent(t_word, t_kase, full_akz,
                                            akzpl, alter, infor, word)
                        elif 'Cmp' in t_kase:
                            t_word = t_word + " // по" + t_word
                    if 'Ss' not in t_kase:
                        if 'p' in t_kase and not c_k:
                            c_k = True
                    getstr_(t_word, t_kase)
        else:
            if not retstring:
                for t_word, t_kase in zip(words, kase):
                    t_word = deacc(t_word)
                    if form + ' ' in t_word + ' ' or form + '/' in t_word:
                        if contmrph:
                            raise SuccessException
                        r_str += ',' + t_kase
        return retvalue, r_str

    def palat(string: str) -> str:
        """Result of a phoneme's morphonological palatalisation."""

        if string.startswith('т') and endroot == 'щ':
            return 'щ'
        else:
            string1: str = 'б п в ф м з т с д ст г к х ск'
            string2: str = 'блплвлфлмлж ч ш ж щ  ж ч ш щ '
            if string in string1:
                return string2[string1.find(string):][:2].rstrip()
        return string

    def strshift(string: str) -> str:
        """Shifts accentuation in a verb form, depending on its place in the
        infinitive."""

        number: int = take_int(akzpl)
        for u in range(number + 1, len(infinitive) + 1):
            if voc(infinitive, u):
                return vtrans(string, number)
        for u in range(number - 1, 0, -1):
            if voc(string, u):
                return vtrans(string, u)
        return vtrans(string, number)

    def vacc(stem: str, ending: str, person: int):
        """Accentuating a verbal form."""

        '''"person" parameters:
        1 - 1st p. p./f.
        0 - other persons p./f.
        2 - infinitive
        3 - part. pres. act./pass.
        4 - imperative
        5 - past
        6 - part. perf. pass.
        '''
        nonlocal full_akz, yo

        if person == 5 or person == 6 or "," in akzpl:
            yo = True  # FIXME: outer scope
        f_form: str = stem + ending
        if "ё" in f_form or "я́" in f_form or "ы́" in f_form:
            return f_form
        a_place: int = take_int(akzpl)
        if person == 2:
            return vtrans(stem, a_place)
        full_akz = ("в" if full_akz.startswith("с'") else full_akz)
        pres_akz: str = full_akz[0]
        past_akz: str = (full_akz[2:] if full_akz[1] == "/" else "а")
        akz: str = (past_akz if person == 5 else pres_akz)

        if akz.startswith('а'):
            ex_len: int = 0
            if person == 3:
                ex_add: int = (0 if voc(stem, len(stem) - 1) else 1)
                if ending == 'щий':
                    ex_len = 1 + ex_add
                elif ending == 'ый':
                    ex_len = 2 + ex_add
            for u in range(len(stem) - ex_len, 0, -1):
                if u <= a_place and voc(stem, u):
                    return vtrans(f_form, u)
        elif akz.startswith('в'):
            for u in range((len(f_form) - 1) if ending.startswith('ши')
                           else len(f_form), 0, -1):
                if voc(f_form, u) and \
                        f_form[u - 1:] in ('е', 'ий', 'ый', 'и)'):
                    return vtrans(f_form, u)
        elif akz == "с'" and person == 5:
            if f_form.endswith('а'):
                return vtrans(f_form, len(f_form))
            elif f_form.endswith('о'):
                return vtrans(vtrans(f_form, len(f_form)), len(f_form) - 2)
            elif voc(f_form, -1):
                return vtrans(f_form, len(f_form) - 2)
            elif f_form.endswith('тый'):
                return vtrans(f_form, len(f_form) - 3)
            elif f_form.endswith('вший'):
                return vtrans(f_form, len(f_form) - 4)
            elif f_form.endswith('в(ши)'):
                return vtrans(f_form, len(f_form) - 5)
            else:
                return vtrans(f_form, len(f_form) - 1)
        elif akz.startswith('с'):
            if person == 1:
                return vtrans(f_form, len(f_form))
            elif person == 4:
                if f_form.endswith('е'):
                    return vtrans(f_form, len(f_form) - 2)
                else:
                    return vtrans(f_form, len(f_form))
            elif person == 5:
                if f_form.endswith('а'):
                    return vtrans(f_form, len(f_form))
                elif f_form.endswith('ший'):
                    return vtrans(f_form, len(f_form) - 4)
                elif f_form.endswith('(ши)'):
                    return vtrans(f_form, len(f_form) - 5)
                else:
                    if voc(f_form, -1):
                        e_len = 2
                    elif f_form.endswith('тый'):
                        e_len = 3
                    else:
                        e_len = 1
                    if '"1"' not in features:
                        return vtrans(f_form, len(f_form) - e_len)
                    elif '["1' in features:
                        return "//".join((vtrans(f_form, len(f_form) - e_len),
                                          accshift(f_form,
                                                   len(f_form) - e_len - 2)))
                    elif '"1"' in features:
                        return accshift(f_form, len(f_form) - e_len - 2)
            else:
                if ending.startswith(('щий', 'ый')):
                    if themvoc == 'и':
                        return vtrans(f_form, len(f_form) - 3)
                for u in range((len(stem) - 1) if person == 3 else len(stem),
                               0, -1):
                    if voc(stem, u):
                        return vtrans(f_form, u)
        return f_form

    def vtrans(string: str, number: int) -> str:
        """Marking a vowel of a verbal form as accented. A subroutine called
        from VACC."""

        nonlocal glob_num, yo, now_yo

        if string.endswith('ла'):
            glob_num = number
        if yo and now_yo and string[number] == 'е':
            yo = False  # FIXME: outer scope
            return stuff(string, number - 1, 1, "ё")
        return stuff(string, number - 1, 1, pr_acc(string[number - 1]))

    def verbpar(_infinitive, _features, _akzpl, build_par=False):
        """Building the verbal paradigm. As in the case of NOMPAR, if the
        BUILD_PAR parameter is passed, output is formatted in a string."""

        # LOCAL stem1, stem3, impsing2, impplur2, ;
        #    ex_stem, ;
        #   example, irreg, aspect, obj_stat, impers, t_info, t_type, ;
        #   akzent, past_string, mpast, fpast_str, fpast, ;
        #   mpaststem, fpaststem, pres_char, p_len,  specific, ;
        #   test_pers, p_sing, p_plur, p_numb, i, k, stem, stem_cons, base, ;
        #   base_cons, prefix, base_voc, add_cons, gerstring, impstem, stemend,
        #   tempast, p_str, match, asp_string, extr_len, asp_base, ch_base,
        #   accinf, matchstr, ext_len, substit, a_s, d_acc, the_form, ;
        #    o_refl, tc1, tc2, p_impplur, p_impsing, full_v_akz, ;
        # LOCAL imms, pams, prms

        nonlocal akzpl, endroot, features, full_akz, glob_num, infinitive, \
            refl, retstring, retvalue, themvoc, yo, now_yo

        infinitive = _infinitive
        features = _features
        akzpl = _akzpl

        retvalue = ""
        retstring = build_par
        if "\x04" in features:
            features = first(features, "\x04")
        if "#16" in features and "=>" in features:
            # A temporary patch: all these cases should rather be
            # processed in the dictionary files themselves - S.A.S
            features = features.replace("#16", "")
        p_pstpr: str = ''
        now_yo = (" ё" in features or "," in akzpl)
        yo = False  # FIXME: outer scope!
        glob_num = 0

        o_refl = refl  # the flag may change, so save it to restore later
        refl = infinitive.endswith(('сь', 'ся'))
        example = vacc(infinitive, '', 2)
        infinitive = derform(infinitive)
        accinf = derform(example)

        irreg = False
        if 'нсв' in features:
            aspect = 'imperf.'
            if 'св-нсв' in features:
                aspect = 'imperf./perf.'
        else:
            aspect = 'perf.'

        half_trans: bool = False
        if (refl or 'нп' in features) and not ("_имеется страд" in features or
                                               "_имеется прич." in features):
            obj_stat = 'intrans.'
        else:
            obj_stat = 'trans.'
            if "_имеется прич. страд." in features and \
                    'imperf.' not in aspect and 'нп' in features:
                half_trans = True

        type_pl = ((features.find("нп") + 3) if 'нп' in features
                   else ((features.find("нсв") + 4) if "нсв" in features
                         else features.find("cв") + 3))
        if "св," in features or "нп," in features:
            type_pl += 1
        impers = ('безл.' in features)
        t_info = features[type_pl:]
        t_type = str(take_int(t_info))

        # Note: we only process Zalizniak's circles (**); Zalizniak's asterisks
        # (in обмереть, разобрать etc.) are apparently redundant, because
        # the type of alternation (об / обо) etc. is predicted by the type
        # of conjugation itself (the cases are limited to verbs like -гнать,
        # -переть etc. forming special conjugation types). The only exception
        # known to me is простереть - which is being processed as an individual
        # case, see below.

        if t_type in ('6', '3'):
            if "**" in t_info:
                t_type += '**'
                if infinitive.endswith("стичь"):
                    infinitive = infinitive.replace("стичь", "стигнуть")
                    example = vacc(infinitive, '', 2)
                    accinf = derform(example)
        t_type += ' '
        if t_type in '4 5 ' and "#16" not in features:
            themvoc = 'и'
        else:
            themvoc = 'е'
        full_akz = features[type_pl +
                            len(t_type.strip()):].replace('*', '').lstrip()
        if full_akz[1:3] == '//':
            full_akz = full_akz[0]
        if full_akz[1:] == '/':
            if "''" in full_akz:
                full_akz = full_akz[:5]
            elif "'" in full_akz:
                full_akz = full_akz[:4]
            else:
                full_akz = full_akz[:3]
        else:
            full_akz = (full_akz[:2] if full_akz[1] == "'" else full_akz[0])
        full_v_akz = full_akz
        akzent = full_akz[0]
        if "(-" in t_info and "-)" in t_info:
            endroot = t_info[t_info.find("(-") + 2:t_info.index("-)")]
        elif "-)" in t_info:
            endroot = t_info[t_info.index("(") + 1:t_info.index("-)")]
        else:
            endroot = ''
        if '_прош._' in features:
            past_string = rest(features, '_прош._').lstrip()
            mpast = derform(first(past_string, ',').strip())
            fpast_str = rest(past_string, ",")
            fpast = first(fpast_str, ";").strip()
            fpast = derform(first(fpast, ",").strip())

            if " " in mpast and "-" in fpast:
                mpast = mpast.replace("-", "")
                fpast = first(mpast, " ") + fpast[1:]
                mpast = mpast.strip()

            mpaststem = deacc((mpast[:-1] if mpast.endswith('л') else mpast)
                              .replace(' ', ''))
            fpaststem = deacc(mpaststem if fpast.startswith('-')
                              else fpast[:-2])
            if mpaststem.endswith('ше'):
                # // if len(fpaststem) > 1: fpaststem = fpaststem[:-1]
                mpaststem = ((mpaststem[:-1] + 'ё') if 'вы' not in mpaststem
                             else mpaststem)

        else:  # not ('_прош._' in features)
            if t_type == '8 ':
                mpaststem = fpaststem = infinitive[:-2] + endroot
            elif t_type == '7 ':
                if infinitive[-3] == 'з':
                    mpaststem = fpaststem = infinitive[:-2]
                elif endroot in 'бс':
                    mpaststem = fpaststem = infinitive[:-3] + endroot
                else:
                    mpaststem = fpaststem = infinitive[:-3]
            elif t_type == '3** ':
                mpaststem = fpaststem = infinitive[:-4]
            elif t_type == '9 ':
                mpaststem = fpaststem = infinitive[:-3]
            else:
                mpaststem = fpaststem = infinitive[:-2]

        pers_plur: List[str] = [""] * 3
        pers_sing: List[str] = [""] * 3
        if '_наст._' in features:  # and '_наст._ _' not in features:
            pres_char = '_наст._'
        elif '_буд._' in features:  # and '_буд._ _' not in features:
            pres_char = '_буд._'
        else:
            pres_char = 'None'
        if pres_char in features:
            pres_str = rest(features, pres_char).lstrip()
            if ';' in pres_str:
                pres_par = first(pres_str, ';')
            elif ' $ ' in pres_str:
                pres_par = first(pres_str, '$')
            else:
                pres_par = pres_str
            specific = False
            p_numb: int = 1
            for i, test_pers in enumerate(pres_par.split(","), start=1):
                if "ед._" in test_pers:
                    specific = True
                    p_sing = take_int(rest(test_pers, "_"))
                    pers_sing[p_sing] = \
                        derform(rest(test_pers, "ед._").lstrip())
                elif "мн._" in test_pers:
                    specific = True
                    p_plur = take_int(rest(test_pers, "_"))
                    pers_plur[p_plur] = \
                        derform(rest(test_pers, "мн._").lstrip())
                else:
                    if i <= 3:
                        pers_sing[i] = derform(test_pers.strip())
                    else:
                        pers_plur[i - 3] = derform(test_pers.strip())
                p_numb = i + 1
            if ' ' in pers_sing[1]:
                stem = first(pers_sing[1])
                ex_stem = stem
                for i in range(1, 4):
                    if i == 1:
                        pers_sing[i] = pers_sing[i].replace(' ', '')
                    else:
                        pers_sing[i] = oneacc(stem + pers_sing[i][1:])
                    pers_plur[i] = oneacc(stem + pers_plur[i][1:])
            if impers:
                pers_sing[3] = pers_sing[1]
                stem1, stem3 = "", ""
            else:
                if not specific and p_numb < 6:
                    stem1 = pers_sing[1][:-1]
                    stem3 = (stem1 if "-" in pers_sing[2]
                             else pers_sing[2][:-2])
                    pers_sing = [''] * 3
                    pers_plur = [''] * 3
                    irreg = True
        if "буд. нет" in features:
            pers_sing = ['-'] * 3
            pers_plur = ['-'] * 3
            impsing, impplur, prespart, presger = ['-'] * 4
        elif "1 ед. нет" in features:
            pers_sing[1] = '-'

        if not irreg:
            if t_type == '0 ':
                pass
            elif t_type == '1 ':
                stem1 = stem3 = mpaststem
            elif t_type == '2 ':
                stem = infinitive[:-4]
                if stem.endswith('о'):
                    stem1 = stem3 = stem[:-1] + "у"
                else:
                    if stem[-2] in "чжшщц":
                        stem1 = stem3 = stem[:-1] + "у"
                    else:
                        stem1 = stem3 = stem[:-1] + "ю"
            elif t_type in '3 3** 6** 10 ':
                stem1 = stem3 = infinitive[:-3]
            elif t_type in ('4 ', '5 '):
                stem3 = infinitive[:-3]
                if stem3.endswith(('ст', 'ск')):
                    stem_cons = stem3[-2:]
                    base = stem3[:-2]
                else:
                    stem_cons = stem3[-1:]
                    base = stem3[:-1]
                stem1 = base + palat(stem_cons)
            elif t_type == '6 ':
                stem = infinitive[:-3]
                if stem.endswith(('ст', 'ск')):
                    stem_cons = stem[-2:]
                    base = stem[:-2]
                else:
                    stem_cons = stem[-1:]
                    base = stem[:-1]
                stem1 = stem3 = base + palat(stem_cons)
            elif t_type == '7 ':
                if infinitive[-3] == 'з':
                    stem1 = stem3 = mpaststem
                else:
                    base = infinitive[:-3]
                    stem1 = stem3 = base + endroot
            elif t_type == '8 ':
                base = infinitive[:-2]
                if dialect == 'aos' and base.endswith(('к', 'г')):
                    endroot = base[-1]
                    base = base[:-1]
                stem1 = base + endroot
                stem3 = base + palat(endroot)
            elif t_type == '9 ':
                base_cons = infinitive[-6]
                prefix = infinitive[:-6]
                if "простереть" not in infinitive:
                    stem1 = stem3 = vocal(prefix) + base_cons + 'р'
                else:
                    stem1 = stem3 = prefix + base_cons + 'р'
            elif t_type == '11 ':
                base_cons = infinitive[-4]
                prefix = infinitive[:-4]
                stem1 = stem3 = vocal(prefix) + base_cons + 'ь'
            elif t_type == '12 ':
                base_voc = infinitive[-3]
                stem = infinitive[:-2]
                if base_voc == 'ы':
                    stem1 = stem3 = stem[:-1] + 'о'
                elif stem.endswith('пе'):
                    stem1 = stem3 = stem[:-1] + 'о'
                elif stem.endswith('бри'):
                    stem1 = stem3 = stem[:-1] + 'е'
                else:
                    stem1 = stem3 = stem
            elif t_type == '13 ':
                stem1 = stem3 = infinitive[:-4]
            elif t_type == '14 ':
                if endroot in 'нм':
                    prefix = infinitive[:-4]
                    base_cons = infinitive[-4]
                    stem1 = stem3 = vocal(prefix) + base_cons + endroot
                elif endroot.startswith('им'):
                    stem1 = stem3 = infinitive[:-3] + endroot
                else:
                    stem1 = stem3 = endroot
            elif t_type in '15 16 ':
                add_cons = ('н' if t_type == '15 ' else 'в')
                stem1 = stem3 = infinitive[:-2] + add_cons

        s_pers: List[str] = [""] * 4
        p_pers: List[str] = [""] * 4

        if pers_sing[1]:
            s_pers[1] = pers_sing[1]
        else:
            s_pers[1] = (
                vacc(stem1, ("у" if stem1[-1:] in "чжшщ" else "ю"), 1)
                if ' ' + t_type in ' 1 2 4 5 6 10 11 12 13 '
                else vacc(stem1, ("ю" if stem1.endswith('л') else "у"), 1)
            )

        _s_pers_2 = pers_sing[2] or vacc(stem3, themvoc + 'шь', 0)
        s_pers[2] = (yotr(_s_pers_2)
                     if "а́м" not in _s_pers_2 and 'а́шь ' in _s_pers_2 + ' '
                     else _s_pers_2)

        _s_pers_3 = pers_sing[3] or vacc(stem3, themvoc + 'т', 0)
        s_pers[3] = (yotr(_s_pers_3) if 'а́т ' in _s_pers_3 + ' '
                     else _s_pers_3)

        _p_pers_1 = pers_plur[1] or vacc(stem3, themvoc + 'м', 0)
        p_pers[1] = (yotr(_p_pers_1) if 'а́м ' in _p_pers_1 + ' '
                     else _p_pers_1)

        _p_pers_2 = pers_plur[2] or vacc(stem3, themvoc + 'те', 0)
        p_pers[2] = (yotr(_p_pers_2) if 'а́те ' in _p_pers_2 + ' '
                     else _p_pers_2)

        if pers_plur[3]:
            _p_pers_3 = pers_plur[3]
        else:
            if themvoc == 'и':
                _p_pers_3 = vacc(stem3, ("ат" if stem3[-1:] in "чшжщ"
                                         else "ят"), 0)
            else:
                _p_pers_3 = vacc(stem1, ('ют' if (t_type == '10 ' or voc(stem1, -1)
                                                  or stem1[-1:] in 'ль')
                                         else 'ут'), 0)
        p_pers[3] = _p_pers_3

        if aspect.startswith('imperf.') and obj_stat.startswith('trans.'):
            if (t_type in '1 2 12 ' or (t_type == '6 ' and
                                        infinitive.endswith('ять'))) \
                    and akzent == 'а':
                p_prespart = vacc(deacc(_p_pers_1), 'ый', 3)
            elif t_type == '13 ':
                p_prespart = accinf[:-2] + 'емый'
            elif t_type in '4 5 6 7 ':
                if themvoc == 'е' and akzent == 'в':
                    p_prespart = '*' + stem3 + 'о́мый'
                elif themvoc == 'е':
                    p_prespart = '*' + _p_pers_1 + 'ый'
                else:
                    p_prespart = '*' + vacc(deacc(_p_pers_1), 'ый', 3)
            elif stem1 == 'влек':
                p_prespart = stem1 + 'о́мый'

        if not prespart:

            # By moving the commented clause we allow formation of
            # present/future perfective gerunds - widely used (especially
            # in classic literature) and basically synonymous with past
            # perfective gerunds - however, prohibited by Zalizniak's rules.
            # To stress the difference, I am keeping the original code
            # commented - S.A.S

            if "_деепр. нет_" not in features:
                if t_type in '1 2 4 5 6 6** 7 10 12 16 ':
                    if not (t_type in '1 2 12 ' and aspect == 'perf.'):
                        presger = vacc(deacc(stem3),
                                       ('а' if stem3[-1:] in 'чжшщ' else 'я'),
                                       1) if stem3 else ''
                elif t_type == '13 ' and aspect == 'imperf.':
                    presger = accinf[:-2] + 'я'
                if "деепр. затрудн." in features:
                    presger = '*' + presger
                elif "_деепр._" in features:
                    gerstring = rest(features, "_деепр._").lstrip()
                    if ';' in gerstring:
                        presger = derform(first(gerstring, ';'))
                    elif ',' in gerstring:
                        presger = derform(first(gerstring, ','))
                    elif ' ' in gerstring:
                        presger = derform(first(gerstring))
                    else:
                        presger = gerstring

            if aspect == 'imperf.':
                prespart = vacc(deacc(_p_pers_3[:-1]), 'щий', 3)
                if '["4"' in features:
                    prespart = accshift(prespart, len(prespart) - 4)
                elif '"4"' in features:
                    prespart = accshift(deacc(prespart), len(prespart) - 4)
                if infinitive == 'быть':
                    prespart = 'бу́дущий'
            '''if "_деепр. нет_" not in features
                    if t_type in '1 2 4 5 6 6** 7 10 12 16 ':
                        presger = vacc(deacc(stem3),
                                       ('а' if stem3[-1:] in 'чжшщ' else 'я'),
                                       1) if stem3 else ''
                    elif t_type = '13 ':
                        presger = accinf[:-2] + 'я'
                    if "деепр. затрудн." in features:
                        presger = '*' + presger
                    elif "_деепр._" in features:
                        gerstring = rest(features, "_деепр._").lstrip()
                        if ';' in gerstring:
                            presger = derform(first(gerstring, ';'))
                        elif ',' in gerstring:
                            presger = derform(first(gerstring, ','))
                        elif ' ' in gerstring:
                            presger = derform(first(gerstring))
                        else:
                            presger = gerstring
            '''

        if 'повел. нет' not in features:
            impstem = deacc(_p_pers_3[:-2])
            if '_повел._' in features:
                impsing = rest(features, '_повел._').lstrip() + ' '
                if ';' in impsing:
                    impsing = derform(first(impsing, ';'))
                if ',' in impsing:
                    impsing = derform(first(impsing, ','))
                if ' ' in impsing:
                    impsing = derform(first(impsing))
                if impsing.startswith('-'):
                    if ex_stem:
                        impsing = ex_stem + impsing[1:]
                    else:
                        impsing = s_pers[1][:-1] + impsing[1:]
                impplur = impsing + 'те'
            if not impsing:
                stemend = impstem[-1:]
                if '["2"' in features:
                    impsing = vacc(impstem, 'и', 4)
                    impsing2 = vacc(impstem, ('й' if voc(stemend, 1) else 'ь'),
                                    4)
                    impplur = vacc(impsing, 'те', 4)
                    impplur2 = vacc(impsing2, 'те', 4)
                elif '"2"' in features:
                    impsing = vacc(impstem, ('й' if voc(stemend, 1) else 'ь'),
                                   4)
                    impplur = vacc(impsing, 'те', 4)
                elif '"3"]' in features:
                    impsing = vacc(impstem, 'и', 4)
                    impsing2 = vacc(impstem, 'ь', 4)
                    impplur = vacc(impstem, 'ьте', 4)
                elif '"3"' in features:
                    impsing = vacc(impstem, 'и', 4)
                    impplur = vacc(impstem, 'ьте', 4)
                else:
                    if voc(stemend, 1):
                        if t_type == '4 ' and "#16" not in features and \
                                (akzent in 'вс' or impstem.startswith('вы')):
                            impsing = vacc(impstem, 'и', 4)
                        elif t_type == '13 ':
                            impsing = accinf[:-2] + 'й'
                        else:
                            impsing = vacc(impstem, 'й', 4)
                    else:
                        if stemend == 'ь':
                            impsing = vacc(infinitive[:-3], 'ей', 4)
                        elif akzent in 'вс':
                            impsing = vacc(impstem, 'и', 4)
                        else:
                            if (not voc(impstem, -1) and
                                not voc(impstem, -2)) or \
                                    impstem[-1] == 'щ' or \
                                    impstem.startswith('вы'):
                                impsing = vacc(impstem, 'и', 4)
                            else:
                                impsing = vacc(impstem, 'ь', 4)
        else:
            impsing, impplur = [' - '] * 2

        if "_прош. нет" not in features:
            pastf = ["",
                     vacc(mpaststem,
                          ('л' if (voc(mpaststem, -1) or
                                   mpaststem[-1] == "\u0301") else ''), 5),
                     vacc(fpaststem, 'ла', 5),
                     vacc(fpaststem, 'ло', 5),
                     vacc(fpaststem, 'ли', 5)]
            if '["5"' in features:
                pastf1 = vacc(mpaststem, 'нул', 5)
            elif '"5"' in features:
                pastf[1] = vacc(mpaststem, 'нул', 5)
            if "прич. прош._" in features:
                past_str = rest(features, "прич. прош._").lstrip() + ' '
                pastpart = derform(first(past_str)
                                   .translate(REMOVE_RIGHTPAREN_SEMICOLON)
                                   .strip())
                if "деепр. прош._" not in features:
                    pastger = pastpart[:-1]
                else:
                    pastger = rest(past_str, "деепр. прош._").lstrip()
                    pastger = derform(first(pastger, " "))
            else:
                if t_type == '7 ' and infinitive.endswith('сти') and \
                        endroot in 'тд':
                    pastpart = vacc(stem1, 'ший', 5)
                    pastger = vacc(stem1, 'ши', 5)
                elif mpaststem.endswith('шё') or mpaststem == 'выше':
                    tempast = mpaststem[:-1] + 'е'
                    pastpart = vacc(tempast, 'дший', 5)
                    pastger = pastpart[:-1]
                else:
                    pastpart = vacc(mpaststem, ('вший' if voc(mpaststem, -1)
                                                else 'ший'), 5)
                    pastger = vacc(mpaststem, ('в(ши)' if voc(mpaststem, -1)
                                               else 'ши'), 5)
                    if pastger.endswith('в(ши)'):
                        pastger = pastger.translate(REMOVE_PARENS)
                        pastger2 = vacc(mpaststem, 'в', 5)
                    if t_type == '9 ':
                        pastger2 = mpaststem + 'а́в(ши)'

            if '"6"]' in features:
                pastpar1 = vacc(mpaststem, 'нувший', 5)
                pastger2 = vacc(mpaststem, 'нув(ши)', 5)
            elif '"6"' in features:
                pastpart = vacc(mpaststem, 'нувший', 5)
                pastger = vacc(mpaststem, 'нув(ши)', 5)
            if '"9"' in features:
                pastger = vacc(stem3, ('а' if stem3[-1] in 'чжшщ' else 'я'), 1)
            if "_прич. прош. нет" in features:
                pastpart = ''

        else:
            pastf = [""] * 5
            pastpart = pastger = pastpar1 = pastger2 = ""

        if obj_stat.startswith('trans.'):
            if aspect.startswith('perf.') or " $ " not in features or \
                    'св-нсв' in features:
                if "?" not in features:
                    if "_прич. страд._" in features:
                        if "_прич. страд._ -жд-" in features:
                            p_pstpr = stem1 + 'денный'
                            if t_type in '7 8 ':
                                p_pstpr = accshift(p_pstpr, glob_num)
                            elif t_type == '4 ':
                                p_pstpr = vacc(stem1, 'денный', 6)
                            elif t_type == '5 ':
                                p_pstpr = strshift(p_pstpr)
                        else:
                            p_str = rest(features, "_прич. страд._").lstrip()
                            if ";" in p_str:
                                p_str = first(p_str, ";")
                            if ',' in p_str:
                                p_str = first(p_str, ',')
                            if " " in p_str:
                                p_pstpr = first(p_str)
                            else:
                                p_pstpr = p_str
                    elif ' '+t_type in ' 9 11 12 14 15 16 ':
                        p_pstpr = vacc(mpaststem, 'тый', 5)
                    elif t_type in '3 3** 10 ':
                        p_pstpr = strshift(infinitive[:-1] + 'ый')
                    elif t_type in '7 8 ':
                        p_pstpr = accshift(stem3 + 'енный', glob_num)
                    elif infinitive.endswith(('ать', 'ять')):
                        if infinitive.endswith('цевать'):
                            p_pstpr = infinitive[:-6] + 'цо́ванный'
                        else:
                            p_pstpr = strshift(infinitive[:-2] + 'нный')
                    elif infinitive.endswith('еть') and t_type == '1 ':
                        p_pstpr = infinitive[:-3] + 'ённый'
                    else:
                        if t_type == '4 ':
                            if "#16" in features:
                                p_pstpr = strshift(stem1[:-1] + 'енный')
                            else:
                                p_pstpr = vacc(stem1, 'енный', 6)
                        else:
                            p_pstpr = strshift(stem1 + 'енный')

                    if "!" in features:
                        p_pstpr = '*' + p_pstpr
                    if '["7"' in features:
                        p_pstpr += "//" + vtrans(deacc(p_pstpr),
                                                 len(p_pstpr) - 4)
                    elif '"7"' in features:
                        p_pstpr = vtrans(deacc(p_pstpr), len(p_pstpr) - 4)
                    if '["8"' in features:
                        p_pstpr = \
                            accshift(deacc(p_pstpr), len(p_pstpr) - 5) + \
                            "//" + p_pstpr
                    elif '"8"' in features:
                        p_pstpr = accshift(deacc(p_pstpr), len(p_pstpr) - 5)
        p_pstpr = (yotr(p_pstpr) if 'а́нный ' in p_pstpr + ' ' else p_pstpr)

        if not impplur:
            impplur = impsing + 'те'

        yo = True  # FIXME: outer scope
        match = ''
        irrmatch: bool = False

        if "$" in features:
            asp_string = rest(features, "$").lstrip().replace("\r", '') \
                .replace("_см._", '')
            if aspect.startswith(('perf.', 'imperf./perf.')):
                if 'I' not in asp_string:
                    match = derform(asp_string.rstrip())
                elif "(" in asp_string and "(-" not in asp_string:
                    match = derform(rest(asp_string, "(")
                                    .replace(")", '').strip())
                else:
                    if 'III' in asp_string:
                        extr_len = 4 if infinitive.endswith('нуть') else 2
                        match = deacc(infinitive[:-extr_len]) + 'ва́ть'
                    elif 'II' in asp_string:
                        if infinitive.endswith('нуть'):
                            match = deacc(infinitive[:-4]) + 'а́ть'
                        elif infinitive.endswith('ать'):
                            match = vtrans(infinitive, len(infinitive) - 2)
                        else:
                            match = deacc(s_pers[1][:-1]) + \
                                ('я́ть' if s_pers[1][-1:] in 'юю́' else 'а́ть')
                    elif 'I' in asp_string:
                        if infinitive.endswith('ять'):
                            match = infinitive[:-3] + 'ивать'
                        elif infinitive.endswith('ьнуть'):
                            match = infinitive[:-5] + 'ивать'
                        elif infinitive.endswith('нуть'):
                            asp_base = infinitive[:-4]
                            match = asp_base + \
                                ('ивать' if asp_base[-1] in 'кгхчшжщ'
                                 else 'ывать')
                        elif infinitive.endswith('цевать'):
                            match = infinitive[:-5] + 'овывать'
                        elif infinitive.endswith(('ать', 'оть')):
                            asp_base = infinitive[:-3]
                            match = asp_base + \
                                ('ивать' if asp_base[-1:] in 'кгхчшжщ'
                                 else 'ывать')
                        else:
                            if "#16" in features:
                                extr_len = 2
                            else:
                                extr_len = 1
                            match = s_pers[1][:-extr_len] + \
                                ('ивать' if (s_pers[1][-1] in 'юю́' or
                                             stem1[-1] in 'кгхчшжщ')
                                 else 'ывать')
                        if "(-а́-)" in asp_string:
                            ch_base = deacc(match[:-5])
                            ch_end = match[-4:]
                            k = next((k for k in range(1, len(ch_base) + 1)
                                      if ch_base[-k] in 'оо́'), 0)
                            if k:
                                ch_base = stuff(ch_base, -k, 1, 'а́')
                            match = ch_base + ch_end
                        elif accinf.startswith('вы́') or \
                                accinf[-2] == "\u0301":
                            match = accshift(deacc(match), len(match) - 5)
                            if "а́" in match and \
                                    (" ё" in features or
                                     "(-ё-)" in asp_string):
                                match = stuff(match, match.index("а́"),
                                              len("а́"), "ё")
                        else:
                            match = vtrans(match, take_int(akzpl))
            elif aspect.startswith('imperf.'):
                if ' ' + asp_string[:2] + ' ' in ' 11 12 13 15 16 ':
                    matchstr = infinitive[:-4]
                    match = vtrans(matchstr, len(matchstr)) + 'ть'
                elif asp_string.startswith('14'):
                    asp_base = infinitive[:-5]
                    match = asp_base + ('а́ть' if asp_base[-1] in 'чжшщ'
                                        else 'я́ть')
                elif asp_string.startswith('10'):
                    asp_base = infinitive[:-7]
                    match = asp_base + ('оло́ть'
                                        if infinitive.endswith('алывать')
                                        else 'оро́ть')
                elif asp_string.startswith('9'):
                    match = infinitive[:-5] + 'ера́ть'
                elif asp_string.startswith('8'):
                    ext_len = (6 if infinitive.endswith('ивать') else 4)
                    matchstr = infinitive[:-ext_len]
                    match = vtrans(matchstr + 'чь',
                                   (take_int(akzpl) if ext_len == 6
                                    else (take_int(akzpl) - 2)))
                elif asp_string.startswith('7'):
                    ext_len = (5 if infinitive.endswith('ывать') else 3)
                    asp_base = infinitive[:-ext_len]
                    matchstr = (asp_base if asp_base.endswith('з')
                                else (asp_base[:-1] + 'с'))
                    match = ((accshift(matchstr, len(matchstr)) + 'ть')
                             if ext_len == 3
                             else vtrans(matchstr + 'ть', take_int(akzpl)))
                elif asp_string.startswith('4'):
                    ext_len = (5 if infinitive.endswith(('ывать', 'ивать'))
                               else 3)
                    matchstr = infinitive[:-ext_len]
                    if "и́ть" not in asp_string:
                        match = ((accshift(matchstr, len(matchstr)) + 'ить')
                                 if ext_len == 3
                                 else vtrans(matchstr + 'ить',
                                             take_int(akzpl)))
                    else:
                        match = deacc(matchstr) + 'и́ть'
                elif asp_string.startswith('3'):
                    if infinitive.endswith(('ывать', 'ивать')):
                        asp_base = accinf[:-5]
                    elif infinitive.endswith('вать'):
                        asp_base = accinf[:-4]
                        asp_base = accshift(asp_base, len(asp_base))
                    else:
                        asp_base = accinf[:-3]
                        asp_base = accshift(asp_base, len(asp_base))
                    match = asp_base + 'нуть'
                elif asp_string.startswith('2'):
                    if infinitive.endswith('цовывать'):
                        match = infinitive[:-8] + 'цева́ть'
                    else:
                        match = infinitive[:-5] + 'а́ть'
                else:
                    if take_int(asp_string) > 0:
                        if infinitive.endswith(('ывать', 'ивать')):
                            asp_base = accinf[:-5]
                            if infinitive.endswith('ывать'):
                                match = asp_base + 'ать'
                            else:
                                match = asp_base + \
                                    ('ать' if asp_base[-1] in 'кгхчшщж'
                                     else 'ять')
                        elif infinitive.endswith('вать'):
                            matchstr = infinitive[:-4]
                            matchstr = vtrans(matchstr, len(matchstr))
                            match = matchstr + 'ть'
                        else:
                            match = accshift(infinitive, len(infinitive) - 3)

                if "(-о-)" in asp_string or "(-о́-)" in asp_string:
                    substit = ("о" if "(-о-)" in asp_string else "о́")
                    ext_len = (1 if match.endswith('чь') else 3)
                    ch_base = match[:-ext_len]
                    ch_end = match[-ext_len + 1:]
                    k = next((k for k in range(1, len(ch_base) + 1)
                             if ch_base[-k] in "аа́"), 0)
                    if k:
                        ch_base = stuff(ch_base, -k, 2, substit)
                    match = ch_base + ch_end
                else:
                    if take_int(asp_string) == 0:
                        # * match = derform(alltrim(asp_string))
                        match = asp_string.strip()
                        irrmatch = True
                    elif "(" in asp_string:
                        if not re.search("-(?:а|ну|и|я)\u0301?ть|-ё-",
                                         asp_string):
                            asp_string = rest(asp_string, "(").replace(")", '')
                            asp_string = derform(asp_string.strip())
                            if ";" in asp_string:
                                asp_string = derform(first(asp_string, ";"))
                            if asp_string != "-":
                                match = asp_string
                            else:
                                a_s = asp_string[1:]
                                d_acc = re.search('и́|а́|я́|ю́|а́|о́', a_s)
                                if a_s.endswith(("бить", "би́ть", "пить",
                                                 "пи́ть", "фить", "фи́ть",
                                                 "мить", "ми́ть", "вить",
                                                 "ви́ть")):
                                    matchstr = match[:-len(a_s) - 1]
                                elif infinitive.endswith(('ждать', 'жда́ть')):
                                    matchstr = match[:-len(a_s) - 1]
                                elif a_s in ("стить", "сти́ть",
                                             "ости́ть", "остить"):
                                    matchstr = match[:-len(a_s) + 1]
                                else:
                                    matchstr = match[:-len(a_s)]
                                match = (deacc(matchstr) if d_acc
                                         else matchstr) + a_s
                        elif '-ё-' in asp_string:
                            match = match.replace("а́", "ё")
                        elif 'а́' in asp_string or 'я́' in asp_string or \
                                'и́' in asp_string or 'у́' in asp_string:
                            match = vtrans(deacc(match), len(match) - 2)
                if infinitive.startswith('вы'):
                    match = vtrans(deacc(match), 2)
        yo = False

        pass_pers: List[Optional[str]] = [None] * 4
        pasp_pers: List[Optional[str]] = [None] * 4
        p_pastf: List[str] = [""] * 5
        if refl:
            if irrmatch:
                match = asp_string
            else:
                match = (match + rform(match)) if match else ''
            for i in range(1, 4):
                s_pers[i] += rform(s_pers[i])
                p_pers[i] += rform(p_pers[i])
            impsing += rform(impsing)
            impplur += rform(impplur)
            impsing2 = (impsing2 + rform(impsing2)) if impsing2 else ''
            impplur2 = (impplur2 + rform(impplur2)) if impplur2 else ''
            prespart = (prespart + rform(prespart)) if prespart else ''
            presger = (presger + rform(presger)) if presger else ''
            pastpart = (pastpart + rform(pastpart)) if pastpart else ''
            pastpar1 = (pastpar1 + rform(pastpar1)) if pastpar1 else ''
            if pastger:
                pastger = pastger.translate(REMOVE_PARENS)
                pastger = pastger + rform(pastger)
            else:
                pastger = ''
            pastger2 = ""
            for i in range(1, 5):
                the_form = pastf[i] + rform(pastf[i])
                if "с''" in full_akz and i > 2:
                    pastf[i] = the_form + "//" + accshift(deacc(the_form),
                                                          len(the_form))
                else:
                    pastf[i] = the_form
            pastf1 = (pastf1 + rform(pastf1)) if pastf1 else ''
        else:
            if obj_stat.startswith('trans.') and aspect.startswith('imperf.'):
                if 'страд. нет' not in features:
                    refl = True
                    if 'буд. нет' in features:
                        pass_pers, pasp_pers = [' - '] * 4, [' - '] * 4
                        p_prespart = p_impsing = p_impplur = p_prspr1 = \
                            p_impsi2 = p_imppl2 = ' - '
                    else:
                        pass_pers = [f + rform(f) for f in s_pers]
                        pasp_pers = [f + rform(f) for f in p_pers]
                        p_prspr1 = prespart + rform(prespart)
                        p_impsing = impsing + rform(impsing)
                        p_impplur = impplur + rform(impplur)
                        p_impsi2 = ((impsing2 + rform(impsing2)) if impsing2
                                    else '')
                        p_imppl2 = ((impplur2 + rform(impplur2)) if impplur2
                                    else '')
                    p_pstpr1 = (pastpart + rform(pastpart)) if pastpart else ''
                    p_pstpr2 = (pastpar1 + rform(pastpar1)) if pastpar1 else ''
                    p_pastf = [(f + rform(f)) if f else '' for f in pastf]
                    p_pastf1 = (pastf1 + rform(pastf1)) if pastf1 else ''
                else:
                    pass_pers, pasp_pers = [' - '] * 4, [' - '] * 4
                    p_pastf = [' - '] * 5
                    p_impsing = p_impplur = p_prespart = p_prspr1 = \
                        p_pastf1 = p_pstpr = p_pstpr1 = p_pstpr2 = ' - '

        if "_" in match:
            match = "see dict. entry"

        getstr: Callable
        if retstring:
            out = OrderedDict([
                ('inf', example),
                ('obj_stat', obj_stat),
                ('aspect', aspect),
            ])
            retvalue += "Inf. {inf} ({obj_stat},{aspect})".format(**out)
            if match:
                out['match'] = match
                out['matchkind'] = ("imperf. match" if aspect.startswith('perf.')
                                    else "perf. match")
                retvalue += " ({matchkind}: {match})".format(**out)
            retvalue += "; "
            getstr = partial(getstr_, out=out)
        else:
            getstr = getstr_

        prms = "act.pr.ind."
        pams = "act.pa.ind."
        imms = "act.imp."

        if impers:
            getstr(s_pers[3], prms + "s.3")
            getstr(pastf[3], pams + "s.n.")
            if obj_stat.startswith('trans.') and aspect.startswith('perf.'):
                getstr(p_pstpr, "pass.part.pa.", full_v_akz)
        else:
            getstr(s_pers[1], prms + "s.1")
            getstr(s_pers[2], prms + "s.2")
            getstr(s_pers[3], prms + "s.3")
            getstr(p_pers[1], prms + "p.1")
            getstr(p_pers[2], prms + "p.2")
            getstr(p_pers[3], prms + "p.3")
            if pastf1:
                pastf[1] = pastf[1] + "//" + pastf1
            getstr(pastf[1], pams + "s.m.")
            getstr(pastf[2], pams + "sFalse")
            getstr(pastf[3], pams + "s.n.")
            getstr(pastf[4], pams + "p.")

            impsing = "//".join((impsing, impsing2)) if impsing2 else impsing
            impplur = "//".join((impplur, impplur2)) if impplur2 else impplur
            getstr(impsing, imms + "s.")
            getstr(impplur, imms + "p.")

            pastpart = (pastpart + "//" + pastpar1) if pastpar1 else pastpart
            pastger = "//".join((pastger, pastger2)) if pastger2 else pastger
            getstr(prespart, "act.part.pr.", akzent)
            getstr(presger, "act.ger.pr.")
            getstr(pastpart, "act.part.pa.", akzent)
            getstr(pastger, "act.ger.pa.")

            if obj_stat.startswith('trans.') and aspect.startswith('imperf.') \
                    and not half_trans:
                if "нп" not in features:
                    prms = "pass.pr.ind."
                    pams = "pass.pa.ind."
                    imms = "pass.imp."
                    getstr(pass_pers[1], prms + "s.1")
                    getstr(pass_pers[2], prms + "s.2")
                    getstr(pass_pers[3], prms + "s.3")
                    getstr(pasp_pers[1], prms + "p.1")
                    getstr(pasp_pers[2], prms + "p.2")
                    getstr(pasp_pers[3], prms + "p.3")
                    if p_pastf1:
                        p_pastf[1] = p_pastf[1] + '//' + p_pastf1
                    getstr(p_pastf[1], pams + "s.m.")
                    getstr(p_pastf[2], pams + "sFalse")
                    getstr(p_pastf[3], pams + "s.n.")
                    getstr(p_pastf[4], pams + "p.")

                    p_impsing = ('//'.join((p_impsing, p_impsi2)) if p_impsi2
                                 else p_impsing)
                    p_impplur = ('//'.join((p_impplur, p_imppl2)) if p_imppl2
                                 else p_impplur)
                    getstr(p_impsing, imms + "s.")
                    getstr(p_impplur, imms + "p.")

                p_prespart = ('//'.join((p_prespart, p_prspr1)) if p_prespart
                              else p_prspr1)
                getstr(p_prespart, "pass.part.pr.", akzent)
                if "нп" not in features:
                    if not p_pstpr:
                        p_pstpr = ('//'.join((p_pstpr1, p_pstpr2)) if p_pstpr2
                                   else p_pstpr2)
                    getstr(p_pstpr, "pass.part.pa.", full_v_akz)
            elif obj_stat.startswith('trans.') and \
                    aspect.startswith('imperf.') and half_trans:
                p_prespart = ('//'.join((p_prespart, p_prspr1)) if p_prespart
                              else p_prspr1)
                getstr(p_prespart, "pass.part.pr.", akzent)
                if not p_pstpr:
                    p_pstpr = ('//'.join((p_pstpr1, p_pstpr2)) if p_pstpr2
                               else p_pstpr1)  # FIXME: value not used
            elif obj_stat.startswith('trans.') and aspect.startswith('perf.'):
                getstr(p_pstpr, "pass.part.pa.", full_v_akz)

        refl = o_refl  # restore reflexivity flag
        return retvalue

    return nompar, verbpar


def morphon(source, target, stemcons: str, inf_part, akzent):
    """Modifying the nominal ending utilizing morphonological information."""

    s_string = '00 0а 0у 0е 0ы 0и 0о ь0 ьа ьу ье ьы ьо ьь'

    if stemcons in 'чшжщкгх':
        if akzent in 'асе' and stemcons in 'чшжщ':

            t_string = ('   а  у  о  и  и  о     а  у  о  и  о    ' if "3**" in inf_part else
                        '   а  у  е  и  и  е  ь  а  у  е  и  е  ь ')
        else:
            t_string = '   а  у  е  и  и  о  ь  а  у  е  и  о  ь '
    elif voc(stemcons, 1):
        if stemcons == 'и':
            t_string = '   а  у  е  ы  и  о  й  я  ю  и  и  е  й '
        else:
            t_string = '   а  у  е  ы  и  о  й  я  ю  е  и  е  й '
    else:
        if akzent in 'асе' and stemcons == 'ц':
            t_string = '   а  у  е  ы     е     а  у  е  ы  е    '
        elif stemcons == 'ц':
            t_string = '   а  у  е  ы     о     а  у  е  ы  о    '
        else:
            t_string = '   а  у  е  ы  и  о  ь  я  ю  е  и  е  ь '
    return t_string[s_string.find(source + target):][:1]


def voctrans(stem, ending, alter, spec, c_case, infor, akzent):
    """Insert "fugitive" vowels."""

    ending = ending.strip()

    # Inserting here " and "6*" not in infor" was incorrect: it ruins
    # paradigms like воробей, улей etc. Instead we should add
    # " or 'мо мс' in infor" to this horrible list of conditions
    # (to ensure correct handling of the noun (sic!) третий

    if alter:  # and "6*" not in infor
        if (
            (re.search(r"м |мо ", infor) and
             not re.search(r"ж |жо |м с |мо мс|мо со |_кф м ", infor)) or
            (re.search(r"ж |жо ", infor) and "8*" in infor)
        ):
            if ending in '0ью':
                if '"2"' in infor + spec and c_case in 'Gp Apa ' \
                        and '3*' in infor:
                    if akzent in "аD":
                        if not voc(stem, -2) and stem[-2:][0] in 'чжшщ':
                            return stuff(stem, -1, 0, 'е')
                        elif voc(stem, -2) and stem[-3:][0] in 'чжшщ':
                            return stuff(stem, -2, 1, 'е')
                    return (stem[:-(2 if voc(stem, -2) else 1)] +
                            'о' + stem[-1:])
                return stem
            elif stem[-1:] == 'е':
                return stem[:-1] + 'ь'
            else:
                part1 = stem[:-2]
                part2 = stem[-2:][0]
                part3 = stem[-1:]
                if part2 == 'о':
                    return part1 + part3
                elif part2 in 'еая':
                    if voc(part1, -1):
                        return part1 + 'й' + part3
                    elif part1[-1:] == 'л' or \
                            (part3 in 'кгх' and part1[-1:] not in 'чшжщц'):
                        return part1 + 'ь' + part3
                    return part1 + part3
                return stem
        else:
            if '6*' in infor:
                return stem
            if ending != '0':
                return stem
            if stem[-2:][0] in 'ьй':
                return stem[:-2]+'е'+stem[-1:]
            else:
                part1 = stem[:-1]
                part2 = stem[-1:]
                if part1[-1:] in 'кгх' or \
                        (part2 in 'кгх' and part1[-1:] not in 'чшжщц') or \
                        (part1[-1:] in 'чшжщц' and akzent in 'веF'):
                    return part1 + 'о' + part2
                return part1 + 'е' + part2
    return stem


def oneacc(string: str) -> str:
    """Strip secondary accents from a word form."""

    accented: bool = False
    ret_string: str = ''
    for c in string[::-1]:
        if c == "\u0301":
            if not accented:
                accented = True
                ret_string = c + ret_string
        else:
            ret_string = c + ret_string
    return ret_string


def yotr(string: str) -> str:
    """Replace accented а́ with a ё."""
    return stuff(string, string.rfind("а́"), len("а́"), "ё")
