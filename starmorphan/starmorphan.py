import re
import socket
from functools import lru_cache

BUFFER_SIZE = 65536


def communicate(s, option='', host="127.0.0.1", port=8001):
    try:
        s = s.encode('CP866')
    except UnicodeEncodeError:
        return None
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))
    sock.send(b"\r\n\r\n_MO_%s%s\x0Ecgie\r\n\r\n" % ((option.encode('CP866') + b' ') if option else b'', s))
    data = sock.recv(BUFFER_SIZE)
    sock.close()
    data = data[data.index(b'\x11\n') + 2:data.index(b'\x12')]
    if data.startswith(b'Word not found'):
        return None
    return data.decode('CP866').strip()


def paradigm(s, host='127.0.0.1', port=8001):
    data = communicate(s, option='', host=host, port=port)
    return data.strip() if data else None


def classify(s, host='127.0.0.1', port=8001):
    data = communicate(s, option='-T', host=host, port=port)
    return (data.split('\x04')[0]) if data else None


def info(s, host='127.0.0.1', port=8001):
    if ord(s[0]) < 128:
        return None
    data = communicate(s, option='-A', host=host, port=port)
    return ([{k.split(' ', 1)[0].lower(): v
              for k, v in (l.strip().replace("'", "").split(': ', 1)
                           for l in re.sub(r'^\d+\. ', '', homonym).split('<p>') if l.strip())}
             for homonym in data.split('<p><p>') if homonym]
            if data else None)


@lru_cache(maxsize=100000)
def grammar(word):
    infostring = info(word)
    if not infostring:
        return '\x10%s' % word
    return '{%s}' % ';'.join('%s %s' % (v['source'], v['morphological'])
                             for v in infostring)

