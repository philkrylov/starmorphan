import os.path
from typing import Optional, Tuple

import marisa_trie  # type: ignore


TR_ZALIZNYAK = str.maketrans({"░": "а\u0301",
                              "▓": "е\u0301",
                              "╡": "ё",
                              "╖": "и\u0301",
                              "╣": "о\u0301",
                              "╛": "у\u0301",
                              "┬": "ы\u0301",
                              "─": "э\u0301",
                              "╞": "ю\u0301",
                              "╩": "я\u0301",
                              "▒": "а\u0300",
                              "┤": "е\u0300",
                              "╢": "ё\u0300",
                              "╕": "и\u0300",
                              "╜": "о\u0300",
                              "┴": "у\u0300",
                              "├": "ы\u0300",
                              "┼": "э\u0300",
                              "╟": "ю\u0300",
                              "╦": "я\u0300",
                              "╙": "ö",
                              "Є": "ü"})


def _dict_entry_to_key_value(line: bytes) -> Tuple[str, bytes]:
    k, v = line.split(b" ", maxsplit=1)
    return (k.decode('CP866'),
            (v.rstrip()
             .decode('CP866')
             .translate(TR_ZALIZNYAK)
             .encode('UTF16')))


def dict_read(n_dict: int) -> marisa_trie.BytesTrie:
    DICTPATH = os.path.dirname(__file__)
    engfile = "dict_en.dct"
    russfile = "dict_ru.dct"
    f = open(os.path.join(DICTPATH, [engfile, russfile][n_dict]), "rb")
    return marisa_trie.BytesTrie(
        _dict_entry_to_key_value(line)
        for line in f if not line.startswith(b"*")
    )


_dict = [None, None]


def dict_get_entry(eng: bool, source: str, n_entry: int = 1) -> Optional[str]:
    n_dict = 0 if eng else 1
    if not _dict[n_dict]:
        _dict[n_dict] = dict_read(n_dict)

    entries = _dict[n_dict].get(source, [])  # type: ignore
    if n_entry <= len(entries):
        return entries[n_entry - 1].decode('UTF16')
    return None
