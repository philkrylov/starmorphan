# starmorphan

Morphological analyser and synthesizer for Russian and English languages based on S.A.Starostin's code from StarLing.

A minimal CLI is provided:

```
usage: python -m starmorphan analyse [-h] WORDFORM [WORDFORM ...]

positional arguments:
  WORDFORM    wordforms to analyse

optional arguments:
  -h, --help  show this help message and exit


usage: python -m starmorphan generate [-h] [-t] LEXEME [LEXEME ...]

positional arguments:
  LEXEME      lexemes to generate paradigm for

optional arguments:
  -h, --help  show this help message and exit
  -t          use transinf format


usage: python -m starmorphan serve [-h] [--host HOST] [-j N_WORKERS] [-u] [PORT]

positional arguments:
  PORT          port to listen [8001]

optional arguments:
  -h, --help    show this help message and exit
  --host HOST   host to listen at [127.0.0.1]
  -j N_WORKERS  worker count [4]
  -u            use UTF-8
```
